<script type="text/javascript">
        $(document).ready(function () {
            $("#inputUsername").keypress(function (event) {

                var key = event.which || event.keyCode; //use event.which if it's truthy, and default to keyCode otherwise

                // Allow: backspace, delete, tab, and enter
                var controlKeys = [8, 9, 13];
                //for mozilla these are arrow keys
                if ($.browser.mozilla)
                    controlKeys = controlKeys.concat([37, 38, 39, 40]);
                // Ctrl+ anything or one of the conttrolKeys is valid
                var isControlKey = event.ctrlKey || controlKeys.join(",").match(new RegExp(key));
                if (isControlKey) {
                    return;
                }

                // stop current key press if it's not a numbers and letters
                if (!(48 <= key && key <= 57) && (65 <= key && key <= 90)) {
                    event.preventDefault();
                    return;
                }
            });
            $('#inputUsername').keyup(function () {
                //to allow decimals,use/[^a-zA-Z0-9]/g 
                var regex = new RegExp(/[^a-zA-Z0-9]/g);
                var containsNonNumeric = this.value.match(regex);
                if (containsNonNumeric)
                    this.value = this.value.replace(regex, '');
            });
        });
</script>