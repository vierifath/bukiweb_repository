/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.model.Ssfuser;
import bukiweb.model.Sslgroup;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import bukiweb.dao.MenuInfDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Rustanian
 */
@Controller
public class TreeMenuController1 {

    @Autowired(required = false)
    private MenuInfDao menuInfDao;

    private String menuku = "";
    private String groupid = "";
    List<Object[]> resultsx;

    protected String getGrup(String groupid) throws Exception {
        // HttpServletRequest request = null;
        int clcd;
        String nama = "";
        Map map = new HashMap();
        String asal = "null";
        String temp = "null";
        String node = "";

        Sslgroup sslgroup;
        Ssfuser ssfuser;
        Ssltree2 ssltree2;
        Sslnode sslnode;

        //System.out.println("Test Tendi : ");
        try {

//            System.out.println("groupid : " + groupid);
            List<Object[]> results = menuInfDao.getTreeParent(groupid);
            List<Object[]> results2 = menuInfDao.getTreeChild(groupid);
            resultsx =results2;
            int a = 0;
            for (Object[] result : results) {
//                results2.remove(a);
                for (Object[] resultn : results) {

                    String test = (String) resultn[3];
                    String prev = (String) resultn[4];
                    node = (String) resultn[2];
                    test = test.trim();
                    prev = prev.trim();
                    node = node.trim();
                    if (test.equals("null") && prev.equals(asal)) {
//                    System.out.println("masuk sini");

                        menuku += "<li>";
                        menuku += "<a href=\"#\"><i class=\"fa fa-sitemap\"></i> <span class=\"nav-label\">";
                        menuku += ((String) resultn[6]).trim();
                        menuku += "</span><span class=\"fa arrow\"></span></a>";
                        String papah = (String) resultn[2];
                        papah = papah.trim();
                        loopMenu(resultsx, papah);
                        menuku += "</li>";
                        temp = node;

                    }
                }
                asal = temp;
                a++;
            }
            return menuku;
        } catch (Exception e) {

            System.out.println("gagal : " + e.getMessage());
            return "error";
        }

    }

    public void loopMenu(List<Object[]> results, String parent) {

//        System.out.println(parent); nodepr=8
        String[] baskom = new String[results.size()];
        ArrayList<String> al = new ArrayList<>();

//        resultsx = results;
//        
        for (int i = 0; i < results.size(); i++) {
            Object[] result = results.get(i);
            String test = (String) result[3];
            test = test.trim();
            if (test.equals(parent)) {
                al.add(((String) result[2]).trim() + "|" + ((String) result[3]).trim() + "|" + ((String) result[6]).trim() + "|" + ((String) result[7]).trim() + "|" + ((String) result[4]).trim() + "|" + i);
//                resultsx.remove(i);
            }

        }
        findprev(al, resultsx);
    }

    public void loopMenu2(List<Object[]> results, String parent) {
        String[] baskom = new String[results.size()];
        ArrayList<String> al = new ArrayList<>();

//        System.out.println("loopmenu : " + resultsx.size());
        for (int i = 0; i < results.size(); i++) {
            Object[] result = results.get(i);
            String test = (String) result[3];
            test = test.trim();
            if (test.equals(parent)) {
                al.add(((String) result[2]).trim() + "|" + ((String) result[3]).trim() + "|" + ((String) result[6]).trim() + "|" + ((String) result[7]).trim() + "|" + ((String) result[4]).trim() + "|" + i);
//                System.out.println("tetest " + ((String) result[2]).trim());
            }
        }
//        System.out.println("loopmenu now : " + resultsx.size());
//        System.out.println("-----------------------------------------------");
        findprev2(al, resultsx);
    }

    public String findprev(ArrayList baskom, List<Object[]> results) {
        String asal = "null";
        String prev = "";
        String temp = "";
        int kondisi = 0;
        int kondisi2 = 0;
        for (Object san : baskom) {
//            String listString = san + "\t";5
//            System.out.println(listString);
            for (Object dit : baskom) {
                String data_tree = dit.toString();
                String[] pepes = data_tree.split("\\|");
                prev = pepes[4];
                String node = pepes[0];
                String status = pepes[3];
                String papah = pepes[1];

                if (prev.equals(asal) && status.equals("1")) {
//                    System.out.println("kondisi a " + kondisi);
                    menuku += "<ul class=\"nav nav-second-level collapse\">";
                    menuku += "<li >";
                    menuku += "<a href=\"#\" onclick=\"loadBody('" + pepes[0] + "')\">";
                    menuku += pepes[2];
                    menuku += "</a>";
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    resultsx.remove(pepes[5]);
                } else if (prev.equals(asal) && status.equals("0")) {
//                    System.out.println("kondisi b " + kondisi);
                    menuku += "<ul class=\"nav nav-second-level collapse\">";
                    menuku += "<li >";
                    menuku += "<a href=\"#\">";
                    menuku += pepes[2];
                    menuku += "<span class=\"fa arrow\"></span></a>";
                    loopMenu2(resultsx, node);
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    resultsx.remove(pepes[5]);
                }
            }
//            System.out.println("tem : " + temp);
            asal = temp;
        }
        return null;
    }

    public String findprev2(ArrayList baskom, List<Object[]> results) {
        String asal = "null";
        String prev = "";
        String temp = "";
        int kondisi = 0;
        int kondisi2 = 0;
        for (Object san : baskom) {
//            String listString = san + "\t";
//            System.out.println(listString);
            for (Object dit : baskom) {
//                System.out.println("ada1");
                String data_tree = dit.toString();
                String[] pepes = data_tree.split("\\|");
//                System.out.println("ada2");;
                prev = pepes[4];
                String node = pepes[0];
                String status = pepes[3];
                String papah = pepes[1];
//                System.out.println("ada3");

                if (prev.equals(asal) && status.equals("1")) {
//                    System.out.println("ada4");
                    menuku += "<ul class=\"nav nav-third-level\">";
                    menuku += "<li class=\"\">";
                    menuku += "<a href=\"#\" onclick=\"loadBodyPost('" + pepes[0] + "')\">";
//                    menuku += "<a href=\""+ result[2]+"\"> onclick=\"loadDoc('"+result[2]+"')\" ";
                    menuku += pepes[2];
                    menuku += "</a>";
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    resultsx.remove(pepes[5]);
                } else if (prev.equals(asal) && status.equals("0")) {
//                    System.out.println("ada5");
                    menuku += "<ul class=\"nav nav-third-level\">";
                    menuku += "<li class=\"\">";
                    menuku += "<a href=\"#\">";
                    menuku += pepes[2];
                    menuku += "<span class=\"fa arrow\"></span></a>";
                    loopMenu2(results, node);
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    resultsx.remove(pepes[5]);
                }
            }
//            System.out.println("tem : " + temp);
            asal = temp;
        }
//        System.out.println("ada");
        return null;
    }
}
