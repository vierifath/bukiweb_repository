package bukiweb.controller;

import bukiweb.dao.DisplayNodeDao;
import bukiweb.dao.ReleaseUserDao;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ReleaseUserController {

    Gson gson = new Gson();

    @Autowired(required = false)
    private ReleaseUserDao releaseUserDao;

    @ResponseBody
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    private String getUser(HttpServletRequest request) {
        try {

            String userlogin = "";
            List<Object[]> getuser = null;

            getuser = releaseUserDao.getAllUser(userlogin);

            String usr_release = "";

            usr_release += "<select id=\"nodeid\" onchange=\"loadReleaseA(this.value)\" class=\"select2_demo_3 form-control m-b\" name=\"userlogin\">";
            usr_release += "<option disabled=\"\" selected=\"\">--PILIH USER ID--</option>";
            for (Object[] resultn : getuser) {
                usr_release += "<option value=\"" + resultn[0] + "/" + resultn[1] + "\">" + resultn[0].toString().trim() + " / " + resultn[1].toString().trim() + "</option>";
            }
            usr_release += "</select>";

            return usr_release;
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    @ResponseBody
    @RequestMapping(value = "/userAs", method = RequestMethod.GET)
    private String getUserAs(HttpServletRequest request) {
        try {

            String userlogin = "";
            List<Object[]> getuser = null;

            getuser = releaseUserDao.getAllUserAs(userlogin);

            String usr_release = "";

            usr_release += "<select id=\"nodeid\" onchange=\"loadReleaseA(this.value)\" class=\"select2_demo_3 form-control m-b\" name=\"userlogin\">";
            usr_release += "<option disabled=\"\" selected=\"\">--PILIH USER ID--</option>";
            for (Object[] resultn : getuser) {
                usr_release += "<option value=\"" + resultn[0] + "/" + resultn[1] + "\">" + resultn[0].toString().trim() + " / " + resultn[1].toString().trim() + "</option>";
            }
            usr_release += "</select>";

            return usr_release;
        } catch (Exception e) {
            return e.getMessage();
        }

    }
    
    @ResponseBody
    @RequestMapping(value = "/UAAUB", method = RequestMethod.POST)
    private String getUserFac(HttpServletRequest request, @RequestBody String treenode) {
        try {
            org.json.JSONObject jsonoGrupId = new org.json.JSONObject(treenode);
            String idgrup = jsonoGrupId.getString("grupid");
            String nodeid = jsonoGrupId.getString("nodeid");
            String ubatid = jsonoGrupId.getString("ubatid");
            String nextp = jsonoGrupId.getString("nextp");
            String func = jsonoGrupId.getString("func");
            String userlogin = jsonoGrupId.getString("userlogin");
            System.out.println(func);
            List<Object[]> getGroupId = releaseUserDao.getUserFacLevel(idgrup, nodeid);
            Integer temp = null;

            for (Object[] resultn : getGroupId) {
                temp = (Integer) resultn[0];
            }

            List<Object[]> getuser = null;
            PagedListHolder pagedListHolderNode = null;
            if (func.equals("UAAUB1")) {
                System.out.println("masuk uaaub");
                if (temp == 1) {
                    System.out.println("masuk temp 1");
                    getuser = releaseUserDao.getAllUser(userlogin);
                    pagedListHolderNode = new PagedListHolder(getuser);
                    pagedListHolderNode.setPageSize(getuser.size());
                } else if (temp == 2) {
                    System.out.println("masuk temp 2");
                    getuser = releaseUserDao.getAllUserCabang(ubatid, nextp, userlogin);
                    System.out.println("masuk temp 2");
                    pagedListHolderNode = new PagedListHolder(getuser);
                    System.out.println("masuk temp 2");
                    pagedListHolderNode.setPageSize(getuser.size());
                }

            } else if (func.equals("UAAUR1")){
//                PagedListHolder pagedListHolderNode = null;
//                List<Object[]> getuser = null;
                System.out.println("masuk uaaur");
                getuser = releaseUserDao.getAllUserAs(userlogin);
                pagedListHolderNode = new PagedListHolder(getuser);
                pagedListHolderNode.setPageSize(getuser.size());

            }else{
                System.out.println("masuk enable disable");
                getuser = releaseUserDao.getAllUserEnable(userlogin);
                pagedListHolderNode = new PagedListHolder(getuser);
                pagedListHolderNode.setPageSize(getuser.size());
            }

            String usr_release = "";

            usr_release += "<select id=\"nodeid\" onchange=\"loadReleaseA(this.value)\" class=\"select2_demo_3 form-control m-b\" name=\"userlogin\">";
            usr_release += "<option disabled=\"\" selected=\"\">--PILIH USER ID--</option>";
            for (Object[] resultn : getuser) {
                usr_release += "<option value=\"" + resultn[0] + "/" + resultn[1] + "\">" + resultn[0].toString().trim() + " / " + resultn[1].toString().trim() + "</option>";
            }
            usr_release += "</select>";
            List pageList = pagedListHolderNode.getPageList();
            JSONObject objGrupId = new JSONObject();
            objGrupId.put("ret_release", usr_release);
            return gson.toJson(objGrupId);
        } catch (Exception e) {
            System.err.println("e : " + e);
            return gson.toJson(e.getMessage());
        }

    }

    @ResponseBody
    @RequestMapping(value = "/getNamaUser", method = RequestMethod.GET)
    private String getNamaUser(HttpServletRequest request, @RequestBody String treenode) {

        if (request.getMethod().contains("GET")) {
            return gson.toJson("null");
        }

        try {
            org.json.JSONObject jsonoGrupId = new org.json.JSONObject(treenode);
            String usr_release = "";
            JSONObject objGrupId = new JSONObject();
            objGrupId.put("ret_release", usr_release);
            return gson.toJson(objGrupId);
        } catch (Exception e) {
            return "gagal " + e.getMessage();
        }

    }

    @ResponseBody
    @RequestMapping(value = "/getUserid", method = RequestMethod.POST)
    private String getUserId(HttpServletRequest request, @RequestBody String treenode) {
        System.out.println("sampe ke sini");
        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(treenode);
        String userId = jsonoGrupId.getString("userId");
        System.out.println("user : " + userId);
        String listUserId = releaseUserDao.updateRelease(userId);

        JSONObject object = new JSONObject();
        object.put("release", getUser(request));
        object.put("pesan", listUserId);
        return gson.toJson(object);
    }

    @ResponseBody
    @RequestMapping(value = "/releaseUserAs", method = RequestMethod.POST)
    private String releaseAs400(HttpServletRequest request, @RequestBody String treenode) {
        System.out.println("sampe ke sini");
        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(treenode);
        String userId = jsonoGrupId.getString("userId");
        System.out.println("user : " + userId);
        String listUserId = releaseUserDao.getAllUserAS400(userId);
        JSONObject object = new JSONObject();
        System.out.println(listUserId);
        object.put("pesan", listUserId);
        return gson.toJson(object);
    }
}
