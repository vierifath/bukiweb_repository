/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Hamdani pc
 */
@Controller
public class UserController {

    boolean logonvalid = false;
    private String LibProg;
    private String ProgName;
    private String SysUserId = "";
    private String SysPassword = "";
    private String IPServer = "";

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();
        Properties prop = new Properties();

        String propFileName = "/bukiweb/conf/jdbc.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }

//        List list = pc.prop(); 
        ProgName = pc.prop();

        IPServer = prop.getProperty("url");
        SysUserId = prop.getProperty("username");
        SysPassword = prop.getProperty("password");
//        LibProg = (String) list.get(0);
//        ProgName = (String) list.get(1);

        System.out.println("URL : " + ProgName);
    }

    @ResponseBody
    @RequestMapping(value = "/enableUser", method = RequestMethod.POST)
    private String enanbledUser(@RequestBody String param) {

        getPropUrl();
        String message;
        org.json.JSONObject jsonoParam = new org.json.JSONObject(param);
//        String username = jsonoParam.getString("username");
        String userid = jsonoParam.getString("userid");
//        String password = jsonoParam.getString("password");
        String aksi = jsonoParam.getString("aksi");

        System.out.println(userid + ":" + aksi);

        try {
            //-----------------

            AS400 as400 = new AS400(IPServer, SysUserId, SysPassword);
            as400.setGuiAvailable(false);
            logonvalid = as400.validateSignon(SysUserId, SysPassword);
            System.out.println("IPSERVER = " + IPServer + ", USER = " + SysUserId + ", Pass = " + SysPassword);

            if (logonvalid) {
                ProgramCall program = new ProgramCall(as400);

                AS400Text userId = new AS400Text(10, as400);
                AS400Text act = new AS400Text(2, as400);
                AS400Text pass = new AS400Text(10, as400);
                AS400Text msg = new AS400Text(50, as400);

                ProgramParameter[] parameterlist = new ProgramParameter[4];

                parameterlist[0] = new ProgramParameter(userId.toBytes(StringUtils.rightPad(userid, 10, " ")));
                parameterlist[1] = new ProgramParameter(act.toBytes(StringUtils.rightPad(aksi, 2, " ")));
                parameterlist[2] = new ProgramParameter(pass.toBytes(StringUtils.rightPad(SysPassword, 10, " ")));
                parameterlist[3] = new ProgramParameter(50);

                System.out.println("sukses");

                program.setProgram(ProgName, parameterlist);

                if (program.run() != true) {
                    AS400Message[] messageList = program.getMessageList();
                    for (int i = 0; i < messageList.length; ++i) {
                        messageList[i].getText();
                        messageList[i].load();
                        messageList[i].getHelp();
                    }
                    message = Arrays.toString(messageList);
                    System.out.println("Error : " + Arrays.toString(messageList));
                } else {
                    AS400Text textData;
                    textData = new AS400Text(50, as400);
                    String pesan = (String) textData.toObject(parameterlist[3].getOutputData());
                    System.out.println("isi pesan : " + pesan);
                    pesan = pesan.trim();
                    if (pesan.equals("")) {
                        message = "Sukses";
                    } else {
                        message = "EROR : " + pesan;
                    }

                }
            } else {
                message = "ERROR";
            }

        } catch (ObjectDoesNotExistException | InterruptedException | ErrorCompletingRequestException | PropertyVetoException | AS400SecurityException | IOException e) {
            message = e.getMessage() + ":" + Arrays.toString(e.getStackTrace());
            System.out.println(message);
        }

        System.out.println(message);

        return message;
    }

    public static void main(String[] args) {
        UserController uc = new UserController();
        uc.getPropUrl();
    }
}
