/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.dao.DisplayNodeDao;
import bukiweb.dao.TreeDao;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Gustiana
 */
@Controller
public class TreeController extends TreeMenuController {

    private Gson gson = new Gson();
    String idnodetemp;

    @Autowired
    private TreeDao treeDao;
    @Autowired
    private DisplayNodeDao displayNodeDao;
//    @Autowired

    @ResponseBody
    @RequestMapping(value = "/getGrupId", method = RequestMethod.GET)
    
    private String getOpening(HttpServletRequest request) {

        List<String[]> listGrupId = treeDao.getGrupId();

        JSONObject objGrupId = new JSONObject();
        objGrupId.put("GrupId", listGrupId);
        return gson.toJson(objGrupId);
    }

    @ResponseBody
    @RequestMapping(value = "/GetByGrup", method = RequestMethod.POST)
    private String getGrupById(HttpServletRequest request, @RequestBody String grupid) throws Exception {

        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(grupid);
//        System.out.println(jsonoGrupId);
        String idgrup = jsonoGrupId.getString("idgrup");
        String func = jsonoGrupId.getString("function");
        List grupname = treeDao.getNameGrup(idgrup);
        System.out.println("Nama Grup : " + grupname.get(0));
        JSONObject objGrupId = new JSONObject();
        objGrupId.put("GrupId", getGrup(func, idgrup, grupname.get(0).toString()));

        return gson.toJson(objGrupId);

    }

    @ResponseBody
    @RequestMapping(value = "/insertAfter", method = RequestMethod.POST)
    private String InsertAfter(HttpServletRequest request, @RequestBody String tree) throws Exception {

        Ssltree2 ssltree = gson.fromJson(tree, Ssltree2.class);
//        System.out.println(ssltree.getSSMAPID() + ":" + ssltree.getSSNODEID() + ":" + ssltree.getSSPARENT() + ":" + ssltree.getSSNODEPR() + ":" + ssltree.getSSGRPID() + ":" + ssltree.getSSLVLFAC());
        String pesan = treeDao.saveTree(ssltree);

        return gson.toJson(pesan);

    }

    @ResponseBody
    @RequestMapping(value = "/insertBefore", method = RequestMethod.POST)
    private String InsertBefore(HttpServletRequest request, @RequestBody String tree) throws Exception {

        Ssltree2 ssltree = gson.fromJson(tree, Ssltree2.class);
//        System.out.println(ssltree.getSSMAPID() + ":" + ssltree.getSSNODEID() + ":" + ssltree.getSSPARENT() + ":" + ssltree.getSSNODEPR() + ":" + ssltree.getSSGRPID() + ":" + ssltree.getSSLVLFAC());
        String pesan = treeDao.inserrtBefore(ssltree, idnodetemp);

        return gson.toJson(pesan);

    }

    @ResponseBody
    @RequestMapping(value = "/GetSubTree", method = RequestMethod.POST)
    private String getSubTree(HttpServletRequest request, @RequestBody String treenode) throws Exception {

        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(treenode);
        String idgrup = jsonoGrupId.getString("grupid");
        String nodeid = jsonoGrupId.getString("nodeid");
        List getSSMAPID = treeDao.getDesc();
        List<Object[]> listGrupId = treeDao.getNode(idgrup);
        System.out.println(idgrup + ":" + nodeid);
        List<String> tempgrup = treeDao.getNodeByParent(nodeid, idgrup);

        int id = Integer.parseInt(getSSMAPID.get(0).toString());

        String gruptemp = "null";
        if (tempgrup.isEmpty()) {
            System.out.println("kosong bos");
        } else {
            for (String grup : tempgrup) {
                gruptemp = grup;
            }
        }
        System.out.println("hasil : " + gruptemp);
        JSONObject param = new JSONObject();
        JSONObject idnodepr = new JSONObject();
        idnodepr.put("id", id);
        idnodepr.put("nodepr", gruptemp.trim());
        param.put("idnodepr", idnodepr);
        param.put("GrupId", listGrupId);

        return gson.toJson(param);

    }

    @ResponseBody
    @RequestMapping(value = "/InsertSubTree", method = RequestMethod.POST)
    private String InsertSubTree(@RequestBody String tree) {
        Ssltree2 ssltree = gson.fromJson(tree, Ssltree2.class);
//        treeDao.saveTree(ssltree);
        return treeDao.insertSubTree(ssltree);
    }

    @ResponseBody
    @RequestMapping(value = "/deleteNode", method = RequestMethod.POST)
    private String getCurrent(HttpServletRequest request, @RequestBody String grupnode) throws Exception {

        org.json.JSONObject jsonoidgrup = new org.json.JSONObject(grupnode);
        String idgrup = jsonoidgrup.getString("grupid");
        String nodeid = jsonoidgrup.getString("nodeid");
        System.out.println(idgrup);
        System.out.println(nodeid);
        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(getNode(request, grupnode));
        org.json.JSONObject beforeParent = jsonoGrupId.getJSONObject("mesaages");
        String nodepr = beforeParent.getString("before");

//        JSONObject ret = new JSONObject();
//        ret.put("grup", idgrup);
//        ret.put("node id", nodeid);
//        ret.put("node parent", nodepr);
        String ret = treeDao.deleteTree(idgrup, nodeid, nodepr);
        System.out.println("hasil dari dao : " + ret);
        return gson.toJson(ret);

    }

    @ResponseBody
    @RequestMapping(value = "/GetDeleteChild", method = RequestMethod.POST)
    private String getDeleteChild(HttpServletRequest request, @RequestBody String grupnode) throws Exception {

        org.json.JSONObject jsonoidgrup = new org.json.JSONObject(grupnode);
        String idgrup = jsonoidgrup.getString("grupid");
        String nodeid = jsonoidgrup.getString("nodeid");
        System.out.println(idgrup);
        System.out.println(nodeid);

//        JSONObject ret = new JSONObject();
//        ret.put("grup", idgrup);
//        ret.put("node id", nodeid);
//        ret.put("node parent", nodepr);
        String ret = treeDao.getChildTree(idgrup, nodeid);
        System.out.println("hasil dari dao : " + ret);
        return gson.toJson(ret);

    }

    @ResponseBody
    @RequestMapping(value = "/TRCHG", method = RequestMethod.POST)
    private String getToupdateTree(HttpServletRequest request, @RequestBody String forUpdate) {

        org.json.JSONObject jsonoidgrup = new org.json.JSONObject(forUpdate);
        String idgrup = jsonoidgrup.getString("grupid");
        String nodeid = jsonoidgrup.getString("nodeid");
        JSONObject objGrupId = new JSONObject();

        List<Ssltree2> facilityLevel = treeDao.getForupdateTree(nodeid, idgrup);
        Ssltree2 ssltree = facilityLevel.get(0);
        List listNodeName = treeDao.getNodeName(nodeid);
        List list = new ArrayList();
        String prev = ssltree.getSSNODEPR().trim();
        String nodeparernt = ssltree.getSSPARENT().trim();
        list.add(prev);
        list.add(nodeparernt);
        System.out.println(prev);
        System.out.println(nodeparernt);

        if (!prev.equals("null")) {
            List listPrevName = treeDao.getNodeName(prev);
            prev = listPrevName.get(0).toString();
        }
        if (!nodeparernt.equals("null")) {
            List listParentName = treeDao.getNodeName(nodeparernt);
            nodeparernt = listParentName.get(0).toString();
        }

        List listGrupName = treeDao.getGrupName(idgrup);

        objGrupId.put("TreeNode", ssltree);
        objGrupId.put("parentname", nodeparernt);
        objGrupId.put("prevname", prev);
        objGrupId.put("nodename", listNodeName.get(0).toString());
        objGrupId.put("grupname", listGrupName.get(0).toString());
        System.out.println("end of method TRCHG");
        return gson.toJson(objGrupId);

    }

    @ResponseBody
    @RequestMapping(value = "/updateTree", method = RequestMethod.POST)
    private String updateTree(HttpServletRequest request, @RequestBody String forUpdate) {

        org.json.JSONObject jsonoidgrup = new org.json.JSONObject(forUpdate);
        String idgrup = jsonoidgrup.getString("grupid");
        String nodeid = jsonoidgrup.getString("nodeid");
        int faclev = jsonoidgrup.getInt("faclev");

        return gson.toJson(treeDao.updateTree(faclev, nodeid, idgrup));

    }

    @ResponseBody
    @RequestMapping(value = "/getNode", method = RequestMethod.POST)
    private String getNode(HttpServletRequest request, @RequestBody String grupid) throws Exception {

//        System.out.println(ssltree.getSSMAPID() + ":" + ssltree.getSSNODEID() + ":" + ssltree.getSSPARENT() + ":" + ssltree.getSSNODEPR() + ":" + ssltree.getSSGRPID() + ":" + ssltree.getSSLVLFAC());
        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(grupid);
        String idgrup = jsonoGrupId.getString("grupid");
        String nodeid = jsonoGrupId.getString("nodeid");
        idnodetemp = nodeid;
        List<Object[]> pesan = treeDao.getNode(idgrup);
        List<Object[]> getBefore = treeDao.getBeforeNode(idgrup, nodeid);
        List getSSMAPID = treeDao.getDesc();
//        System.out.println(getSSMAPID.get(0).toString());
        System.out.println(getBefore);
        Object[] parentBefore = getBefore.get(0);

        JSONObject jsonParentBefore = new JSONObject();
//        System.out.println(parentBefore[1]);
//        if (parentBefore[1].equals("null") || parentBefore[1] == null) {
//            parentBefore[1] = idgrup;
//        }

        jsonParentBefore.put("parent", parentBefore[0]);
        jsonParentBefore.put("before", parentBefore[1]);

        JSONObject objGrupId = new JSONObject();
//        System.out.println(getBefore);
        objGrupId.put("mesaages", jsonParentBefore);
        objGrupId.put("GrupId", pesan);
        objGrupId.put("id", getSSMAPID.get(0).toString());
//        System.out.println("lolos coy");
        return gson.toJson(objGrupId);

    }

    @ResponseBody
    @RequestMapping(value = "/getDetNode", method = RequestMethod.POST)
    private String getDetNode(HttpServletRequest request, @RequestBody String nodeid) {

        Map map = new HashMap();

        org.json.JSONObject idnode = new org.json.JSONObject(nodeid);

        List<Object[]> listParent = displayNodeDao.getParentNode();

        String node = idnode.getString("node");
        String parentNode = idnode.getString("parent");
        String nodeId = idnode.getString("nodeid");
        String urlforsave = idnode.getString("urlforsave");

        System.out.println("node : " + node);
        System.out.println("parentNode : " + parentNode);
        System.out.println("nodeId : " + nodeId);
        System.out.println("urlforsave : " + urlforsave);

        parentNode = parentNode.trim();
        Sslnode s = treeDao.getDetNode(node);

        String pesan = "";

        System.out.println("nodeId : " + nodeId);

        if (urlforsave.equals("insertSubtree")) {
            parentNode = nodeId;
        }

        if (parentNode.equals("null")) {
            parentNode = "";
        }

        System.out.println("parentNode : " + parentNode);
        System.out.println("s.getSSNODEPRNT() : " + s.getSSNODEPRNT());
        String nameParentNode = "";
        for (Object[] parentListObj : listParent) {
            if (s.getSSNODEPRNT().equals(parentListObj[0])) {
                nameParentNode = parentListObj[1].toString();
                break;
            }
        }

        if (parentNode.contains(s.getSSNODEPRNT().trim())) {
            pesan = "sukses";
        } else {
            pesan = "PARENT NODE ISN'T MATCH";
        }

        JSONObject parent = new JSONObject();
        parent.put("pesan", pesan);
        parent.put("parentnode", nameParentNode);
        parent.put("allnode", s);
        parent.put("listparent", listParent);
//        map.put("nameParentNode", nameParentNode);

        return gson.toJson(parent);
    }

//    @ResponseBody
//    @RequestMapping(value = "/GetCurrent", method = RequestMethod.POST)
//    private String getCurrentTree(HttpServletRequest request, @RequestBody String grupnode) throws Exception {
//
//        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(grupnode);
//        String idgrup = jsonoGrupId.getString("grupid");
//        String nodeid = jsonoGrupId.getString("nodeid");
////        String newtree = jsonoGrupId.getString("newnode");
////        int faclvl = Integer.parseInt(jsonoGrupId.getString("facility"));
//
////        Ssltree2 ssltree = new Ssltree2();
////        ssltree.setSSNODEID(newtree);
////        ssltree.setSSLVLFAC(faclvl);
//        List<Object[]> listGrupId = displayNodeDao.getNodeId();
//        List<Object[]> getBefore = treeDao.getBeforeNode(idgrup, nodeid);
//        List getSSMAPID = treeDao.getDesc();
////        System.out.println(getSSMAPID.get(0).toString());
//        System.out.println(getBefore);
//        Object[] parentBefore = getBefore.get(0);
//
//        JSONObject jsonParentBefore = new JSONObject();
//
//        jsonParentBefore.put("parent", parentBefore[0]);
//        jsonParentBefore.put("before", parentBefore[1]);
//
//        JSONObject objGrupId = new JSONObject();
////        System.out.println(getBefore);
//        objGrupId.put("mesaages", jsonParentBefore);
//        objGrupId.put("GrupId", listGrupId);
//        objGrupId.put("id", getSSMAPID.get(0).toString());
////        System.out.println("lolos coy");
//        return gson.toJson(objGrupId);
//
//    }
    //    @ResponseBody
//    @RequestMapping(value = "/GetBefore", method = RequestMethod.POST)
//    private String getBeforeTree(HttpServletRequest request, @RequestBody String grupnode) throws Exception {
//
//        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(grupnode);
//        String idgrup = jsonoGrupId.getString("grupid");
//        String nodeid = jsonoGrupId.getString("nodeid");
////        String newtree = jsonoGrupId.getString("newnode");
////        int faclvl = Integer.parseInt(jsonoGrupId.getString("facility"));
//
////        Ssltree2 ssltree = new Ssltree2();
////        ssltree.setSSNODEID(newtree);
////        ssltree.setSSLVLFAC(faclvl);
//        List<Object[]> listGrupId = displayNodeDao.getNodeId();
//        List<Object[]> getBefore = treeDao.getBeforeNode(idgrup, nodeid);
//        List getSSMAPID = treeDao.getDesc();
////        System.out.println(getSSMAPID.get(0).toString());
//        System.out.println(getBefore);
//        Object[] parentBefore = getBefore.get(0);
//
//        JSONObject jsonParentBefore = new JSONObject();
//
//        jsonParentBefore.put("parent", parentBefore[0]);
//        jsonParentBefore.put("before", parentBefore[1]);
//
//        JSONObject objGrupId = new JSONObject();
////        System.out.println(getBefore);
//        objGrupId.put("mesaages", jsonParentBefore);
//        objGrupId.put("GrupId", listGrupId);
//        objGrupId.put("id", getSSMAPID.get(0).toString());
////        System.out.println("lolos coy");
//        return gson.toJson(objGrupId);
//
//    }
    //    @ResponseBody
//    @RequestMapping(value = "/GetFirst", method = RequestMethod.POST)
//    private String getfirst(@RequestBody String tree) {
//
//        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(tree);
//        String idgrup = jsonoGrupId.getString("grupid");
//        String nodeid = jsonoGrupId.getString("nodeid");
////        String newtree = jsonoGrupId.getString("newnode");
////        int faclvl = Integer.parseInt(jsonoGrupId.getString("facility"));
//
////        Ssltree2 ssltree = new Ssltree2();
////        ssltree.setSSNODEID(newtree);
////        ssltree.setSSLVLFAC(faclvl);
//        List<Object[]> listGrupId = displayNodeDao.getNodeId();
//        List<Object[]> getBefore = treeDao.getBeforeNode(idgrup, nodeid);
//        List getSSMAPID = treeDao.getDesc();
////        System.out.println(getSSMAPID.get(0).toString());
//        System.out.println(getBefore);
//        Object[] parentBefore = getBefore.get(0);
//
//        JSONObject jsonParentBefore = new JSONObject();
//
//        jsonParentBefore.put("parent", parentBefore[0]);
//
//        JSONObject objGrupId = new JSONObject();
////        System.out.println(getBefore);
//        objGrupId.put("mesaages", jsonParentBefore);
//        objGrupId.put("GrupId", listGrupId);
//        objGrupId.put("id", getSSMAPID.get(0).toString());
////        System.out.println("lolos coy");
//        return gson.toJson(objGrupId);
//    }
//    @ResponseBody
//    @RequestMapping(value = "/InsertFirst", method = RequestMethod.POST)
//    private String InsertFirst(@RequestBody String tree) {
//        Ssltree2 ssltree = gson.fromJson(tree, Ssltree2.class);
////        treeDao.saveTree(ssltree);
//        return treeDao.insertFirst(ssltree);
//    }
//    @ResponseBody
//    @RequestMapping(value = "/getLast", method = RequestMethod.POST)
//    private String getLast(@RequestBody String tree) {
//        org.json.JSONObject jsonoGrupId = new org.json.JSONObject(tree);
//        String idgrup = jsonoGrupId.getString("grupid");
//        String nodeid = jsonoGrupId.getString("nodeid");
//        List getSSMAPID = treeDao.getDesc();
////        List<Object[]> listGrupId = displayNodeDao.getNodeId();
//        System.out.println(idgrup + ":" + nodeid);
//        List<String> tempgrup = treeDao.getNodeByParent(nodeid, idgrup);
//
//        int id = Integer.parseInt(getSSMAPID.get(0).toString());
//
//        String gruptemp = "null";
//        if (tempgrup.isEmpty()) {
//            System.out.println("kosong bos");
//        } else {
//            for (String grup : tempgrup) {
//                gruptemp = grup;
//            }
//        }
//        System.out.println("hasil : " + gruptemp);
//        JSONObject param = new JSONObject();
//        JSONObject idnodepr = new JSONObject();
//        idnodepr.put("id", id);
//        idnodepr.put("nodepr", gruptemp.trim());
//        param.put("idnodepr", idnodepr);
////        param.put("GrupId", listGrupId);
//
//        return gson.toJson(param);
//    }
}
