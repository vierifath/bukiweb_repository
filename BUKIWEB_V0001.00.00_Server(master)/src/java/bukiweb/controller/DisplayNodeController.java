/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.dao.DisplayNodeDao;
import bukiweb.model.Sslnode;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dpti
 */
@Controller
public class DisplayNodeController {

    Gson gson = new Gson();

    @Autowired(required = false)
    private DisplayNodeDao displayNodeDao;

    //SSFNODE//
    String SSNODEID = "";
    String SSNODENM = "";
    String SSNODEST = "0";
    String SSNODEPRNT = "";
    String SSNODEVER = "";
    String SSNDIPFUNC = "0";
    String SSFIELD1 = "N";
    String SSFIELD2 = "0";
    String SSFIELD3 = "0";
    String SSFIELD4 = "0";

    //SSFNDSC//
    String SSSECCOD = "security";
    String SSFLD01 = "null";
    String SSFLD02 = "null";
    String SSFLD03 = "null";
    String SSFLD04 = "0";
    String SSFLD05 = "0";

//    @RequestMapping(value = "/NDDIS2", method = RequestMethod.GET)
//    private ModelAndView getNode(HttpServletRequest request, HttpServletResponse response) {
//        
//        
//        
//        PagedListHolder pagedListHolder = (PagedListHolder) request.getSession().getAttribute("profilList");
//        List list = pagedListHolder.getPageList();
//        String menu = (String) request.getSession().getAttribute("menuku");
//
////        System.out.println("Menuku : " + menu);
//        Map map = new HashMap();
//        try {
//            List<Object[]> getAllNodes = displayNodeDao.getAllNode();//Hasil Query di tampung di list getAllNodes
////            for (Object[] allNode : getAllNodes) {
////                System.out.println("Node : " + allNode[0]);
////            }
//
//            PagedListHolder pagedListHolderNode = new PagedListHolder(getAllNodes);
////            System.out.println("PAGE LIST : "+pagedListHolder.getPageList());
//            pagedListHolderNode.setPageSize(50);
//            request.getSession().setAttribute("listnode", pagedListHolderNode);
//            List pageList = pagedListHolderNode.getPageList();
//            map.put("node", pageList);
//            System.out.println(" page : " + pageList.get(0));
//            System.out.println(" jumlah row : " + pagedListHolder.getPageCount());
//        } catch (Exception e) {
//            System.err.println("Error : " + e);
//        }
////        map.put("profil", list);
////        map.put("menuku", menu);
//        map.put("url", "body/Node/viewNode.jsp");
//        return new ModelAndView("body/Node/viewNode", map);
////        return new ModelAndView("dashboard", map);
//    }

    @ResponseBody
    @RequestMapping(value = "/displayNode", method = RequestMethod.GET)
    private String getNode2(HttpServletRequest request, HttpServletResponse response) {
        
      
        
//        System.out.println("sugoooiii");
        Map map = new HashMap();
        List<Object[]> getAllNodes = displayNodeDao.getAllNode();
        JSONObject object = new JSONObject();
        object.put("Node", getAllNodes);
//        System.out.println("obj sugoi :"+object);
        return gson.toJson(object);
    }

    @ResponseBody
    @RequestMapping(value = "/nodeid", method = RequestMethod.GET)
    private String getParent(HttpServletRequest request) {
        
        List<Object[]> listNode = displayNodeDao.getNodeId();
        JSONObject nodeid = new JSONObject();
        nodeid.put("nodeid", listNode);

        return gson.toJson(nodeid);
    }

    @ResponseBody
    @RequestMapping(value = "/NDCHG", method = RequestMethod.POST)
    private String getupdateNode(HttpServletRequest request, @RequestBody String nodeid) {

        Map map = new HashMap();

        org.json.JSONObject idnode = new org.json.JSONObject(nodeid);

        List<Object[]> listParent = displayNodeDao.getParentNode();

        String node = idnode.getString("node");

        Sslnode s = displayNodeDao.getNodeById(node);

        List list = displayNodeDao.getTree(node);
        String pesan;

        if (list.isEmpty() || list.equals("") || list == null) {
            pesan = "sukses";
        } else {
            pesan = "gagal";
        }
        String nameParentNode = "";
        for (Object[] parentListObj : listParent) {
            if (s.getSSNODEPRNT().equals(parentListObj[0])) {
                nameParentNode = parentListObj[1].toString();
                break;
            }
        }

        JSONObject parent = new JSONObject();
        parent.put("pesan", pesan);
        parent.put("parentnode", nameParentNode);
        parent.put("allnode", s);
        parent.put("listparent", listParent);
//        map.put("nameParentNode", nameParentNode);

        return gson.toJson(parent);
    }

    @ResponseBody
    @RequestMapping(value = "/NDCHG2", method = RequestMethod.POST)
    private String updateNode(HttpServletRequest request, @RequestBody String nodetoupdate) {

        System.out.println("ini method update post");
//        org.json.JSONObject updatenode = new org.json.JSONObject(nodetoupdate);
        Sslnode sslnode = gson.fromJson(nodetoupdate, Sslnode.class);
        Map map = new HashMap();
        PesanError pesan = new PesanError();

        try {
            pesan.setKode(displayNodeDao.updateNode(sslnode));
        } catch (Exception e) {
            pesan.setKode(e.getMessage());
        }

        return gson.toJson(pesan);
    }

    @ResponseBody
    @RequestMapping(value = "/NDADD", method = RequestMethod.GET)
    private String insertNode(HttpServletRequest request, HttpServletResponse response) {

        
        List lk = new ArrayList<>();
        List<Object[]> listParent = displayNodeDao.getParentNode();
        JSONObject parent = new JSONObject();

        JSONArray array = new JSONArray();
        lk.add(listParent);
        parent.put("parent", listParent);
//        Map map = new HashMap();

//        map.put("parent", pagedListHolderParent.getPageList());
//        map.put("profil", list);
//        map.put("menuku", menu);
//        map.put("url", "body/Node/formInputNode.jsp");
        return gson.toJson(parent);
//        return new ModelAndView("dashboard", map);
    }

    @ResponseBody
    @RequestMapping(value = "/NDADD1", method = RequestMethod.POST)
    private String getinsertNode(HttpServletRequest request, @RequestBody String node) {
        System.out.println("test sini");

        Sslnode sslnode = gson.fromJson(node, Sslnode.class);
        PesanError pesan = new PesanError();
        Map map = new HashMap();

        pesan.setKode(displayNodeDao.saveNode(sslnode));

        System.out.println("method insert");
        return gson.toJson(pesan);
//        return new ModelAndView("redirect:displaynode");
    }

    @ResponseBody
    @RequestMapping(value = "/NDDEL", method = RequestMethod.POST)
    private String deleteNode(HttpServletRequest request, @RequestBody String nodeid) {

        Map map = new HashMap();

        org.json.JSONObject idnode = new org.json.JSONObject(nodeid);

        String node = idnode.getString("node");

        List<Object[]> listParent = displayNodeDao.getParentNode();

        Sslnode s = displayNodeDao.getNodeById(node);

        String nameParentNode = "";
        for (Object[] parentListObj : listParent) {
            if (s.getSSNODEPRNT().equals(parentListObj[0])) {
                nameParentNode = parentListObj[1].toString();
                break;
            }
        }

        JSONObject parent = new JSONObject();
        parent.put("parentnode", nameParentNode);
        parent.put("allnode", s);
        parent.put("listparent", listParent);
//        map.put("nameParentNode", nameParentNode);

        return gson.toJson(parent);

    }

    @ResponseBody
    @RequestMapping(value = "/NDDEL1", method = RequestMethod.POST)
    private String deletegetNode(HttpServletRequest request, @RequestBody String nodetoupdate) {

//            displayNodeDao.deleteNode(node);
        System.out.println("ini method update post");
//        org.json.JSONObject updatenode = new org.json.JSONObject(nodetoupdate);
        org.json.JSONObject idnode = new org.json.JSONObject(nodetoupdate);

        String node = idnode.getString("SSNODEID");
        Map map = new HashMap();
        PesanError pesan = new PesanError();

        try {
            pesan.setKode(displayNodeDao.deleteNode(node));
        } catch (Exception e) {
            pesan.setKode(e.getMessage());
        }

        return gson.toJson(pesan);
    }
}
