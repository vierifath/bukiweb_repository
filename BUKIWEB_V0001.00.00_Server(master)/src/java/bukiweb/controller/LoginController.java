/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.dao.LoginDao;
import bukiweb.dao.MenuInfDao;
import bukiweb.dao.ProfileDao;
import bukiweb.dao.ReleaseUserDao;
import bukiweb.entity.data2000.CLFPRG01;
import bukiweb.entity.data2000.TBFILE;
import bukiweb.entity.datauser.SSFUSER;
import bukiweb.entity.datauser.SSFUSERPRF;
import bukiweb.model.Sslgroup;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jdt.internal.compiler.batch.Main;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Gustiana
 */
@Controller
public class LoginController extends MenuController {

    private Gson gson = new Gson();

    boolean logonvalid = false;
    private String SysUserId = "";
    private String SysPassword = "";
    private String IPServer = "";
//    public String Username;
//    public static String Password;

    final static Logger logger = Logger.getLogger(Main.class.getName());
    static FileHandler fh;
    String hari = "";
    String jam = "";

    List<SSFUSER> getUsername;

    @Autowired
    private LoginDao loginDao;
    @Autowired
    private ProfileDao profileDao;
    @Autowired
    private MenuInfDao menuInfDao;
    @Autowired
    private ReleaseUserDao releaseUserDao;

    public void getPropValues() {
        String result = "";
        Properties prop = new Properties();

        String propFileName = "/bukiweb/conf/jdbc.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }

        // get the property value and print it out
        IPServer = prop.getProperty("url");
        SysUserId = prop.getProperty("username");
        SysPassword = prop.getProperty("password");
//
//        System.out.println("IPSERVER : " + IPServer);
//        System.out.println("USERNAME : " + SysUserId);
//        System.out.println("PASSSWORD : " + SysPassword);

    }

    public void getPropLog() {

        Date date = new Date();
        DateFormat Formathari = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat Formatjam = new SimpleDateFormat("HH:mm:ss");
        hari = Formathari.format(date);
        jam = Formatjam.format(date);

    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    private String getLogout(HttpServletRequest request, @RequestBody String username) throws Exception {
        System.out.println("log out post");
        org.json.JSONObject userpass = new org.json.JSONObject(username);
        String user = userpass.getString("username");

        System.out.println(user);

        loginDao.getLogout(user);

        return "Sukses";
    }

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    private String getOpening(HttpServletRequest request) {

        String Username = null;
        String Password = null;
        int clcd = 0;
        String userid = null;
        
        

        CLFPRG01 clfprg01;
        SSFUSER ssfuser;
        PesanError pesan = new PesanError();
        JSONObject obj = new JSONObject();

        try {
            List<Object[]> getCLCRDT = loginDao.getAll();
            clcd = getCLCRDT.indexOf(0);
        } catch (Exception e) {
            clcd = 1;
            pesan.setKode("500");
            pesan.setError(e.getMessage());
            System.err.println("ERROR : " + e.getMessage());
//            return gson.toJson(pesan);
        }
        obj.put("clcd", clcd);
        return gson.toJson(obj);

    }

    private int getMacAdd(String user, String mac, String ip) {

        int ret = 1;
        List<Object[]> list = loginDao.getMacAdd(user);
        if (!list.isEmpty()) {
            Object[] listMacIp = list.get(0);
            System.out.println("ini mac address : " + listMacIp[0]);
            System.out.println("ini mac address : " + mac);
            if (listMacIp[0] != null || !listMacIp[0].equals("")) {
                String tempMacadd = listMacIp[0].toString().trim();
                if (tempMacadd.equals(mac)) {
                    ret = 1;
                } else {
                    ret = 0;
                }

            }
            if (ret != 1) {
                System.out.println("ini ip address : " + listMacIp[1]);
                System.out.println("ini ip address : " + ip);
                if (listMacIp[1] != null || !listMacIp[1].equals("")) {
                    String tempIpadd = listMacIp[1].toString().trim();
                    if (tempIpadd.equals(ip)) {
                        ret = 1;
                    } else {
                        if (ret == 0) {
                            ret = 3;
                        } else {
                            ret = 2;
                        }
                    }

                }
            }
            System.out.println("ini status : " + listMacIp[2]);
            if (listMacIp[2] != null || !listMacIp[2].equals("")) {
                String tempSts = listMacIp[2].toString().trim();
                if (!tempSts.equals("0")) {
                    ret = 1;
                }

            }

        }

        return ret;

    }

    @ResponseBody
    @RequestMapping(value = "/loginPost", method = RequestMethod.POST)
    private String getLogin(HttpServletRequest request, @RequestBody String login) throws IOException, Exception {
        Map map = new HashMap();
        org.json.JSONObject userpass = new org.json.JSONObject(login);
        String username = userpass.getString("username");
        String password = userpass.getString("password");
        String mac = userpass.getString("mac");
        String ip = userpass.getString("ip");
        String Grp_Id="";

        String ret = null;
        PesanError pesan = new PesanError();
        SSFUSER ssfuser;
        SSFUSERPRF ssfuserprf=null;
        int SSSTSLGN = 0;
        String user = null;
        getPropValues();
//        System.out.println("web service");
//        System.out.println("Mlebu neng method login post");

        username = username;
        password = password;

        try {
            //Dekripsi
            Dekripsi dk = new Dekripsi();
            username = dk.decrypt(username);
            password = dk.decrypt(password);
//            //-----------------
//            System.out.println("test2 : " + Username);
//            System.out.println("test2 : " + Password);
            AS400 as400 = new AS400(IPServer, SysUserId, SysPassword);

            as400.setGuiAvailable(false);
            logonvalid = as400.validateSignon(username, password);
//            System.out.println("IPSERVER = " + IPServer + ", USER = " + SysUserId + ", Pass = " + SysPassword);

            if (logonvalid) {
                as400.disconnectAllServices();

                TBFILE tbfile = null;
                getUsername = loginDao.getLogin(username);

                ssfuser = getUsername.get(0);
                SSSTSLGN = ssfuser.getSSSTSLGN();
                Grp_Id=ssfuser.getSSGRPID();
                //hapus besok
//                SSSTSLGN=1;
                //==============
                int macip = getMacAdd(username, mac, ip);
//                int macip = 1;
                if (macip == 1) {
                    return getBeranda(request,username,Grp_Id,ssfuserprf,ssfuser);
//                    if (SSSTSLGN == 1) {
//                        loginDao.Logout(Username);
//                    } else {
//                        ret = "USER LOGIN IN ANOTHER APPLICATION";
//                        pesan.setError("USER LOGIN IN ANOTHER APPLICATION");
//                    }
                } else if (macip == 0) {
                    ret = "YOUR CANNOT LOGIN USING THIS USER IN THIS PC";
                } else if (macip == 2) {
                    ret = "THIS USER HAS REGISTERED WITH ANOTHER IP ADDRESS";
                } else if (macip == 3) {
                    ret = "YOUR MAC ADDRESS AND IP ADDRESS NOT REGISTERED";
                } else if (macip == 4) {
                    ret = "STATUS THIS USER IS NON ACTIVE";
                }

            } else {
                pesan.setKode("404");
                pesan.setError("WRONG USERNAME OR PASSWORD");
                ret = "WRONG USERNAME OR PASSWORD";
            }

        } catch (PropertyVetoException | AS400SecurityException | IOException e) {
            pesan.setKode("404");
            pesan.setError(e.getMessage());
            ret = e.getMessage();
        }
        return ret;
    }

    private String getBeranda(HttpServletRequest request,String UserID, String GrpID,SSFUSERPRF ssfuserprf ,SSFUSER ssfuser) throws IOException {

        String msg = "";
        Map map = new HashMap();
        PesanError pesan = new PesanError();
//        SSFUSER ssfuser = null;
        int SSSTSLGN = 0;
        String user = null;
        ArrayList<String> list = new ArrayList<>();
        getPropLog();
        System.out.println("Mlebu neng Dashboard");

        try {
            List<Object[]> getCLCRDT = loginDao.getAll();
            int clcd = getCLCRDT.indexOf(0);
            System.out.println("masuk sini co");
//            ssfuser = getUsername.get(0);
//            user = ssfuser.getSSUIDSGN();
            user = UserID.trim();
//            SSFUSERPRF ssfuserprf;
            ssfuserprf = profileDao.getProfilUser(user);
            list.add(ssfuserprf.getUNAME());
            list.add(ssfuserprf.getUIDSGN());
//            List<Sslgroup> getGroupById = menuInfDao.getGroupById(ssfuser.getSSGRPID());
            List<Sslgroup> getGroupById = menuInfDao.getGroupById(GrpID);
            msg = "User id using invalid group or group is not available";
            Sslgroup sslgroup = getGroupById.get(0);
            list.add(sslgroup.getSSGRPNM());
            System.out.println("SSGRPNM : " + sslgroup.getSSGRPNM());
            List<Object[]> getBranch = profileDao.getProfilBranch(user);
            msg = "User id  do not have branch";
            System.out.println("brch : " + getBranch.get(0));
//            PagedListHolder listHolderBranch = new PagedListHolder(getBranch);
            List<Object[]> getLoc = profileDao.getProfilLoc(user);
            Object[] arrBranch = getBranch.get(0);
            msg = "User id  do not have loc";
            Object[] arrLoc = getLoc.get(0);
            System.out.println("branch : " + arrBranch[1]);
            System.out.println("loc : " + arrLoc[1]);
            list.add(StringUtils.right(arrBranch[1].toString(), 30));
            list.add(StringUtils.right(arrLoc[1].toString(), 27));
            
            list.add(hari);
            list.add(jam);
            list.add(sslgroup.getSSGRPID());
            list.add(String.valueOf(ssfuserprf.getUBATID()));
            list.add(arrBranch[0].toString());
            list.add(arrLoc[0].toString());
            list.add(String.valueOf(ssfuserprf.getDPCODU()));
            list.add(String.valueOf(ssfuserprf.getUBU()));
            list.add(String.valueOf(ssfuserprf.getUBATID()));
            list.add(String.valueOf(ssfuserprf.getULIMO()));
            list.add(String.valueOf(ssfuserprf.getULIMI()));
            list.add(String.valueOf(ssfuserprf.getULORAK()));
            list.add(String.valueOf(ssfuserprf.getULIRAK()));
            list.add(String.valueOf(ssfuserprf.getUKAS()));
            list.add(String.valueOf(ssfuser.getSSSTSFE()));
            list.add(String.valueOf(ssfuser.getSSSTSNONFE()));
            list.add(String.valueOf(clcd));
            list.add(String.valueOf(ssfuserprf.getFILER2()));
            System.out.println("list prof ="+list);
            String temp = getLogin(ssfuserprf.getUIDSGN().trim());
            
//            String temp = getLogin(user);
            JSONObject listProfil = new JSONObject();

            System.out.println("username : " + list.get(0));
            System.out.println("userlogin : " + list.get(1));
            System.out.println("groupname : " + list.get(2));
            System.out.println("cabang : " + list.get(3));
            System.out.println("lokasi : " + list.get(4));
            System.out.println("logindate : " + list.get(5));
            System.out.println("loginname : " + list.get(6));
            System.out.println("grupid : " + list.get(7));
            System.out.println("ubatid : " + list.get(8));
            System.out.println("ubatid : " + list.get(19));

            listProfil.put("username", list.get(0));
            listProfil.put("userlogin", list.get(1).trim().replace(" ", ""));
            listProfil.put("groupname", list.get(2));
            System.out.println("list.get(3) ="+list.get(3));
            listProfil.put("cabang", list.get(3));
            listProfil.put("lokasi", list.get(4));
            listProfil.put("logindate", list.get(5));
            listProfil.put("loginname", list.get(6));
            listProfil.put("groupid", list.get(7));
            listProfil.put("ubatid", list.get(8));
            listProfil.put("cabangCode", list.get(9));
            listProfil.put("lokasiCode", list.get(10));
            listProfil.put("dppcode", list.get(11));
            listProfil.put("bu", list.get(12));
            listProfil.put("ubatid", list.get(13));
            listProfil.put("ulimo", list.get(14));
            listProfil.put("ulimi", list.get(15));
            listProfil.put("ulorak", list.get(16));
            listProfil.put("ulirak", list.get(17));
            listProfil.put("ukas", list.get(18));
            listProfil.put("fe", list.get(19));
            listProfil.put("nonfe", list.get(20));
            listProfil.put("clcd", list.get(21));
            listProfil.put("path", getPic(ssfuserprf.getUIDSGN().trim()));
            listProfil.put("nik", list.get(22));
            
            JSONArray array = new JSONArray();

            array.add(listProfil);

            JSONObject listDashboard = new JSONObject();

            listDashboard.put("profil", array);
            listDashboard.put("menu", temp);
//            listDashboard.put("menu", "rustanianmenuku");

            return gson.toJson(listDashboard);
        } catch (Exception e) {

            pesan.setKode(e.toString());
            pesan.setError(e.getMessage());
            return gson.toJson(pesan);
        }
    }

    public String getPath() {

        Properties prop = new Properties();
        List list = new ArrayList();

        String propFileName = "/bukiweb/conf/path.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }

        return prop.getProperty("path");
    }

    public String getPic(String pathPic) throws IOException {

        String defPath = getPath();

        String path = "../img/userWhite.jpg";
        System.out.println("path : " + pathPic);
        File file = new File(defPath + pathPic + "/" + pathPic + ".pic");
        if (!file.exists()) {
            new File(defPath + pathPic).mkdir();
            try (PrintWriter out = new PrintWriter(new BufferedWriter(
                    new FileWriter(defPath + pathPic + "/" + pathPic + ".pic", false)))) {
                out.println(path);
            }
        }
        try (BufferedReader br = new BufferedReader(new FileReader(defPath + pathPic + "/" + pathPic + ".pic"))) {
            String sCurrentLine;
            String abc[];
            while ((sCurrentLine = br.readLine()) != null) {
                path = sCurrentLine;

            }
        } catch (IOException e) {
            path = "../img/userWhite.jpg";
            System.out.println("e : " + e.getMessage());
        }
        return path;
    }

    @ResponseBody
    @RequestMapping(value = "/change", method = RequestMethod.POST)
    private String changePass(HttpServletRequest request, @RequestBody String changepass) throws Exception {

        Map map = new HashMap();
        String pesan = null;

        System.out.println("ini web service");
        org.json.JSONObject pass = new org.json.JSONObject(changepass);
        
        String Username= pass.getString("username");
        String Oldpass = pass.getString("Oldpassword");
        String newPassword = pass.getString("password");
//        String confirmPassword = pass.getString("confirmnewpassword");

        try {

            AS400 as400 = new AS400(IPServer, SysUserId, SysPassword);
            as400.setGuiAvailable(false);
            as400.setUserId(Username);
            as400.changePassword(Oldpass, newPassword.trim());
            System.out.println(newPassword.trim());
            System.out.println(newPassword + ";");

            pesan = "Sukses";
            System.out.println(Username);

        } catch (PropertyVetoException | AS400SecurityException | IOException e) {
            pesan = e.getMessage();
            System.out.println("ini error web service : " + pesan);
        }
        return pesan;

    }

    @ResponseBody
    @RequestMapping(value = "/getUserVacility", method = RequestMethod.POST)
    private String getUserVacility(HttpServletRequest request, @RequestBody String param) {
        System.out.println("tes");
        String pesan = null;

        org.json.JSONObject pass = new org.json.JSONObject(param);

        String groupid = pass.getString("groupid");
        String nodid = pass.getString("nodid");

        try {
            List<Object[]> results = menuInfDao.getUserVacility(groupid, nodid);
            System.out.println(Arrays.toString(results.get(0)));
//            
            pesan = String.valueOf(results.get(0));
        } catch (Exception e) {
            System.out.println("err : " + e.getMessage());
            pesan = e.getMessage();
        }
        return pesan;

    }

    @ResponseBody
    @RequestMapping(value = "/getAllUser", method = RequestMethod.POST)
    private String getAllUser(HttpServletRequest request, @RequestBody String json) {

//     
        org.json.JSONObject jsono = new org.json.JSONObject(json);
        String userlogin = jsono.getString("userlogin");
        String lvlfac = jsono.getString("lvlfac");
        JSONObject object = new JSONObject();
//        List<Object[]> getGroupId = releaseUserDao.getUserFacLevel(idgrup, nodeid);
//        for (Object[] resultn : getGroupId) {
//                temp = (Integer) resultn[0];
//            }
        try {
            List<Object[]> results = new ArrayList();
            if (lvlfac.equals("1")) {
                results = releaseUserDao.getAllUserChangePass(userlogin);
            } else {
                results = releaseUserDao.getAllUserChangePassBrcnh(userlogin,lvlfac);
                
            }
            String alluser = "";

            alluser += "<select id=\"nodeid\" onchange=\"loadReleaseA(this.value)\" class=\"select2_demo_3 form-control m-b\" name=\"userlogin\">";
            alluser += "<option>--PILIH USER ID--</option>";
            for (Object[] resultn : results) {
                alluser += "<option value=\"" + resultn[0] + "/" + resultn[1] + "\">" + resultn[0].toString().trim() + " / " + resultn[1].toString().trim() + "</option>";
            }
            alluser += "</select>";
            object.put("alluser", alluser);
        } catch (Exception e) {
            System.out.println("err : " + e.getMessage());
        }
        return gson.toJson(object);

    }

    private String LibProg;
    private String ProgName;

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();
        Properties prop = new Properties();

        String propFileName = "/bukiweb/conf/jdbc.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }

//        List list = pc.prop(); 
        ProgName = pc.prop();

        IPServer = prop.getProperty("url");
        SysUserId = prop.getProperty("username");
        SysPassword = prop.getProperty("password");
//        LibProg = (String) list.get(0);
//        ProgName = (String) list.get(1);

        System.out.println("URL : " + ProgName);
    }

    @ResponseBody
    @RequestMapping(value = "/chgdefpass", method = RequestMethod.POST)
    private String changeDefPass(HttpServletRequest request, @RequestBody String chg) throws Exception {

        Dekripsi dekripsi = new Dekripsi();
        org.json.JSONObject jsonoParam = new org.json.JSONObject(chg);
        getPropUrl();
        String message;
//        String username = jsonoParam.getString("username");
        String userid = jsonoParam.getString("userid");
        String password = jsonoParam.getString("password");
        password = dekripsi.decrypt(password);
//        String password = jsonoParam.getString("password");
        String aksi = "3";

//        System.out.println(userid + ":" + aksi);
        try {
            //-----------------

            AS400 as400 = new AS400(IPServer, SysUserId, SysPassword);
            as400.setGuiAvailable(false);
            logonvalid = as400.validateSignon(SysUserId, SysPassword);
//            System.out.println("IPSERVER = " + IPServer + ", USER = " + SysUserId + ", Pass = " + SysPassword);

            if (logonvalid) {
                ProgramCall program = new ProgramCall(as400);

                AS400Text userId = new AS400Text(10, as400);
                AS400Text act = new AS400Text(2, as400);
                AS400Text pass = new AS400Text(10, as400);
                AS400Text msg = new AS400Text(50, as400);

                ProgramParameter[] parameterlist = new ProgramParameter[4];

                parameterlist[0] = new ProgramParameter(userId.toBytes(StringUtils.rightPad(userid, 10, " ")));
                parameterlist[1] = new ProgramParameter(act.toBytes(StringUtils.rightPad(aksi, 2, " ")));
                parameterlist[2] = new ProgramParameter(pass.toBytes(StringUtils.rightPad(password, 10, " ")));
                parameterlist[3] = new ProgramParameter(50);

                System.out.println("sukses");

                program.setProgram(ProgName, parameterlist);

                if (program.run() != true) {
                    AS400Message[] messageList = program.getMessageList();
                    for (int i = 0; i < messageList.length; ++i) {
                        messageList[i].getText();
                        messageList[i].load();
                        messageList[i].getHelp();
                    }
                    message = Arrays.toString(messageList);
                    System.out.println("Error : " + Arrays.toString(messageList));
                } else {
                    String pesan = (String) msg.toObject(parameterlist[3].getOutputData());
                    System.out.println("isi pesan sukses : " + Arrays.toString(parameterlist[3].getOutputData()));
                    System.out.println("isi pesan : " + pesan);
                    pesan = pesan.trim();
                    if (pesan.equals("")) {
                        message = "Sukses";
                    } else {
                        message = "EROR : " + pesan;
                    }

                }
            } else {
                message = "ERROR";
            }

        } catch (ObjectDoesNotExistException | InterruptedException | ErrorCompletingRequestException | PropertyVetoException | AS400SecurityException | IOException e) {
            message = e.getMessage() + ":" + Arrays.toString(e.getStackTrace());
            System.out.println(message);
        }
        return gson.toJson(message);

    }

    @ResponseBody
    @RequestMapping(value = "/cekUserEnable", method = RequestMethod.POST)
    private String cekUserEnable(HttpServletRequest request, @RequestBody String login) throws IOException, Exception {
        System.out.println("sampe ");
        Map map = new HashMap();
        org.json.JSONObject userpass = new org.json.JSONObject(login);
        JSONObject object = new JSONObject();
        String username = userpass.getString("username");
        String password = userpass.getString("password");
        System.out.println("sampe2 ");

        String ret = null;

        int SSSTSLGN = 0;
        String user = null;
        getPropValues();
        username = username;
        password = password;

        try {
            //Dekripsi
            Dekripsi dk = new Dekripsi();
//            Username = dk.decrypt(Username);
            password = dk.decrypt(password);
//            //-----------------

            AS400 as400 = new AS400(IPServer, SysUserId, SysPassword);
            as400.setGuiAvailable(false);
            logonvalid = as400.validateSignon(username, password);
            if (logonvalid) {

                as400.disconnectAllServices();
                object.put("errcode", "0");
                object.put("errmsg", "sukses");
                ret = "0";
            } else {
                System.out.println("sampe5 ");
                object.put("errcode", "1");
                object.put("errmsg", "WRONG USERNAME OR PASSWORD");

            }

        } catch (PropertyVetoException | AS400SecurityException | IOException e) {
            System.out.println("Err " + e.getMessage());
            object.put("errcode", "99");
            object.put("errmsg", e.getMessage());
        }
        return gson.toJson(object);
    }

}
