/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author HP
 */
@Controller
public class ChangePictureController {

    Gson gson = new Gson();

    public String getPath(){
        
        Properties prop = new Properties();
        List list = new ArrayList();

        String propFileName = "/bukiweb/conf/path.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }
        
        return prop.getProperty("path");
    }
    
    @ResponseBody
    @RequestMapping(value = "/savePicture", method = RequestMethod.POST)
    private String savePicture(HttpServletRequest request, @RequestBody String param) throws IOException {

        String defPath = getPath();
        
        org.json.JSONObject jsonoParam = new org.json.JSONObject(param);
        String userlogin = jsonoParam.getString("username");
        String path = jsonoParam.getString("path");
        int idx = Integer.parseInt(jsonoParam.getString("idx"));
        String existPath = "";
        String ret = "";
        try {
            String pathPic = userlogin + ".pic";
            new File(defPath + userlogin).mkdir();
            File fileDir = new File(defPath + userlogin + "/" + pathPic);
            File file = new File(defPath + userlogin + "/" + pathPic);
            
            if (file.exists()) {
                int count = (int) Files.list(Paths.get(defPath + userlogin)).count();
                String newPath = defPath + userlogin + "/" + userlogin + "B" + count + ".pic";
                if (idx != 0) {
                    File f = new File(defPath + userlogin + "/" + userlogin + "B" + idx + ".pic");
                    boolean delete = f.delete();
                    System.out.println("result delete : " + delete);
                    newPath = defPath + userlogin + "/" + userlogin + "B" + idx + ".pic";
                } else {
                    if (count == 8) {
                        for (int i = 1; i < 7; i++) {
                            File file2 = new File(defPath + userlogin + "/" + userlogin + "B" + i + ".pic");
                            boolean delete = file2.delete();
                            System.out.println("result delete : " + delete);
                            File file1 = new File(defPath + userlogin + "/" + userlogin + "B" + (i + 1) + ".pic");
                            boolean success = file1.renameTo(file2);
                            System.out.println("result rename : " + success);
                        }
                        newPath = defPath + userlogin + "/" + userlogin + "B" + (count - 1) + ".pic";
                    }
                }
                File file1 = new File(newPath);
                boolean success = file.renameTo(file1);
                System.out.println("count : " + count);
                List listPath = new ArrayList();

//                for (int i = 1; i < count; i++) {
//                    try (BufferedReader br = new BufferedReader(new FileReader(defPath + userlogin + "/" + userlogin + "B" + i + ".pic"))) {
//                        String sCurrentLine;
//                        String abc[];
//                        while ((sCurrentLine = br.readLine()) != null) {
//                            existPath = sCurrentLine;
//                        }
//                        if (path.contains(existPath)) {
//                            newPath = defPath + userlogin + "/" + userlogin + "B" + i + ".pic";
//                            System.out.println("sami sareng");
//                            br.close();
//                            File fClose = new File(newPath);
//                            fClose.compareTo(fClose);
//
//                        }
//                        System.out.println("B" + i + " : " + existPath);
//                        br.close();
//                        br.reset();
//                        br.wait(1);
//                    } catch (IOException e) {
//                        System.out.println("e : " + e.getMessage());
//                    }
//                }
//                File file1 = new File(newPath);
//                boolean success = file.renameTo(file1);
                System.out.println("result rename : " + success);
            }
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new FileWriter(defPath + userlogin + "/" + pathPic, false)));
            out.println(path);
            out.close();

            ret = "sukses";
        } catch (Exception e) {
            System.err.println("e : " + e);
            ret = e.getMessage();
        }

        return gson.toJson(ret);
    }

    public static void main(String[] args) {
//        for (int i = 1; i < 7; i++) {
//            File file2 = new File("D://pic/BK20151101/BK20151101B" + i + ".pic");
//            boolean delete = file2.delete();
//            System.out.println("result delete : " + delete);
//            File file1 = new File("D://pic/BK20151101/BK20151101B" + (i + 1) + ".pic");
//            boolean success = file1.renameTo(file2);
//            System.out.println("result rename : " + success);
//        }
        new File("D://pic/BK20151102").mkdir();
    }

    @ResponseBody
    @RequestMapping(value = "/getPicture", method = RequestMethod.POST)
    private String getPicture(HttpServletRequest request, @RequestBody String param) throws IOException {

        String defPath = getPath();
        
        org.json.JSONObject jsonoParam = new org.json.JSONObject(param);
        String userlogin = jsonoParam.getString("username");
        String path = "";
        String pathDef = "../img/userWhite.jpg";
        int count = (int) Files.list(Paths.get(defPath + userlogin)).count();
        System.out.println("count : " + count);
        List listPath = new ArrayList();
        for (int i = 1; i < count; i++) {
            try (BufferedReader br = new BufferedReader(new FileReader(defPath + userlogin + "/" + userlogin + "B" + i + ".pic"))) {
                String sCurrentLine;
                String abc[];
                while ((sCurrentLine = br.readLine()) != null) {
                    path = sCurrentLine;

                }
                System.out.println("B" + i + " : " + path);
                listPath.add(path);
            } catch (IOException e) {
                System.out.println("e : " + e.getMessage());
            }
        }
//        listPath.add(pathDef);
        JSONObject jsonoPath = new JSONObject();
        jsonoPath.put("listPath", listPath);
        jsonoPath.put("count", count);
        return gson.toJson(jsonoPath);
    }

}
