/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.json.simple.JSONObject;

/**
 *
 * @author Gustiana
 */
public class PropertiesController {

    private String LibPrg;
    private String PrgName;
    private Gson gson = new Gson();
    
    
    public String prop() {
        String defUrl;
        String result = "";
        Properties prop = new Properties();
        List list = new ArrayList();

        String propFileName = "/bukiweb/conf/program.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }
        
        JSONObject object = new JSONObject();

        // get the property value and print it out
        LibPrg = prop.getProperty("LibPrg");
        PrgName = prop.getProperty("PrgName");

        list.add(LibPrg);
        list.add(PrgName);
        
        object.put("LibPrg", LibPrg);
        object.put("PrgName", PrgName);
        String prog = LibPrg + PrgName;
//        System.out.println("URL : " + defUrl);
        return prog;
    }
    
    public static void main(String[] args) {
        PropertiesController pc = new PropertiesController();
        System.out.println(pc.prop());
    }
}
