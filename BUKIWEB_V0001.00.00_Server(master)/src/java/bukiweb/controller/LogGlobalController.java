/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.dao.logGlobalInfDao;
import bukiweb.model.Llfglobal;
import bukiweb.model.LlfglobalId;
import bukiweb.model.Ssltree2;
import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Rustanian
 */
@Controller
public class LogGlobalController {

    private Gson gson = new Gson();


    @Autowired
    private logGlobalInfDao globalInfDao;

    @ResponseBody
    @RequestMapping(value = "/logGlobal", method = RequestMethod.POST)
    private String InsertAfter(HttpServletRequest request, @RequestBody String param) throws Exception {
        String pesan="";
        String lluser = "";
        String lltrmid = "";
        String lldate = "";
        String lltime = "";
        String llmodul = "";
        String llappnm = "";
        String llappfunc = "";
        String llmode = "";
        String lldata1 = "";
        String lldata2 = "";
        String lldata3 = "";
        String lldata4 = "";
        String lldata5 = "";
        String lldata6 = "";
        String lldata7 = "";
        String lldata8 = "";
        String llerrmsg = "";

        try {
            JSONObject paramvalue = new JSONObject(param);
            System.out.println(paramvalue + "test");
//            org.json.JSONObject paramvalue2 = new org.json.JSONObject(paramvalue.getString("param"));
//            System.out.println(paramvalue2 + "test2");
//            JSONObject paramvalue2 =  paramvalue.getJSONObject("param");
//            System.out.println(paramvalue2+"tes2");
//            String prm = paramvalue2.toString();
//            String prm = (paramvalue.getString("param")).replace("//", "");
            String prm = (paramvalue.getString("param"));
            System.out.println(prm+"tes3");

            LlfglobalId llfglobalId = new LlfglobalId();
            Llfglobal llfglobal = new Llfglobal();

            if (prm.length() >= 500) {
                lldata2 = prm.substring(500, prm.length());
                prm = prm.substring(0, 500);
                if (lldata2.length() > 500) {
                    lldata3 = lldata2.substring(1000, 500);
                    lldata2 = lldata2.substring(0, 500);
                }
            }

            llfglobalId.setLlappfunc(paramvalue.getString("application_function"));
            llfglobalId.setLlappnm(paramvalue.getString("application_name"));
            llfglobalId.setLldata1(prm);
            llfglobalId.setLldata2(lldata2);
            llfglobalId.setLldata3(lldata3);
            llfglobalId.setLldata4(lldata4);
            llfglobalId.setLldata5(lldata5);
            llfglobalId.setLldata6(lldata6);
            llfglobalId.setLldata7(lldata7);
            llfglobalId.setLldata8(lldata8);
            llfglobalId.setLldate(paramvalue.getString("date"));
            llfglobalId.setLlerrmsg(paramvalue.getString("msg"));
            llfglobalId.setLlmode(paramvalue.getString("mode"));
            llfglobalId.setLlmodul(paramvalue.getString("module"));
            llfglobalId.setLltime(paramvalue.getString("time"));
            llfglobalId.setLltrmid(paramvalue.getString("terminal"));
            llfglobalId.setLluser(paramvalue.getString("user"));

            llfglobal.setId(llfglobalId);

//        String pesan = treeDao.saveTree(ssltree);
             pesan = globalInfDao.saveLog(llfglobal);
             if(pesan.equals("sukses")){
                 pesan="{\"errcode\":\"0\",\"msg\":\""+pesan+"\"}";
             }
        } catch (Exception e) {
            pesan=e.getMessage();
            System.out.println("pesan : "+pesan);
            pesan="{\"errcode\":\"1\",\"msg\":\""+pesan+"\"}";
        }

        return gson.toJson(pesan);

    }
}
