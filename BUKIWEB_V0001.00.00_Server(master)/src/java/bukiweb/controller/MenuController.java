/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.model.Ssfuser;
import bukiweb.model.Sslgroup;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import bukiweb.dao.MenuInfDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Rustanian
 */
@Controller
public class MenuController {

    @Autowired(required = false)
    private MenuInfDao menuInfDao;

//    private String menuku = "";
//    private String groupid = "";
//    private String appName = "";
//    private String userid = "";
//    List<Object[]> resultsx;

    protected String getLogin(String id) throws Exception {
        // HttpServletRequest request = null;
        int clcd;
        
        String nama = "";
        Map map = new HashMap();
        String asal = "null";
        String temp = "null";
        String node = "";
        String menuku = "";
        String User_Id="";
        String Grp_Id="";
        String appName = "";
        List<Object[]> resultsx;

//        pagedListHolder.
        Sslgroup sslgroup;
        Ssfuser ssfuser;
        Ssltree2 ssltree2;
        Sslnode sslnode;

        //System.out.println("Test Tendi : ");
        try {
            
            List<Ssfuser> getIdgroup = menuInfDao.getGroupId(id);
            User_Id = id.trim();
            ssfuser = getIdgroup.get(0);
            Grp_Id = ssfuser.getSSGRPID().trim();
            List<Object[]> results=null;
            List<Object[]> results2=null;
            
            results = menuInfDao.getTreeParent(Grp_Id);
            results2 = menuInfDao.getTreeChild(Grp_Id);
            resultsx = results2;
            int a = 0;
            for (Object[] result : results) {
//                results2.remove(a);
                for (Object[] resultn : results) {

                    String test = (String) resultn[3];
                    String prev = (String) resultn[4];
                    node = (String) resultn[2];
                    test = test.trim();
                    prev = prev.trim();
                    node = node.trim();
                    if (test.equals("null") && prev.equals(asal)) {
//                    System.out.println("masuk sini");

                        menuku += "<li>";
                        menuku += "<a href=\"#\" ><i class=\"fa fa-sitemap\"></i> <span class=\"nav-label\">";
                        menuku += ((String) resultn[6]).trim();
                        appName = ((String) resultn[6]).trim().replace(" ", "%20");
                        menuku += "</span><span class=\"fa arrow\"></span></a>";
                        String papah = (String) resultn[2];
                        papah = papah.trim();
                        menuku +=loopMenu(resultsx, papah,appName,User_Id);
                        menuku += "</li>";
                        temp = node;

                    }
                }
                asal = temp;
                a++;
            }
            return menuku;
        } catch (Exception e) {

            System.out.println("gagal : " + e.getMessage());
            return "error";
        }

    }

    public String loopMenu(List<Object[]> results, String parent, String appName,String userid) {

//        System.out.println(parent); nodepr=8
        String[] baskom = new String[results.size()];
        ArrayList<String> al = new ArrayList<>();
        
//        resultsx = results;
//        
        for (int i = 0; i < results.size(); i++) {
            Object[] result = results.get(i);
            String test = (String) result[3];
            test = test.trim();
            if (test.equals(parent)) {
                al.add(((String) result[2]).trim() + "|" + ((String) result[3]).trim() + "|" + ((String) result[6]).trim() + "|" + ((String) result[7]).trim() + "|" + ((String) result[4]).trim() + "|" + i+ "|" +result[8]+ "|" +result[9]+ "|" +result[10]+ "|" +result[11]+ "|" +result[12]+ "|" +result[5]);
//                resultsx.remove(i);
            }

        }
        return findprev(al, results,appName, userid);
    }

    public String loopMenu2(List<Object[]> results, String parent,String appName,String userid) {
        String[] baskom = new String[results.size()];
        ArrayList<String> al = new ArrayList<>();

//        System.out.println("loopmenu : " + resultsx.size());
        for (int i = 0; i < results.size(); i++) {
            Object[] result = results.get(i);
            String test = (String) result[3];
            test = test.trim();
            if (test.equals(parent)) {
                al.add(((String) result[2]).trim() + "|" + ((String) result[3]).trim() + "|" + ((String) result[6]).trim() + "|" + ((String) result[7]).trim() + "|" + ((String) result[4]).trim() + "|" + i+ "|" +result[8]+ "|" +result[9]+ "|" +result[10]+ "|" +result[11]+ "|" +result[12]+ "|" +result[5]);
//                System.out.println("tetest "+((String) result[2]).trim());
            }
        }
//        System.out.println("loopmenu now : " + resultsx.size());
//        System.out.println("-----------------------------------------------");
       return findprev2(al, results,appName,userid);
    }

    public String findprev(ArrayList baskom, List<Object[]> results,String appName, String userid) {
        String asal = "null";
        String prev = "";
        String temp = "";
        String url = "";
        String verson = "";
        String ipfunc = "";
        String menuku="";
        int kondisi = 0;
        int kondisi2 = 0;
        for (Object san : baskom) {
//            String listString = san + "\t";5
//            System.out.println(listString);
            for (Object dit : baskom) {
                String data_tree = dit.toString();
                String[] pepes = data_tree.split("\\|");
                
                String papah = pepes[1];
                prev = pepes[4];
                String node = pepes[0];
                String status = pepes[3];

                if (prev.equals(asal) && status.equals("1")) {
//                    System.out.println("kondisi a " + kondisi);
                    String secCode = (pepes[9]).trim();
                    String desknode = (pepes[10]).trim();
                    String levfac = (pepes[11]).trim();
                    url = pepes[6];
                    verson = pepes[7];
                    ipfunc = pepes[8];
                    if (ipfunc.trim().equals("") || ipfunc.trim() == null) {
                        ipfunc = "0";
                    }
                    if (secCode.equals("") || secCode == null) {
                        secCode = "0";
                    }
//                    String secCode = "tes";
                    menuku += "<ul class=\"nav nav-second-level collapse\">";
                    menuku += "<li >";
                    menuku += "<a href=\"#\" onclick=\"loadBody('" +url.trim()+"/"+ pepes[0] + "&app=" + appName + "&modul=" + ((String) pepes[2]).replace(" ", "%20") + "&userid=" + userid + "&seccode=" + secCode.replace(" ", "%20") + "&verson=" + verson.replace(".", "-").trim()+ "&ipfunc=" + ipfunc.replace(" ", "%20").trim()+"&desknode=" + desknode.replace(" ", "%20").trim()+"&nodetea=" + node.replace(" ", "%20").trim()+"&levfac=" + levfac.replace(" ", "%20").trim()+ "')\">";
                    menuku += pepes[2];
                    menuku += "</a>";
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    results.remove(pepes[5]);
                } else if (prev.equals(asal) && status.equals("0")) {
//                    System.out.println("kondisi b " + kondisi);
                    menuku += "<ul class=\"nav nav-second-level collapse\">";
                    menuku += "<li >";
                    menuku += "<a href=\"#\">";
                    menuku += pepes[2];
                    appName = ((String) pepes[2]).replace(" ", "%20");
                    menuku += "<span class=\"fa arrow\"></span></a>";
                    menuku += loopMenu2(results, node,appName,userid);
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    results.remove(pepes[5]);
                }
            }
//            System.out.println("tem : " + temp);
            asal = temp;
        }
        return menuku;
    }

    public String findprev2(ArrayList baskom, List<Object[]> results,String appName,String userid) {
        String asal = "null";
        String prev = "";
        String temp = "";
        String url = "";
        String verson = "";
        String ipfunc = "";
        String menuku="";
        int kondisi = 0;
        int kondisi2 = 0;
        for (Object san : baskom) {
//            String listString = san + "\t";
//            System.out.println(listString);
            for (Object dit : baskom) {
//                System.out.println("ada1");
                String data_tree = dit.toString();
                String[] pepes = data_tree.split("\\|");
//                System.out.println("ada2");;
                prev = pepes[4];
                String node = pepes[0];
                String status = pepes[3];
                String papah = pepes[1];
//                System.out.println("ada3");

                if (prev.equals(asal) && status.equals("1")) {
//                    System.out.println("ada4");
                    String secCode = (pepes[9]).trim();
                    String desknode = (pepes[10]).trim();
                    String levfac = (pepes[11]).trim();
                    url = pepes[6].toString().trim();
                    verson = pepes[7].toString().trim();
                    ipfunc = pepes[8].toString().trim();
                    if (ipfunc.trim().equals("") || ipfunc.trim() == null) {
                        ipfunc = "0";
                    }
                    if (secCode.equals("") || secCode == null) {
                        secCode = "0";
                    }
                    menuku += "<ul class=\"nav nav-third-level\">";
                    menuku += "<li class=\"\">";
                    menuku += "<a href=\"#\" onclick=\"loadBody('" +url.trim()+"/"+ pepes[0] + "&app=" + appName + "&modul=" + ((String) pepes[2]).replace(" ", "%20") + "&userid=" + userid + "&seccode=" + secCode.replace(" ", "%20")+ "&verson=" + verson.replace(".", "-").trim()+ "&ipfunc=" + ipfunc.replace(" ", "%20").trim() +"&desknode=" + desknode.replace(" ", "%20").trim() +"&nodetea=" + node.replace(" ", "%20").trim()+"&levfac=" + levfac.replace(" ", "%20").trim()+ "')\">";
//                    menuku += "<a href=\""+ result[2]+"\"> onclick=\"loadDoc('"+result[2]+"')\" ";
                    menuku += pepes[2];
                    menuku += "</a>";
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    results.remove(pepes[5]);
                } else if (prev.equals(asal) && status.equals("0")) {
//                    System.out.println("ada5");
                    menuku += "<ul class=\"nav nav-third-level\">";
                    menuku += "<li class=\"\">";
                    menuku += "<a href=\"#\">";
                    menuku += pepes[2];
                    appName = ((String) pepes[2]).replace(" ", "%20");
                    menuku += "<span class=\"fa arrow\"></span></a>";
                    menuku += loopMenu2(results, node,appName,userid);
                    menuku += "</li>";
                    menuku += "</ul>";
                    kondisi++;
                    temp = pepes[0];
                    results.remove(pepes[5]);
                }
            }
//            System.out.println("tem : " + temp);
            asal = temp;
        }
//        System.out.println("ada");
        return menuku;
    }
}
