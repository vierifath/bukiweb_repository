/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao;

import bukiweb.entity.datauser.SSFNODE;
import bukiweb.model.Ssfndsc;
import bukiweb.model.Sslnode;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.annotations.SQLInsert;
import org.springframework.web.servlet.tags.Param;

/**
 *
 * @author dpti
 */
public interface DisplayNodeDao {

    List<Object[]> getAllNode();

    public String saveNode(Sslnode sslnode);
    
    public void saveNode1(SSFNODE sslnode);

    public String deleteNode(String nodeId);

    public Sslnode getNodeById(String nodeId);

    public void saveNDSC(Ssfndsc ssfndsc);
    
    public Ssfndsc getNDSCById(String nodeId);

    List<Object[]> getParentNode();

    public String updateNode(Sslnode sslnode);
    
    public List<Object[]> getNodeId();
    
    public List getTree(String nodeid);

}
