/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao;

import bukiweb.entity.data2000.TBFILE;
import bukiweb.entity.datauser.SSFUSERPRF;
import java.util.List;

/**
 *
 * @author Gustiana
 */
public interface ProfileDao {
    
    SSFUSERPRF getProfilUser(String UIDSGN);
    
    List<Object[]> getProfilBranch(String UIDSGN);
    
    List<Object[]> getProfilLoc(String UIDSGN);
    
}
