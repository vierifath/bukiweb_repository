/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao.impl;

import bukiweb.dao.TreeDao;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gustiana
 */
@Repository
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class TreeDaoImpl implements TreeDao {

    @Autowired
    private SessionFactory sessionFactory;
    private TreeDao treeDao;

    @Override
    public List<String[]> getGrupId() {
        return sessionFactory.getCurrentSession().createQuery("select SSGRPID from Sslgroup").list();
    }

    @Override
    public List getNameGrup(String id) {
        return sessionFactory.getCurrentSession().createQuery("select SSGRPNM from Sslgroup where SSGRPID = :ssgrpid").
                setParameter("ssgrpid", id).list();
    }

    @Override
    public List<Object[]> getBeforeNode(String grupid, String nodecur) {

//            System.out.println(treeBefore);
////            ssltree.setSSMAPID(null);
//            ssltree.setSSPARENT(parent);
//            ssltree.setSSNODEPR(treeBefore);
//            ssltree.setSSGRPID(grupid);
//            
//            System.out.println(ssltree);
//            
//            Query query = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set SSNODEPR = :before where SSGRPID = :grupid and SSNODEID = :nodeid");
//            query.setParameter("before", ssltree.getSSNODEID());
//            query.setParameter("grupid", grupid);
//            query.setParameter("nodeid", nodecur);
//
//            sessionFactory.getCurrentSession().saveOrUpdate(ssltree);
//
//            System.out.println(query.executeUpdate() + " row(s) is succes insert");
        Query query = sessionFactory.getCurrentSession().createQuery("select SSPARENT,SSNODEPR from Ssltree2 where SSGRPID = :grupid and SSNODEID = :node");
        query.setParameter("grupid", grupid);
        query.setParameter("node", nodecur);
        return query.list();

    }

    @Override
    public List<String> getNodeByParent(String parent, String grupid) {
        List list = new ArrayList();
        try {
            System.out.println(parent + ":" + grupid);
            Query query = sessionFactory.getCurrentSession().createQuery("select SSNODEID from Ssltree2 where SSGRPID = :grupid and SSPARENT = :parent");
            query.setParameter("grupid", grupid);
            query.setParameter("parent", parent);
            return query.list();

        } catch (Exception e) {
            list.add(e);
            return list;
        }

    }

    @Override
    @Transactional(readOnly = false)
    public String saveTree(Ssltree2 ssltree) {
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set SSNODEPR = :nodeid where SSNODEPR = :nodepr and SSGRPID = :grup");
            query.setParameter("nodeid", ssltree.getSSNODEID());
            query.setParameter("nodepr", ssltree.getSSNODEPR());
            query.setParameter("grup", ssltree.getSSGRPID());
            System.out.println(query.executeUpdate() + " row(s) updated");
            sessionFactory.getCurrentSession().save(ssltree);
            return "sukses";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public List getDesc() {
        return sessionFactory.getCurrentSession().createQuery("select max(SSMAPID) from Ssltree2").list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String insertSubTree(Ssltree2 ssltree) {
        System.out.println("ini untuk save tree");
        String pesan;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set ssnodepr = :nodeid where ssgrpid = :grpid and ssparent = :parent and ssnodepr = :nodepr");
            query.setParameter("nodeid", ssltree.getSSNODEID());
            query.setParameter("grpid", ssltree.getSSGRPID());
            query.setParameter("parent", "null");
            query.setParameter("nodepr", "null");
            sessionFactory.getCurrentSession().save(ssltree);
            pesan = "sukses";
        } catch (Exception e) {
            pesan = e.getMessage();
        }
        return pesan;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String inserrtBefore(Ssltree2 ssltree, String nodeid) {
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set SSNODEPR = :nodeid where SSNODEID = :nodepr and SSGRPID = :grup");
            query.setParameter("nodeid", ssltree.getSSNODEID());
            query.setParameter("nodepr", nodeid);
            query.setParameter("grup", ssltree.getSSGRPID());
            System.out.println(query.executeUpdate() + " row(s) updated");
            sessionFactory.getCurrentSession().save(ssltree);
            return "sukses";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String insertFirst(Ssltree2 ssltree) {
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set SSNODEPR = :nodeid where SSPARENT = :parent and SSNODEPR = 'null' and SSGRPID = :grup");
            query.setParameter("nodeid", ssltree.getSSNODEID());
            query.setParameter("parent", ssltree.getSSPARENT());
            query.setParameter("grup", ssltree.getSSGRPID());
            System.out.println(query.executeUpdate() + " row(s) updated");
            sessionFactory.getCurrentSession().save(ssltree);
            return "sukses";
        } catch (Exception e) {
            return "gagal;" + e;
        }
    }

    @Override
    public List<Object[]> getLast(String grupid, String parent) {
        return sessionFactory.getCurrentSession().createQuery("select ").list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String deleteTree(String grup, String nodeid, String nodepr) {
        Query queryUpdate = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set SSNODEPR = :nodepr where SSGRPID = :grupid and SSNODEPR = :nodeid");
        queryUpdate.setParameter("nodepr", nodepr);
        queryUpdate.setParameter("grupid", grup);
        queryUpdate.setParameter("nodeid", nodeid);
        System.out.println(queryUpdate.executeUpdate() + " row(s) is updated");

        getChildTree(grup, nodeid);

        Query query1 = sessionFactory.getCurrentSession().createQuery("delete from Ssltree2 where SSGRPID = :grup AND SSPARENT = :idnode");
        query1.setParameter("idnode", nodeid);
        query1.setParameter("grup", grup);
        System.out.println(query1.executeUpdate() + " row(s) is deleted");

        Query query = sessionFactory.getCurrentSession().createQuery("delete from Ssltree2 where SSGRPID = :grup AND SSNODEID = :idnode");
        query.setParameter("idnode", nodeid);
        query.setParameter("grup", grup);
        System.out.println(query.executeUpdate() + " row(s) is deleted");
        return "Sukses";
    }

    List listChild1 = new ArrayList();
    List listChild2 = new ArrayList();
    List listChild3 = new ArrayList();
    List listChild4 = new ArrayList();
    int idx1 = -1;
    int idx2 = -1;
    int idx3 = -1;
    int idx4 = -1;

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String deleteChild(String grup, String nodeid) {
        System.out.println(grup);
        System.out.println(nodeid);
        String pesan;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("DELETE FROM Ssltree2 WHERE SSGRPID = :grup AND SSPARENT = :nodeid");
            query.setParameter("grup", grup);
            query.setParameter("nodeid", nodeid);
            int i = query.executeUpdate();
            pesan = "sukses";
        } catch (Exception e) {
            System.out.println(e);
            pesan = e.getMessage();
        }
        return pesan;
    }

    @Override
    public String getChildTree(String grup, String nodeid) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
        query.setParameter("grup", grup);
        query.setParameter("nodeid", nodeid.trim());
        List list = query.list();
        String lev = null;
        if (list.isEmpty()) {
            lev = "sukses";
        } else {
            for (int i = 0; i < list.size(); i++) {
                Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
                query1.setParameter("grup", grup);
                query1.setParameter("nodeid", list.get(i).toString().trim());
                List list1 = query1.list();
                listChild1.add(list1);
                System.out.println("list1 : " + listChild1);
                getChildTree1(grup, list.get(i).toString().trim());
            }
        }
        return lev;
    }

    public void getChildTree1(String grup, String nodeid) {
        System.out.println(nodeid);
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
        query.setParameter("grup", grup);
        query.setParameter("nodeid", nodeid.trim());
        List list = query.list();
        String lev = null;
        int idx = 0;
        if (list.isEmpty()) {
            Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT SSNODEID FROM Ssltree2 WHERE SSGRPID = :grup AND SSPARENT = :nodeid");
            query1.setParameter("grup", grup);
            query1.setParameter("nodeid", nodeid.trim());
            List list1 = query1.list();
            if (!list1.isEmpty()) {
                lev = deleteChild(grup, nodeid.trim());
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
                query1.setParameter("grup", grup);
                query1.setParameter("nodeid", list.get(i).toString().trim());
                List list1 = query1.list();
                if (!list1.isEmpty()) {
                    listChild2.add(list1);
                    System.out.println("list2 : " + listChild2);
                    getChildTree2(grup, list.get(i).toString().trim());
                }
                idx++;
            }
            System.out.println(idx);
            System.out.println(list.size());
            if (idx == list.size()) {
                lev = deleteChild(grup, nodeid.trim());
            }
        }

    }

    public void getChildTree2(String grup, String nodeid) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
        query.setParameter("grup", grup);
        query.setParameter("nodeid", nodeid.trim());
        List list = query.list();
        String lev = null;
        int idx = 0;
        if (list.isEmpty()) {
            Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT SSNODEID FROM Ssltree2 WHERE SSGRPID = :grup AND SSPARENT = :nodeid");
            query1.setParameter("nodeid", nodeid.trim());
            query1.setParameter("grup", grup);
            List list1 = query1.list();
            if (!list1.isEmpty()) {
                lev = deleteChild(grup, nodeid.trim());
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
                query1.setParameter("grup", grup);
                query1.setParameter("nodeid", list.get(i).toString().trim());
                List list1 = query1.list();
                if (!list1.isEmpty()) {
                    listChild3.add(list1);
                    System.out.println("list2 : " + listChild3);
                    getChildTree3(grup, list.get(i).toString().trim());
                }
                idx++;
            }
            System.out.println(idx);
            System.out.println(list.size());
            if (idx == list.size()) {
                lev = deleteChild(grup, nodeid.trim());
            }
        }

    }

    public void getChildTree3(String grup, String nodeid) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
        query.setParameter("grup", grup);
        query.setParameter("nodeid", nodeid.trim());
        List list = query.list();
        String lev = null;
        int idx = 0;
        if (list.isEmpty()) {
            Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT SSNODEID FROM Ssltree2 WHERE SSGRPID = :grup AND SSPARENT = :nodeid");
            query1.setParameter("nodeid", nodeid.trim());
            query1.setParameter("grup", grup);
            List list1 = query1.list();
            if (list1 != null) {
                lev = deleteChild(grup, nodeid.trim());
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
                query1.setParameter("grup", grup);
                query1.setParameter("nodeid", list.get(i).toString().trim());
                List list1 = query1.list();
                if (!list1.isEmpty()) {
                    listChild4.add(list1);
                    System.out.println("list2 : " + listChild4);
                    getChildTree4(grup, list.get(i).toString().trim());
                }
                idx++;
            }
            System.out.println(idx);
            System.out.println(list.size());
            if (idx == list.size()) {
                lev = deleteChild(grup, nodeid.trim());
            }
        }

    }

    public void getChildTree4(String grup, String nodeid) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT t.SSNODEID FROM Ssltree2 t, Sslnode n WHERE SSGRPID = :grup AND t.SSPARENT = :nodeid AND t.SSNODEID = n.SSNODEID AND n.SSNODEST = 0 GROUP BY t.SSNODEID");
        query.setParameter("grup", grup);
        query.setParameter("nodeid", nodeid.trim());
        List list = query.list();
        String lev = null;
        int idx = 0;
        if (list.isEmpty()) {
            Query query1 = sessionFactory.getCurrentSession().createQuery("SELECT SSNODEID FROM Ssltree2 WHERE SSGRPID = :grup AND SSPARENT = :nodeid");
            query1.setParameter("nodeid", nodeid.trim());
            query1.setParameter("grup", grup);
            List list1 = query1.list();
            if (list1 != null) {
                lev = deleteChild(grup, nodeid.trim());
            }
        }
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public String updateTree(int faclev, String nodeid, String grupid) {
        Query query = sessionFactory.getCurrentSession().createQuery("update Ssltree2 set SSLVLFAC = :faclev where SSGRPID = :grup and SSNODEID = :node");
        query.setParameter("node", nodeid);
        query.setParameter("grup", grupid);
        query.setParameter("faclev", faclev);
        int result = query.executeUpdate();
        System.out.println(result);
//        System.out.println(query.executeUpdate() + " row(s) is updated");
        String ret = " row(s) is updated";
        return result + ret;
    }

    @Override
    public List<Ssltree2> getForupdateTree(String nodeid, String grupid) {

        Query query = sessionFactory.getCurrentSession().createQuery("from Ssltree2 where SSGRPID = :grup and SSNODEID = :node");
        query.setParameter("node", nodeid);
        query.setParameter("grup", grupid);
        return query.list();
    }

    @Override
    public List<Object[]> getNode(String grupid) {
        Query query = sessionFactory.getCurrentSession().createQuery("select SSNODEID,SSNODENM from Sslnode where SSNODEID not in (select SSNODEID from Ssltree2 where SSGRPID=:grupid)");
        query.setParameter("grupid", grupid);
        List list = query.list();

        return list;
    }

    @Override
    public List getNodeName(String nodeid) {
        Query query = sessionFactory.getCurrentSession().createQuery("select SSNODENM from Sslnode where SSNODEID = :nodeid");
        query.setParameter("nodeid", nodeid);

        return query.list();
    }

    @Override
    public List getGrupName(String grupid) {
        Query query = sessionFactory.getCurrentSession().createQuery("select SSGRPNM from Sslgroup where SSGRPID = :grupid");
        query.setParameter("grupid", grupid);

        return query.list();
    }

    @Override
    public Sslnode getDetNode(String nodeid) {
        return (Sslnode) this.sessionFactory.getCurrentSession().get(Sslnode.class, nodeid);
    }

    @Override
    @Transactional(readOnly = false)
    public String CopyTree(String grupidfrom, String grupidto) {
        String pesan = "";

        try {
            Query query = sessionFactory.getCurrentSession().createQuery("SELECT SSGRPID FROM Ssltree2 WHERE SSGRPID = :grupto");
            query.setParameter("grupto", grupidto);
            List list = query.list();
            if (!list.isEmpty()) {
                Query queryDel = sessionFactory.getCurrentSession().createQuery("DELETE FROM Ssltree2 WHERE SSGRPID = :grupto");
                queryDel.setParameter("grupto", grupidto);
                queryDel.executeUpdate();
            }
            Query queryCopy = sessionFactory.getCurrentSession().createQuery("");
        } catch (Exception e) {
        }

        return pesan;
    }

    @Override
    public String deleteChildTree(String grup, String nodeid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
