/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao.impl;

import bukiweb.controller.PesanError;
import bukiweb.dao.DisplayNodeDao;
import bukiweb.entity.datauser.SSFNODE;
import bukiweb.model.Ssfndsc;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import java.sql.Connection;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.SQLInsert;
import org.hibernate.annotations.SQLUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author dpti
 */
@Repository
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class DisplayNodeImpDao implements DisplayNodeDao {

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    public void setSession(Session session) {
        session.beginTransaction();
        this.session = session;
    }

    @Override
    public List<Object[]> getAllNode() {
        return this.sessionFactory.getCurrentSession().createQuery("FROM Sslnode").list();
    }

    @Transactional(readOnly = false)
    @Override
    public String saveNode(Sslnode sslnode) {
        String pesan;
        System.out.println("test " + sslnode.getSSNODEID());
        try {
            this.sessionFactory.getCurrentSession().save(sslnode);

            pesan = "sukses";
            System.out.println(" row(s) success insert");
        } catch (HibernateException e) {
            pesan = e.getMessage();
            System.out.println("test gagal :" + e.getMessage());
        }
        return pesan;
    }

    @Override
    public void saveNDSC(Ssfndsc ssfndsc) {
        System.out.println("test ");
        try {
            this.sessionFactory.getCurrentSession().saveOrUpdate(ssfndsc);
            Query query = this.sessionFactory.getCurrentSession().createQuery("update Ssfndsc set "
                    + "SSFLD01 = ' ', SSFLD02 = ' ', SSFLD03 = ' ', SSFLD04 = '0', SSFLD05 = '0' "
                    + "where SSNDID = :nodeid");

            query.setParameter("nodeid", ssfndsc.getSSNDID());
            System.out.println(query.executeUpdate() + " row(s) success insert");
        } catch (Exception e) {
            System.out.println("test gagal :" + e.getMessage());
        }
    }

    @Override
    public String deleteNode(String nodeId) {
        String pesan = null;
        try {
            Query query = this.sessionFactory.getCurrentSession().createQuery("DELETE FROM Sslnode WHERE SSNODEID = :nodeid");
            query.setParameter("nodeid", nodeId);
            Query querydel = this.sessionFactory.getCurrentSession().createQuery("SELECT SSNODEID FROM Ssltree2 WHERE SSNODEID = :nodeid ");
            querydel.setParameter("nodeid", nodeId);
            List list = querydel.list();

            System.out.println(list);
            if (list == null || list.isEmpty() || list.equals("")) {
                int result = query.executeUpdate();
                System.out.println(result + " row(s) Success Deleted");
                pesan = "sukses";
            } else {
                System.out.println("tree menu");
                pesan = "HAPUS '" + nodeId + "' YANG ADA DI TREE MENU DULU";
            }

        } catch (Exception e) {
            System.err.println("Error Deleted row " + e.getMessage());
            pesan = "DELETE NODE FAILED";
        }
        return pesan;
    }

    @Override
    public Sslnode getNodeById(String nodeId) {
//        return (Sslnode) this.sessionFactory.getCurrentSession().get(Sslnode.class, nodeId);
        return (Sslnode) this.sessionFactory.getCurrentSession().createQuery("from Sslnode where trim(ssnodeid) = :nodeid").
                setParameter("nodeid", nodeId.trim()).uniqueResult();
    }

    @Override
    public List<Object[]> getParentNode() {
        return this.sessionFactory.getCurrentSession().createQuery("select rtrim(SSNODEID),rtrim(SSNODENM) from Sslnode where SSNODEST = '0' ORDER BY SSNODEID ASC").list();
    }

    @Override
    public String updateNode(Sslnode sslnode) {

        System.out.println("test " + sslnode.getSSNODEID());
        System.out.println("test " + sslnode.getSSNODENM());

        String pesan = null;
        try {

            this.sessionFactory.getCurrentSession().update(sslnode);
            System.out.println(" row(s) success updated");
            pesan = "sukses";
        } catch (Exception e) {
            System.out.println(" failed cause : " + e);
            pesan = e.getMessage();
        }

        return pesan;
    }

    @Transactional(readOnly = false, isolation = Isolation.READ_UNCOMMITTED)
    @Override
    public void saveNode1(SSFNODE sslnode) {
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(sslnode);
            System.out.println("row success");
        } catch (Exception e) {
            System.err.println("gagal : " + e.getMessage());
        }
    }

    @Override
    public List<Object[]> getNodeId() {
        return this.sessionFactory.getCurrentSession().createQuery("select trim(SSNODEID), SSNODENM from Sslnode").list();
    }

    @Override
    public Ssfndsc getNDSCById(String nodeId) {
        return (Ssfndsc) this.sessionFactory.getCurrentSession().get(Ssfndsc.class, nodeId);
    }

    @Override
    public List getTree(String nodeid) {
        Query querydel = this.sessionFactory.getCurrentSession().createQuery("SELECT SSNODEID FROM Ssltree2 WHERE SSNODEID = :nodeid");
        querydel.setParameter("nodeid", nodeid);
        List list = querydel.list();
        return list;
    }

}
