/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao.impl;

import bukiweb.dao.ReleaseUserDao;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author dpti
 */
@Repository
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class ReleaseDaoImp implements ReleaseUserDao {

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    public void setSession(Session session) {
        session.beginTransaction();
        this.session = session;
    }

    @Override
    public List<Object[]> getUserFacLevel(String groupid, String nodeid) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT SSLVLFAC,SSNODEID FROM Ssltree2 WHERE SSGRPID = :groupid AND SSNODEID = :nodeid");
        query.setParameter("groupid", groupid);
        query.setParameter("nodeid", nodeid);
        return query.list();
    }

    @Override
    public List<Object[]> getAllUser(String userlogin) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT p.UIDSGN, p.UNAME FROM SSFUSERPRF p, SSFUSER u WHERE u.SSUIDSGN = p.UIDSGN AND u.SSSTSLGN = '2' ORDER BY p.UIDSGN");
        return query.list();
    }

    @Override
    public List<Object[]> getAllUserCabang(String ubatid, String nextp, String userlogin) {
//        Query query = sessionFactory.getCurrentSession().createQuery("SELECT UIDSGN, UNAME FROM SSFUSERPRF WHERE SMBRCH = (SELECT SMBRCH FROM SSFUSERPRF WHERE UBATID = :ubatid)  ORDER BY UIDSGN");
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT p.UIDSGN, p.UNAME FROM SSFUSERPRF p, SSFUSER u WHERE p.SMBRCH = (SELECT SMBRCH FROM SSFUSERPRF WHERE UBATID = :ubatid) AND p.NEXTP NOT IN (:nextp) AND p.UIDSGN = u.SSUIDSGN AND u.SSSTSLGN = '2' ORDER BY UIDSGN");
        query.setParameter("ubatid", Integer.parseInt(ubatid));
        query.setParameter("nextp", nextp);
        return query.list();
    }

    @Override
    public String updateRelease(String userId) {
        String pesan;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("UPDATE SSFUSER SET SSSTSLGN = '1' WHERE SSUIDSGN = :userId");
            query.setParameter("userId", userId);
            query.executeUpdate();
            pesan = "sukses";
        } catch (Exception e) {
            pesan = e.getMessage();
        }
        return pesan;
    }

    @Override
    public String getAllUserAS400(String uidsgn) {
        String pesan;
        try {
            Query query = sessionFactory.getCurrentSession().createQuery("SELECT DPCODU, UBATID FROM SSFUSERPRF WHERE UIDSGN = :uidsgn");
            query.setParameter("uidsgn", uidsgn);
            List<Object[]> list = query.list();
            Object[] os = list.get(0);
            System.out.println(os[0]);
            System.out.println(os[1]);

            Query queryUpdate = sessionFactory.getCurrentSession().createQuery("UPDATE Trlbtot SET BTSIGN = 0 WHERE DPCOD = :dpcod AND BTCOD = :btcod");
            queryUpdate.setParameter("dpcod", os[0].toString());
            queryUpdate.setParameter("btcod", os[1].toString());
            queryUpdate.executeUpdate();
            pesan = "sukses";

        } catch (Exception e) {
            pesan = e.getMessage();
        }
//        System.out.println(ret + " row(s) updated");
        return pesan;
    }

    @Override
    public List<Object[]> getAllUserAs(String userlogin) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT p.UIDSGN, p.UNAME FROM SSFUSERPRF p, SSFUSER u, Trlbtot t WHERE u.SSUIDSGN = p.UIDSGN and t.BTSIGN = '1' and p.UBATID = t.BTCOD ORDER BY UIDSGN");
        
        return query.list();
    }
    
    @Override
    public List<Object[]> getAllUserChangePass(String userlogin) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT p.UIDSGN, p.UNAME FROM SSFUSERPRF p, SSFUSER u WHERE u.SSUIDSGN = p.UIDSGN ORDER BY UIDSGN");
        
        return query.list();
    }
    
    @Override
    public List<Object[]> getAllUserChangePassBrcnh(String userlogin, String smbrch) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT p.UIDSGN, p.UNAME FROM SSFUSERPRF p, SSFUSER u WHERE u.SSUIDSGN = p.UIDSGN and p.SMBRCH = :smbrch ORDER BY p.UIDSGN");
        query.setParameter("smbrch", Integer.parseInt(smbrch));
        return query.list();
    }

    @Override
    public List<Object[]> getAllUserEnable(String userlogin) {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT p.UIDSGN, p.UNAME FROM SSFUSERPRF p, SSFUSER u WHERE u.SSUIDSGN = p.UIDSGN AND u.SSUIDSGN <> :userlogin ORDER BY p.UIDSGN");
        query.setParameter("userlogin", userlogin);
        return query.list();
    }

}
