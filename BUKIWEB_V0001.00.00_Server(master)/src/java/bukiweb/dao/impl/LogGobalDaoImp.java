/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao.impl;

import bukiweb.dao.logGlobalInfDao;
import bukiweb.model.Llfglobal;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rustanian
 */
@Repository
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class LogGobalDaoImp implements logGlobalInfDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(readOnly = false)
    public String saveLog(Llfglobal llfglobal) {
        try {
            sessionFactory.getCurrentSession().save(llfglobal);
            return "sukses";
        } catch (Exception e) {
            return "gagal;" + e;
        }
    }

}
