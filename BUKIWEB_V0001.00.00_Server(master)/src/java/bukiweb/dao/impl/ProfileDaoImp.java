/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao.impl;

import bukiweb.dao.ProfileDao;
import bukiweb.entity.datauser.SSFUSERPRF;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gustiana
 */
@Repository
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class ProfileDaoImp implements ProfileDao{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    public SSFUSERPRF getProfilUser(String UIDSGN) {
        return (SSFUSERPRF) this.sessionFactory.getCurrentSession().get(SSFUSERPRF.class, UIDSGN);
                
    }

    public List<Object[]> getProfilBranch(String UIDSGN) {
        return this.sessionFactory.getCurrentSession().createQuery("select t.TBFCODE, t.TBFNAME from TBFILE t, SSFUSERPRF s where t.TBFRTYPE = 'BNK' and s.SMBRCH = t.TBFCODE and s.UIDSGN = :UIDSGN").
                setParameter("UIDSGN", UIDSGN).list();
    }

    public List<Object[]> getProfilLoc(String UIDSGN) {
        return this.sessionFactory.getCurrentSession().createQuery("select t.TBFCODE, t.TBFNAME from TBFILE t, SSFUSERPRF s where t.TBFRTYPE = 'LOC' and s.SMBRCH*100+s.ULOC = t.TBFCODE and s.UIDSGN = :UIDSGN").
                setParameter("UIDSGN", UIDSGN).list();
    }
    
}
