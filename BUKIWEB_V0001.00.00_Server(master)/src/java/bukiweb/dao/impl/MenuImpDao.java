/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao.impl;

import bukiweb.model.Ssfuser;
import bukiweb.model.Sslgroup;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import bukiweb.dao.MenuInfDao;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rustanian
 */
@Repository
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class MenuImpDao implements MenuInfDao {

    @Autowired
    private SessionFactory sessionFactory;
//    ,SSUIDSGN,SSSTSFNGR,SSSTSLGN,SSSTSFE, SSSTSNONFE

    @Override
    public List<Ssfuser> getGroupId(String ssuidsgn) {
        return sessionFactory.getCurrentSession().createQuery(" from Ssfuser where SSUIDSGN = '" + ssuidsgn + "'").list();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sslgroup> getGroupById(String groupid) {
        return sessionFactory.getCurrentSession().createQuery(" from Sslgroup where SSGRPID = '" + groupid + "'").list();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Ssltree2> getTreeMenu(String groupid) {
        return sessionFactory.getCurrentSession().createQuery(" select ssltree2x0_.SSGRPID as SSGRPID2_, ssltree2x0_.SSLVLFAC as SSLVLFAC2_, ssltree2x0_.SSMAPID as SSMAPID2_, ssltree2x0_.SSNODEID as SSNODEID2_, ssltree2x0_.SSNODEPR as SSNODEPR2_, ssltree2x0_.SSPARENT as SSPARENT2_ from Ssltree2 ssltree2x0_ where ssltree2x0_.SSGRPID='" + groupid + "'").list();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object[]> getTreeMenu2(String groupid) {
        return sessionFactory.getCurrentSession().createQuery("select A.SSMAPID, A.SSGRPID, A.SSNODEID, A.SSPARENT, A.SSNODEPR, A.SSLVLFAC, B.SSNODENM from Ssltree2 A, Sslnode B  where A.SSGRPID = '" + groupid + "' and  A.SSNODEID=B.SSNODEID").list();

    }

    @Override
    public List<Sslnode> getNodeByGroup(String groupid) {
        return (List<Sslnode>) (Sslnode) sessionFactory.getCurrentSession().createQuery(" from Sslnode where (SSNODEID IN (Select SSNODEID from Ssltree2 where (SSGRPID='" + groupid + "' )) or (SSNODEID in(select SSPARENT from Ssltree2 where (SSGRPID='" + groupid + "')))) order by SSNODEID").list();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Sslnode> getNodeParent(String groupid) {
        return sessionFactory.getCurrentSession().createQuery(" from Sslnode where SSNODEID IN (select SSNODEID from Sslnode where (SSNODEID LIKE 'ROOTBSYS%' and SSNODEST <> 0 ))").list();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Object[]> getTreeParent(String groupid) {
        return sessionFactory.getCurrentSession().createQuery("select A.SSMAPID, A.SSGRPID, A.SSNODEID, A.SSPARENT, A.SSNODEPR, A.SSLVLFAC, B.SSNODENM from Ssltree2 A, Sslnode B  where A.SSGRPID = '" + groupid + "' and A.SSPARENT='null' and  A.SSNODEID=B.SSNODEID order by B.SSNODENM ").list();
    }

    public List<Object[]> getTreeChild(String groupid) {
        return sessionFactory.getCurrentSession().createQuery("select A.SSMAPID, A.SSGRPID, A.SSNODEID, A.SSPARENT, A.SSNODEPR, A.SSLVLFAC, B.SSNODENM, B.SSNODEST, B.SSNODEURL,B.SSNODEVER, B.SSNDIPFUNC, B.SSSECCODE, B.SSDESCNODE from Ssltree2 A, Sslnode B  where A.SSGRPID = '" + groupid + "' and A.SSPARENT <> 'null' and  A.SSNODEID=B.SSNODEID  order by B.SSNODENM ").list();
    }

    @Override
    public List<Object[]> getUserVacility(String groupid, String nodeid) {
        return sessionFactory.getCurrentSession().createQuery("select SSLVLFAC from Ssltree2 where SSGRPID = '"+groupid+"' and SSNODEID = '"+nodeid+"'").list();
//        throw new UnsupportedOperationException("select SSLVLFAC from Ssltree2 where SSGRPID = '"+groupid+"' and SSNODEID = '"+nodeid+"'"); //To change body of generated methods, choose Tools | Templates.
    }

}
