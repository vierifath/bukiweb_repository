/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao;

import java.util.List;

/**
 *
 * @author dpti
 */
public interface ReleaseUserDao {
    List<Object[]> getUserFacLevel(String groupid, String nodeid);
    List<Object[]> getAllUser(String userlogin);
    List<Object[]> getAllUserAs(String userlogin);
    List<Object[]> getAllUserChangePassBrcnh(String userlogin, String smbrch);
    List<Object[]> getAllUserChangePass(String userlogin);
    List<Object[]> getAllUserEnable(String userlogin);
    List<Object[]> getAllUserCabang(String ubatid, String nextp, String userlogin);
    String getAllUserAS400(String uidsgn);
    String updateRelease(String userId);
}
