/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao;

import bukiweb.model.Ssfuser;
import bukiweb.model.Sslgroup;
import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import java.util.List;

/**
 *
 * @author Rustanian
 */
public interface MenuInfDao {

    public List<Ssfuser> getGroupId(String ssuidsgn);

    public List<Sslgroup> getGroupById(String groupid);

    public List<Ssltree2> getTreeMenu(String groupid);

    public List<Object[]> getTreeMenu2(String groupid);

    public List<Sslnode> getNodeByGroup(String groupid);

    public List<Sslnode> getNodeParent(String groupid);

    public List<Object[]> getTreeParent(String groupid);

    public List<Object[]> getTreeChild(String groupid);

    public List<Object[]> getUserVacility(String groupid, String nodeid);
}
