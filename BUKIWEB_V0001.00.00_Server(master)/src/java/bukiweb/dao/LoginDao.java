/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bukiweb.dao;

import bukiweb.entity.data2000.CLFPRG01;
import bukiweb.entity.datauser.SSFUSER;
import java.util.List;

/**
 *
 * @author Gustiana
 */
public interface LoginDao {

//    void saveOrUpdate(CLFPRG01 clfprg01);
//
//    void delete(CLFPRG01 clfprg01);

//    List getUser();

    List<Object[]> getAll();
    
    List<SSFUSER> getLogin(String username);
    
    public void getLogout(String username);

    public void Logout(String username);
    
    public List<Object[]> getMacAdd(String user);
    
//    List getUsername();
//    
//    java.awt.List getListUser();
}
