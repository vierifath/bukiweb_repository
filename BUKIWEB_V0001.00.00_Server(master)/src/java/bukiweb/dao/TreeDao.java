/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.dao;

import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Gustiana
 */
public interface TreeDao {
    public List<String[]> getGrupId(); //
    public List getNameGrup(String id); //
    public List<Object[]> getBeforeNode(String grupid, String nodecur); //
    public List<String> getNodeByParent(String parent, String grupid); //
    public String saveTree(Ssltree2 ssltree); //
    public List getDesc(); //
    public String insertSubTree(Ssltree2 ssltree); //
    public String inserrtBefore(Ssltree2 ssltree, String nodeid);
    public String insertFirst(Ssltree2 ssltree); 
    public List<Object[]> getLast(String grupid, String parent);
    public String deleteTree(String grup, String nodeid, String nodepr); //
    public String deleteChildTree(String grup, String nodeid); //
    public String getChildTree(String grup, String nodeid); //
    public List<Ssltree2> getForupdateTree(String nodeid, String grupid); //
    public String updateTree(int faclev, String nodeid, String grupid); //
    public List<Object[]> getNode(String grupid); //
    public List getNodeName(String nodeid); //
    public List getGrupName(String grupid); //
    public Sslnode getDetNode(String nodeid); //
    public String CopyTree(String grupidfrom, String grupidto); //
}
