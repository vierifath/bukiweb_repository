<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>

        <!--        <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../js/MaskedPassword.js"></script>
        <title>Login to BukiWEB 2015</title>
        <!--Shortcut Icon-->
        <link rel="shortcut icon" href="../img/miniLogoBukopin.png">
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <!-- Sweet Alert -->
        <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <script src="../js/plugins/sweetalert/sweetalert.min.js"></script> 

    </head>

    <script>
                window.history.forward();
                function noBacklog() {
                window.onbeforeunload = function () {
//                alert("You Can't Back Page");
                };
                }
        var message = "Sorry !!! Right Click is Disabled";
                function clickIE4() {

                if (event.button == 2) {

                alert(message);
                        return false;
                }

                }

        function clickNS4(e) {

        if (document.layers || document.getElementById && !document.all) {

        if (e.which == 2 || e.which == 3) {

        alert(message);
                return false;
        }

        }

        }

        if (document.layers) {

        document.captureEvents(Event.MOUSEDOWN);
                document.onmousedown = clickNS4;
        }

        else if (document.all && !document.getElementById) {

        document.onmousedown = clickIE4;
        }

        document.oncontextmenu = new Function("alert(message);return false");</script>

    <script>
                var ipadd = "";
                var findIP = new Promise(r => {
                var w = window,
                        a = new (w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({iceServers:[]}),
                        b = () => {};
                        a.createDataChannel("");
                        a.createOffer(c => a.setLocalDescription(c, b, b), b);
                        a.onicecandidate = c => {
                        try{
                        c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)
                        } catch (e){

                        }
                        }
                });
                /*Usage example*/
                    findIP.then(
                        function(ip) {
                            ipadd = ip;
                            document.getElementById("ipadd").value = ipadd;
//                            alert(ipadd);
                        },
                        function() {
                            alert("Something went wrong; you're probably using a sucky browser.");
                        }
                    );
   </script>

    <body onload="noBacklog();"
          onpageshow="if (event.persisted) noBack();" onunload="">

        <div 
            id="testing"
            style="text-align:center; 
            background: no-repeat center center fixed; 

            /* Set rules to fill background */
            min-height: 100%;
            min-width: 1024px;

            /* Set up proportionate scaling */
            width: 100%;
            height: auto;

            /* Set up positioning */
            position: fixed;
            top: 0;
            left: 0;

            border-width:0px;">
        </div>
        <div id="div1" class="middle-box text-center loginscreen animated bounceInUp" style="position: relative;
             top: -7%;
             transform: translateY(-50%);
             -moz-transform: translateY(-50%);
             -webkit-transform: translateY(-50%);
             -ms-transform: translateY(-50%);">
            <div style="border-radius: 3px;">
                <div>    
                    <div>

                        <img src="../img/NewLogoBUKIWEB270.png">

                    </div>
                    <h3 style="color:#ffffff;">SILAHKAN LOGIN</h3>
                    <form class="m-t" role="form" method="post" action="login">

                        <div class="form-group">
                            <div class="input-group m-b tooltip-demo">                            
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>                            
                                <input data-toggle="tooltip" data-placement="right" data-original-title="ONLY NUMBERS AND UPPERCASE" id="inputUsername" maxlength="10" type="text" class="form-control" placeholder="USERNAME" required="" name="username">
                                <input id="ipadd" type="hidden" class="form-control" name="ipadd">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group m-b">                            
                                <span class="input-group-addon"><span class="fa fa-lock"></span></span>                            
                                <input class="form-control password" type="text" id="demo-field" maxlength="10" placeholder="PASSWORD" required="" name="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group m-b col-lg-12">  
                                <span class="input-group-addon"><img src="../../kapatcha.jpg" id="kaptchaImage" onclick="document.location.reload()"/></span> 
                            </div>
                            <div class="input-group m-b col-lg-12">   
                                <input class="form-control" type="text" name="kaptcha" maxlength="5">
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="btn btn-primary block full-width m-b">LOGIN</button>
                        </div>
                        <font color="red" >${Error}</font>
                        <!--<a href="#" target="_blank"><small>Forgot password?</small></a>-->

                    </form>
                    <script type="text/javascript">

                                //apply masking to the demo-field
                                //pass the field reference, masking symbol, and character limit
                                new MaskedPassword(document.getElementById("demo-field"), '\u25CF');</script>
                    <p class="m-t"> <small><p class="text-center" style="color:#fff;">Copyright PT. Bank Bukopin,Tbk &copy; 2015</p></small> </p>
                </div>
            </div>
        </div>

    </div>

    <!--Validation Username-->
    <script type="text/javascript" src="../js/validateUsername.js"></script>

    <script type="text/javascript">
                                $("#inputUsername").on("keypress", function (event) {
                        var validationForm = /[A-Za-z0-9]/g;
                                var key = String.fromCharCode(event.which);
                                if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || validationForm.test(key)) {
                        return true;
                        }
                        return false;
                        });
                                $('#demo-field').on("paste", function (e) {
                        e.preventDefault();
                        });
                                $("#inputUsername").on('keyup', function (e) {
                        $(this).val($(this).val().toUpperCase());
                        });</script>
    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
    <!--SCRIPT BACKGROUND IMAGE-->
    <script type="text/javascript">

                                var imageArray = new Array(); // leave as is.

                                // Specify number of milliseconds between image switches.
                                var switchMilliseconds = 6000;
                                // Specify the id of the div or other HTML tag with the 
                                //   background image to switch.

                                var divID = 'testing';
                                // To add more images, continue the pattern below.

                                imageArray[0] = '../images/1.jpeg';
                                //        imageArray[1] = '../images/5.jpg';

                                        // No further customization needed in this JavaScript

                                                function publishPicture(i) {
                                                document.getElementById(divID).style.background = 'url("' + imageArray[i] + '")';
                                                        i++;
                                                        if (i > (imageArray.length - 1)) {
                                                i = 0;
                                                }
                                                setTimeout('publishPicture(' + i + ')', switchMilliseconds);
                                                }
                                        publishPicture(0);
    </script>

    <!--<script>
    
        $(document).ready(function () {
    
            $('.demo1').click(function () {
                swal({
                    title: "Welcome in Alerts",
                    text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                });
            });
    
            $('.demo2').click(function () {
                swal({
                    title: "Good job!",
                    text: "You clicked the button!",
                    type: "success"
                });
            });
    
            $('.demo3').click(function () {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                });
            });
    
            $('.demo4').click(function () {
                swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });
            });
    
    
        });
    
    </script>-->
    <!--        <div style="opacity: -0.18; display: none;" class="sweet-overlay" tabindex="-1"></div>
            <div style="display: none; margin-top: -128px; opacity: -0.19;" data-timer="null" data-animation="pop" data-has-done-function="false" data-allow-outside-click="false" data-has-confirm-button="true" data-has-cancel-button="false" data-custom-class="" class="sweet-alert hideSweetAlert">
                <div style="display: none;" class="sa-icon sa-error">
                    <span class="sa-x-mark">
                        <span class="sa-line sa-left"></span>
                        <span class="sa-line sa-right"></span>
                    </span>
                </div>
                <div style="display: none;" class="sa-icon sa-warning">
                    <span class="sa-body"></span>
                    <span class="sa-dot"></span>
                </div>
                <div style="display: none;" class="sa-icon sa-info"></div>
                <div style="display: none;" class="sa-icon sa-success">
                    <span class="sa-line sa-tip"></span>
                    <span class="sa-line sa-long"></span>
    
                    <div class="sa-placeholder"></div>
                    <div class="sa-fix"></div>
                </div>
                <div style="display: none;" class="sa-icon sa-custom"></div>
                <h2>Welcome in Alerts</h2>
                <p style="display: block;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <fieldset>
                    <input placeholder="" tabindex="3" type="text">
                    <div class="sa-input-error"></div>
                </fieldset>
                <div class="sa-error-container">
                    <div class="icon">!</div>
                    <p>Not valid!</p>
                </div>
                <div class="sa-button-container">
                    <button style="display: none; box-shadow: none;" class="cancel" tabindex="2">Cancel</button>
                    <button style="display: inline-block; background-color: rgb(174, 222, 244); box-shadow: 0px 0px 2px rgba(174, 222, 244, 0.8), 0px 0px 0px 1px rgba(0, 0, 0, 0.05) inset;" class="confirm" tabindex="1">OK</button>
                </div>
            </div>
            <div style="display: none;" class="sa-icon sa-error">
                <span class="sa-x-mark">
                    <span class="sa-line sa-left"></span>
                    <span class="sa-line sa-right"></span>
                </span>
            </div>
            <div style="display: none;" class="sa-icon sa-warning">
                <span class="sa-body"></span>
                <span class="sa-dot"></span>
            </div>
            <div style="display: none;" class="sa-icon sa-info"></div>
            <div style="display: none;" class="sa-icon sa-success">
                <span class="sa-line sa-tip"></span>
                <span class="sa-line sa-long"></span>
    
                <div class="sa-placeholder"></div>
                <div class="sa-fix"></div>
            </div>-->


    <!--    <script>
            $(document).ready(function () {
    
                var images = Array("http://placekitten.com/500/200",
                        "http://placekitten.com/499/200",
                        "http://placekitten.com/501/200",
                        "http://placekitten.com/500/199");
                var currimg = 0;
    
    
                function loadimg() {
    
                    $('#background').animate({opacity: 1}, 500, function () {
    
                        //finished animating, minifade out and fade new back in           
                        $('#background').animate({opacity: 0.7}, 100, function () {
    
                            currimg++;
    
                            if (currimg > images.length - 1) {
    
                                currimg = 0;
    
                            }
    
                            var newimage = images[currimg];
    
                            //swap out bg src                
                            $('#background').css("background-image", "url(" + newimage + ")");
    
                            //animate fully back in
                            $('#background').animate({opacity: 1}, 400, function () {
    
                                //set timer for next
                                setTimeout(loadimg, 5000);
    
                            });
    
                        });
    
                    });
    
                }
                setTimeout(loadimg, 5000);
    
            });
        </script>-->

</body>
</html>