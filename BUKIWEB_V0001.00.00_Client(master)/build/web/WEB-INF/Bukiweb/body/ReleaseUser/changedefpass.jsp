<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--<link href="../css/bootstrap.min.css" rel="stylesheet">-->
<!--<link href="../font-awesome/css/font-awesome.css" rel="stylesheet">-->
<!--<link href="../css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">

<link href="../css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">-->

<script>
    function defpass(URL) {
        $("#test1").load(URL + "?" + $("form").serialize());
    }

    function loadReleaseA(URL) {
//        alert(URL);
        URL = URL.split("/");
//        alert(URL[0]);
        document.getElementById("release").innerHTML = "<input value=\"" + URL[1] + "\" name=\"username\" type=\"text\" class=\"form-control\" id=\"username\">";
    }
</script>
<link href="../css/plugins/select2/select2.min.css" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">                        
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ${form_name} - ${verson}
                </div>
                <div class="panel-body">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>CHANGE USER PROFILE PASSWORD TO DEFAULT</h5>                                   
                        </div>
                        <div class="ibox-title">
                            ${pesan}                                   
                        </div>
                        <div class="ibox-content">
                            <form id="form" class="form-horizontal" method="post">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">USER ID</label>
                                    <div class="col-sm-10 input-group-sm m-b-n-sm">

                                        ${release}

                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label class="col-sm-2 control-label">USERNAME</label>
                                    <div class="col-sm-10 input-group-sm m-b-n-sm" id="release">
                                        <input placeholder="USERNAME" value="" name="username" type="text" class="form-control" id="username">
                                    </div>
                                </div>
                                <script>
                                    function enableBukiweb() {
//                                        alert(document.getElementById("username"));
                                    }
                                </script>
                                <div class="form-group" >
                                    <label class="col-sm-2 control-label">NEW PASSWORD</label>
                                    <div class="col-sm-10 input-group-sm m-b-n-sm" id="release">
                                        <input placeholder="NEW PASSWORD" maxlength="10" value="" name="newpassword" type="password" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button onclick="defpass('chgdefpass')" class="btn btn-w-m btn-outline btn-primary" type="button"><i class="fa fa-spinner"></i> PROSES</button>                                        
                                        <button class="btn btn-w-m btn-outline btn-primary" type="reset"><i class="fa fa-times"></i> RESET</button>
                                    </div>
                                </div>
                            </form>
                            <div class="ibox-title text-center">
                                <h4>NOTE : KOSONGKAN NEW PASSWORD = PASSWORD DEFAULT</h4>                                   
                            </div>  
                            <div id="nengkuh"></div>                        
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  

<%--<c:import url="../../footer/footer.jsp"/>--%>
<!-- Mainly scripts -->
<!--<script src="../js/jquery-2.1.1.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

 Custom and plugin javascript 
<script src="../js/inspinia.js"></script>
<script src="../js/plugins/pace/pace.min.js"></script>

<script src="../js/plugins/iCheck/icheck.min.js"></script>-->
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
<script src="../js/plugins/select2/select2.full.min.js"></script>

<script>
    $(".select2_demo_3").select2({
        allowClear: true
    });
</script>
