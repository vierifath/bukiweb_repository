<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--<link href="../css/bootstrap.min.css" rel="stylesheet">-->
<!--<link href="../font-awesome/css/font-awesome.css" rel="stylesheet">-->
<link href="../css/plugins/jsTree/style.min.css" rel="stylesheet">
<!--<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<script src="../js/jquery/jquery.min.js"></script>
<script src="../js/jquery-2.1.1.js"></script>-->
<script>
    
</script>

<div class="ibox-content">
    <div class="col-sm-12">
        <div id="jstree1">
            ${treeMenu}
        </div>
    </div>
</div>

<div class="modal inmodal" id="modalTree" tabindex="-1" role="dialog" aria-hidden="true">
    <!--    <div class="modal-dialog modal-sm">
            <div class="form-group">
                <button type="button" onclick="loadInsert('getFirst')" class="btn btn-white" data-dismiss="modal">INSERT FIRST</button>
                <button type="button" onclick="loadInsert('getBefore')" class="btn btn-white" data-dismiss="modal">INSERT BEFORE</button>
                <button type="button" onclick="loadInsert('getCurrent')" class="btn btn-white" data-dismiss="modal">INSERT AFTER</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">INSERT LAST</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">INSERT SUBTREE</button>
            </div>
        </div>-->
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--<i class="fa fa-laptop modal-icon"></i>-->
                <h4 class="modal-title">INSERT NEW TREE</h4>
                <!--<small class="font-bold ">Session telah mati, silahkan login kembali !!!</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group text-center">
                    <!--<button type="button" onclick="loadInsert('getFirst')" class="btn btn-white" data-dismiss="modal">INSERT FIRST</button>-->
                    <button type="button" onclick="loadInsert('getBefore')" class="btn btn-white" data-dismiss="modal">INSERT BEFORE</button>
                    <button type="button" onclick="loadInsert('getCurrent')" class="btn btn-white" data-dismiss="modal">INSERT AFTER</button>
                    <!--<button type="button" class="btn btn-white" data-dismiss="modal">INSERT LAST</button>-->
                    <button type="button" onclick="loadInsert('getSubtree')" class="btn btn-white" data-dismiss="modal">INSERT SUBTREE</button>
                </div>
                <!--                <div class="form-group">
                                </div>
                                <div class="form-group">
                                </div>-->
            </div>
            <!--            <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onclick="getValue()">Login</button>
                        </div>-->
        </div>
    </div>
</div>
<div class="modal inmodal" id="modalGrpTree" tabindex="-1" role="dialog" aria-hidden="true">
    <!--    <div class="modal-dialog modal-sm">
            <div class="form-group">
                <button type="button" onclick="loadInsert('getFirst')" class="btn btn-white" data-dismiss="modal">INSERT FIRST</button>
                <button type="button" onclick="loadInsert('getBefore')" class="btn btn-white" data-dismiss="modal">INSERT BEFORE</button>
                <button type="button" onclick="loadInsert('getCurrent')" class="btn btn-white" data-dismiss="modal">INSERT AFTER</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">INSERT LAST</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">INSERT SUBTREE</button>
            </div>
        </div>-->
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--<i class="fa fa-laptop modal-icon"></i>-->
                <h4 class="modal-title">INSERT NEW TREE</h4>
                <!--<small class="font-bold ">Session telah mati, silahkan login kembali !!!</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group text-center">
                    <!--<button type="button" onclick="loadInsert('getFirst')" class="btn btn-white" data-dismiss="modal">INSERT FIRST</button>-->

                    <!--<button type="button" class="btn btn-white" data-dismiss="modal">INSERT LAST</button>-->
                    <button type="button" onclick="loadInsert('getSubtree')" class="btn btn-white" data-dismiss="modal">INSERT SUBTREE</button>
                </div>
                <!--                <div class="form-group">
                                </div>
                                <div class="form-group">
                                </div>-->
            </div>
            <!--            <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" onclick="getValue()">Login</button>
                        </div>-->
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<!--<script src="../js/bootstrap.min.js"></script>
<script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>-->
<script src="../js/plugins/jsTree/jstree.min.js"></script>

<style>
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
</style>

<script>

                        $(document).ready(function () {
//                            alert('ada');
//                            $('#myModalCoba').modal({backdrop: 'static', keyboard: false, show: true});
                            $('#jstree1').jstree({
                                'core': {
                                    'check_callback': true
                                },
                                'plugins': ['types'],
                                'types': {
                                    'default': {
                                        'icon': 'fa fa-folder'
                                    },
                                    'html': {
                                        'icon': 'fa fa-file-code-o'
                                    },
                                    'svg': {
                                        'icon': 'fa fa-file-picture-o'
                                    },
                                    'css': {
                                        'icon': 'fa fa-file-code-o'
                                    },
                                    'img': {
                                        'icon': 'fa fa-file-image-o'
                                    },
                                    'js': {
                                        'icon': 'fa fa-file-text-o'
                                    }

                                }
                            });


                            $('#myModalCoba').modal('hide');


                        });
</script>