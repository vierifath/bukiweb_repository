<%-- 
    Document   : formInsert
    Created on : Dec 11, 2015, 9:29:07 AM
    Author     : Gustiana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Sweet Alert -->
<link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<div class="wrapper wrapper-content animated fadeInRight">
    <!--<div class="col-sm-12">-->

    <!--</div>-->
    <div class="panel panel-info">
        <div class="panel-heading">
            ${form_name} - ${verson}
        </div>
        <div class="panel-body">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>MAINTENANCE TREE</h5>                                   
                </div>
               ${pesan}
                <div class="ibox-content">
                    <form id="form" class="form-horizontal" method="get">
                        <div class="ibox float-e-margins">
                            <!--<div class="ibox-content">-->
                                
                                <div class="form-group">
                                <label class="col-sm-2 control-label">GROUP</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSGRPID}" name="nodename" type="text" class="form-control">
                                </div>
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${grupname}" name="nodename" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">NODE ID</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSNODEID}" name="nodeid" type="text" class="form-control">
                                </div>                                   
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${nodename}" name="nodeid" type="text" class="form-control">
                                </div>                                   
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PARENT</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSPARENT}" name="nodename" type="text" class="form-control">
                                </div>
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${parentname}" name="nodename" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PREVIOUS</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSNODEPR}" name="nodename" type="text" class="form-control">
                                </div>
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${prevname}" name="nodename" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">FACILITY LEVEL</label>
                                <div class="col-sm-10 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSLVLFAC}" name="faclev" type="text" class="form-control">
                                </div>
                            </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button  class="btn btn-w-m btn-outline btn-primary demo4" type="button"><i class="fa fa-trash-o"></i> DELETE</button>                                        
                                        <!--<button class="btn btn-w-m btn-outline btn-primary" type="reset">RESET</button>-->
                                    </div>
                                </div>
                            <!--</div>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>         

<!-- Sweet alert -->
<script src="../js/plugins/sweetalert/sweetalert.min.js"></script>

<script>

    $(document).ready(function () {

        $('.demo4').click(function () {
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    $("#test1").load('TRDEL1?' + $("form").serialize());
//                    loadBodyPost('TRDEL1');
//                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        });


    });
</script>