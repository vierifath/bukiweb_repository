<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../css/plugins/select2/select2.min.css" rel="stylesheet">


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">                        
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ${form_name} - ${verson}
                </div>
                <div class="panel-body">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>TREE MAINTENANCE</h5>                                   
                        </div>
                        ${pesan}
                        <div class="ibox-content">
                            <form class="form-horizontal" method="get">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Group ID</label>
                                    <div class="col-sm-10 input-group-sm m-b-n-sm">

                                        <select id="nodeid" onchange="loadAa('DISTREE?node=' + this.value)" class="select2_demo_3 form-control m-b" name="nodeid">
                                            <option>--PILIH GROUP ID--</option>
                                            <c:forEach var="node" items="${node}" >
                                                <option value="${node}">${node}</option>
                                            </c:forEach>
                                        </select> 
                                    </div>
                                </div>
                            </form>
                            <div id="nengkuh"></div>
                            
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       

<script src="../js/plugins/select2/select2.full.min.js"></script>

<script>
$(".select2_demo_3").select2({
                                                
                                            });
</script>
