<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>




<!--<script type="text/javascript" src="../js/validateUsername.js"></script>-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#faclevel').keyup(function () {
            var regex = new RegExp(/[^0-9]/g);
            var containsNonNumeric = this.value.match(regex);
            if (containsNonNumeric)
                this.value = this.value.replace(regex, '');
        });
    });
</script>
${pesan}
<link href="../css/plugins/select2/select2.min.css" rel="stylesheet">
<form id="form" class="form-horizontal" method="get">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <!--            <div class="form-group">
                            <label class="col-sm-2 control-label">Node Name</label>
                            <div class="col-sm-10">
                                <input readonly="" value="${allnode.SSNODEID}" name="nodeid" type="text" class="form-control">
                            </div>
                        </div>-->
            <div class="form-group">
                <label class="col-sm-2 control-label">Node Name</label>
                <div class="col-sm-10 input-group-sm m-b-n-sm">
                    <input readonly="" value="${allnode.SSNODENM}" name="nodename" type="text" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div>
                    <label class="col-sm-2 control-label">Node Status</label>
                    <script type="text/javascript">
                        function cekrdb1() {
                            document.getElementById("parentnodeid").disabled = true;
                        }

                        function cekrdb2() {
                            document.getElementById("parentnodeid").disabled = false;
                        }
                        function loadBodySani(URL) {
//                            var xhttp = new XMLHttpRequest();
//                            xhttp.onreadystatechange = function () {
//                                if (xhttp.readyState === 4 && xhttp.status === 200) {
//                                    document.getElementById("test1").innerHTML = xhttp.responseText;
//                                }
//                            };
                            $("#test1").load(URL + '?' + $("form").serialize());
//                            xhttp.open("GET", URL + '?' + $("form").serialize(), true);
//                            xhttp.send();

                        }
                    </script>
                    <div class="col-sm-10">
                        <input disabled="" readonly="" onchange="cekrdb1()" ${radio1} value="0" id="optionsRadios1" name="nodestatus" type="radio"> Parent Node
                    </div>
                    <div class="col-sm-10">
                        <input disabled="" readonly="" onchange="cekrdb2()" ${radio2} value="1" id="optionsRadios2" name="nodestatus" type="radio"> Child of Parent Node
                    </div>
                </div>                                    
            </div>
            <div class="form-group">
                <div>
                    <label class="col-sm-2 control-label">Parent Node</label>
                    <div class="col-sm-4 input-group-sm m-b-n-sm">
                        <select disabled="" id="parentnodeid" class="select2_demo_3 form-control m-b" name="parentnode" ${cbb}>
                            <option value="${allnode.SSNODEPRNT}" selected="">${allnode.SSNODEPRNT}</option>
                            <c:forEach var="parent" items="${parent}" >
                                <c:if test="${parent.get(0) == allnode.SSNODEPRNT}">
                                    <option value="${parent.get(0)}" selected="">${parent.get(0)} <label class="col-sm-2 control-label"> / </label>${parent.get(1)}</option>
                                </c:if>
                                <c:if test="${parent.get(0) != allnode.SSNODEPRNT}">
                                    <option value="${parent.get(0)}">${parent.get(0)} <label class="col-sm-2 control-label"> / </label>${parent.get(1)}</option>
                                </c:if>
                            </c:forEach>
                        </select>                                         
                    </div>
                </div>                                    
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Version</label>
                <div class="col-sm-4 input-group-sm m-b-n-sm">
                    <input readonly="" value="${allnode.SSNODEVER}" name="version" type="text" placeholder="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">IP Server Function</label>
                <div class="col-sm-4 input-group-sm m-b-n-sm">
                    <input readonly="" value="${allnode.SSNDIPFUNC}" name="ipserver" type="text" placeholder="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">URL</label>
                <div class="col-sm-6 input-group-sm m-b-n-sm">
                    <input readonly="" value="${allnode.SSNODEURL}" required="" name="nodeurl" type="text" placeholder="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Secure Node</label>
                <div class="col-sm-10 input-group-sm m-b-n-sm">
                    <input readonly="" value="${allnode.SSSECCODE}" name="securenode" required="" type="text" placeholder="" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Deskripsi Node</label>
                <div class="col-sm-10 input-group-sm m-b-n-sm">
                    <input readonly="" value="${allnode.SSDESCNODE}" required="" name="descnode" type="text" placeholder="" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Facility Level</label>
                <div class="col-sm-2 input-group-sm m-b-n-sm">
                    <input id="faclevel" required="" name="faclev" type="text" placeholder="" maxlength="1" class="form-control">
                    <!--                    <select class="form-control m-b" name="faclev" required="required">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                        </select>-->
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button ${cbb} onclick="loadBodySani('${urlforsave}')" class="btn btn-w-m btn-outline btn-primary" type="button"><i class="fa fa-save"></i> SAVE</button>                                        
                    <!--<button class="btn btn-w-m btn-outline btn-primary" type="reset">RESET</button>-->
                </div>
            </div>
        </div>
    </div>
</div>
</form>


<script src="../js/plugins/select2/select2.full.min.js"></script>

<script>
$(".select2_demo_3").select2({
                                                allowClear: true
                                            });
</script>
