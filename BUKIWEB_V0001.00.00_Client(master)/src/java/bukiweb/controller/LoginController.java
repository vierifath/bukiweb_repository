/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.entity.datauser.SSFUSER;
import bukiweb.model.LlfglobalId;
import com.google.code.kaptcha.Constants;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Gustiana
 */
@Controller
public class LoginController {

    Gson gson = new Gson();

    boolean logonvalid;
//    public String Username;
//    public List Username = new ArrayList();
//    public static String Password;
    String defUrl;
    boolean defPass = false;
    int newtab = 0;
    int newtab1 = 0;
    List macList = new ArrayList();
    String expired = "";
    String sukses = "";

    List<SSFUSER> getUsername;

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();

        defUrl = pc.prop();
    }

    protected String logout(HttpServletRequest request) {
//        if (!request.getParameter("id").isEmpty()) {
//            Username=request.getParameter("id");
//        }
        newtab = 0;
        String macaddr = (String) request.getSession().getAttribute("macaddr");
        String cek_user = "";
        String Username=request.getParameter("username");
        String Password=request.getParameter("password");
        cek_user = request.getParameter("userid");
        System.out.println("kur cek user " + cek_user);
        if (!cek_user.equals("") || cek_user != null || !cek_user.contains("null") || !cek_user.isEmpty()) {
            Username = cek_user;
        } else {
            Username = (String) request.getSession().getAttribute("logout");
        }
        System.out.println("usernami " + Username);
        String ret = "";
        getPropUrl();

        //request.
        request.getSession().removeAttribute("profilList");
        request.getSession().removeAttribute("profil");
        request.getSession().removeAttribute("menuku");
        request.getSession().removeAttribute("nodeList");
        request.getSession().removeAttribute("parentList");
        request.getSession().removeAttribute("nodeListDel");
        request.getSession().invalidate();

        System.err.println("log out");

        org.json.simple.JSONObject userpass = new org.json.simple.JSONObject();
        System.out.println(Username);

        userpass.put("username", Username);
        StringBuilder sb = new StringBuilder();

        try {
            //Log Global
            LlfglobalId id = new LlfglobalId();
            org.json.simple.JSONObject param = new org.json.simple.JSONObject();
            ValidasiNode validasiNode = new ValidasiNode();

            param.put("username", Username);
            id.setLlappfunc("logout");
            id.setLlappnm("bukiweb single logout");
            id.setLldata1(param.toString());
            id.setLlerrmsg("");
            id.setLlmode("DEBUG");
            id.setLlmodul("BUKIWEB");
            id.setLltrmid(macaddr);
            id.setLluser(Username);
            System.out.println("param di login :" + param);
            ret = validasiNode.LogGlobal(id);

            if (ret.contains("sukses")) {
                System.out.println("sukses log " + ret);
            } else {
                System.out.println("err log " + ret);
            }
            //End of Log

            URL url = new URL(defUrl + "logout");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(userpass).getBytes());
            os.flush();
            os.close();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                System.out.println(sb.toString());

                //Log Global Info
                id.setLlmode("INFO");
                id.setLlerrmsg("Sukses");
                ret = validasiNode.LogGlobal(id);

                if (ret.contains("sukses")) {
                    System.out.println("sukses log " + ret);
                } else {
                    System.out.println("err log " + ret);
                }
                //End of Log

            } else {
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            System.err.println("error");

        }

        return sb.toString();
    }
    
    
    
    protected String logout404(HttpServletRequest request, String userid) {
//        if (!request.getParameter("id").isEmpty()) {
//            Username=request.getParameter("id");
//        }

        String macaddr = (String) request.getSession().getAttribute("macaddr");
        String Username=request.getParameter("username");
        String Password=request.getParameter("password");
        String cek_user = "";
//        cek_user = request.getParameter("userid");
//        System.out.println("cek user " + cek_user);
//        if (!cek_user.equals("") || cek_user != null || !cek_user.contains("null") || !cek_user.isEmpty()) {
//            Username = cek_user;
//        }else{
        Username = userid;
//        }
        
        String ret = "";
        getPropUrl();

        //request.
        request.getSession().removeAttribute("profilList");
        request.getSession().removeAttribute("profil");
        request.getSession().removeAttribute("menuku");
        request.getSession().removeAttribute("nodeList");
        request.getSession().removeAttribute("parentList");
        request.getSession().removeAttribute("nodeListDel");

        System.err.println("log out");

        org.json.simple.JSONObject userpass = new org.json.simple.JSONObject();
        System.out.println(Username);

        userpass.put("username", Username);
        StringBuilder sb = new StringBuilder();

        try {
            //Log Global
            LlfglobalId id = new LlfglobalId();
            org.json.simple.JSONObject param = new org.json.simple.JSONObject();
            ValidasiNode validasiNode = new ValidasiNode();

            param.put("username", Username);
            id.setLlappfunc("logout");
            id.setLlappnm("bukiweb single logout");
            id.setLldata1(param.toString());
            id.setLlerrmsg("");
            id.setLlmode("DEBUG");
            id.setLlmodul("BUKIWEB");
            id.setLltrmid(macaddr);
            id.setLluser(Username);
            System.out.println("param di login :" + param);
            ret = validasiNode.LogGlobal(id);

            if (ret.contains("sukses")) {
                System.out.println("sukses log " + ret);
            } else {
                System.out.println("err log " + ret);
            }
            //End of Log

            URL url = new URL(defUrl + "logout");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(userpass).getBytes());
            os.flush();
            os.close();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                System.out.println(sb.toString());

                //Log Global Info
                id.setLlmode("INFO");
                id.setLlerrmsg("Sukses");
                ret = validasiNode.LogGlobal(id);

                if (ret.contains("sukses")) {
                    System.out.println("sukses log " + ret);
                } else {
                    System.out.println("err log " + ret);
                }
                //End of Log

            } else {
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            System.err.println("error");

        }

        return sb.toString();
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    private ModelAndView getLogout(HttpServletRequest request, HttpServletResponse response) {

        response.setHeader("Cache-Control", "private,no-store,no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
//        httpResponse.setHeader("Cache-Control", "private,no-store,no-cache");
//        System.out.println("logout " + request.getParameter("userid"));
        String Username = request.getParameter("userid");
//        System.out.println("username " + Username);
        logout(request);
        return new ModelAndView("redirect:login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    private ModelAndView getOpening(HttpServletRequest request) {

        getPropUrl();

        System.out.println(request.getLocalAddr());
        System.out.println(request.getRemoteHost());

        int clcd;
        String Username = null;
        String Password = null;
        Map map = new HashMap();

        System.out.println("lebet ka login get");
//            if (request.getSession().getAttribute("profil") == null) {
        String hasil = "";
        try {
            URL url = new URL(defUrl + "login");
//                System.out.println(url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            if (out.hasNext()) {
                hasil = out.next();
            }
            JSONObject json = new JSONObject(hasil);
            clcd = json.getInt("clcd");
            System.out.println("json : " + json);
        } catch (IOException | JSONException ex) {
            clcd = -1;
            System.err.println("error " + ex);
        }

        if (clcd != -1) {
            if (request.getSession().getAttribute("sukses") != null) {
                sukses = request.getSession().getAttribute("sukses").toString();
            }
            if (sukses.contains("Sukses")) {
                map.put("Error", "<script>"
                        + "swal({\n"
                        + "                                                    title: \"Bukiweb Warning\",\n"
                        + "                                                    type: \"success\",\n"
                        + "                                                    text: \"Change Password Success\"\n"
                        + "                                                });"
                        + "</script>");

            }
            sukses = "";
            return new ModelAndView("login", map);
        } else {
            return new ModelAndView("err500", map);
        }
//            } else {
//        return new ModelAndView("redirect:dashboard");
//            }

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    private ModelAndView getLogin(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {

        String kaptchaExpected = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        String kaptchaReceived = request.getParameter("kaptcha");
        String textResult = "";
        Map map = new HashMap();

        System.out.println("request.getRemoteAddr() : " + request.getLocalAddr());
        System.out.println("request.getRemoteHost() : " + request.getRemoteHost());
        
        if (kaptchaReceived == null || !kaptchaReceived.equalsIgnoreCase(kaptchaExpected)) {
//        if (kaptchaReceived == null ) {
            textResult = "You're entered invalid code";

        } else {
            textResult = "Congratulation you're entered correct code";
            newtab1 = 0;

            String ret = "";
            getPropUrl();

            System.out.println("lebet ka method login post");

            String expire = "";
            String password = request.getParameter("password");
            String username = request.getParameter("username");
            String ipadd = request.getParameter("ipadd");
            String macadd = "";
//            ipadd = "10.0.11.121";
            System.out.println(ipadd);
//        System.out.println("saya ganteng " + password + " Tes :" + username);
            //GET MAC ADDRESS

            macadd = getMacAdressByUseArp(ipadd);

            request.getSession().setAttribute("macadd", macadd);
            System.out.println("macadd : " + macadd);
            System.out.println("ipadd : " + ipadd);
            
            //END OF GET MAC ADDRESS
            
            request.getSession().setAttribute("username", username.toUpperCase());
            username = username.toUpperCase();
            password = password.toUpperCase();
            org.json.simple.JSONObject userpass = new org.json.simple.JSONObject();
            Enkripsi ek = new Enkripsi();
            String UsernameEnk = ek.encrypt(username);
            String PasswordEnk = ek.encrypt(password);
            request.getSession().setAttribute("password", PasswordEnk);
            userpass.put("username", UsernameEnk);
            userpass.put("password", PasswordEnk);
            userpass.put("mac", macadd);
            userpass.put("ip", ipadd);
            try {
                //Log Global
                LlfglobalId id = new LlfglobalId();
                org.json.simple.JSONObject param = new org.json.simple.JSONObject();
                ValidasiNode validasiNode = new ValidasiNode();

                param.put("username", username);
//            param.put("password", PasswordEnk);
                id.setLlappfunc("login");
                id.setLlappnm("bukiweb single logon");
                id.setLldata1(param.toString());
                id.setLlerrmsg("");
                id.setLlmode("DEBUG");
                id.setLlmodul("BUKIWEB");
                id.setLltrmid(macadd);
                id.setLluser(username);
                System.out.println("param di login :" + param);
                ret = validasiNode.LogGlobal(id);
                //End of Log
                if (ret.contains("sukses")) {
                    System.out.println("sukses log " + ret);
                } else {
                    System.out.println("err log " + ret);
                }
                URL url = new URL(defUrl + "loginPost");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                os.write(gson.toJson(userpass).getBytes());
                os.flush();
                os.close();
                StringBuilder sb = new StringBuilder();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }

                    br.close();
//                System.out.println(sb.toString());
                } else {
                    System.out.println(conn.getResponseMessage());
                }

                conn.disconnect();
                ArrayList list = new ArrayList();
                try {
                    JSONObject listDashboard = new JSONObject(sb.toString());
                    JSONArray listProfil = listDashboard.getJSONArray("profil");
                    String listMenu = listDashboard.getString("menu");
                    System.out.println(listProfil);
                    for (int i = 0; i < listProfil.length(); i++) {
                        JSONObject profil = listProfil.getJSONObject(i);
                        list.add(profil.getString("username").trim());
                        list.add(profil.getString("userlogin").trim());
                        list.add(profil.getString("groupname").trim());
                        list.add(profil.getString("cabang").trim());
                        list.add(profil.getString("lokasi").trim());
                        list.add(profil.getString("logindate").trim());
                        list.add(profil.getString("loginname").trim());
                        list.add(profil.getString("groupid").trim());
                        list.add(profil.getString("ubatid").trim());
                        list.add(PasswordEnk);
                        list.add(profil.getString("cabangCode").trim());
                        list.add(profil.getString("lokasiCode").trim());
                        list.add(profil.getString("dppcode").trim());
                        list.add(profil.getString("bu").trim());
                        list.add(profil.getString("ubatid").trim());
                        list.add(profil.getString("ulimo").trim());
                        list.add(profil.getString("ulimi").trim());
                        list.add(profil.getString("ulorak").trim());
                        list.add(profil.getString("ulirak").trim());
                        list.add(profil.getString("ukas").trim());
                        list.add(profil.getString("fe").trim());
                        list.add(profil.getString("nonfe").trim());
                        list.add(profil.getString("clcd").trim());
                        list.add(profil.getString("path").replaceAll(" ", "+"));
                        list.add(profil.getString("nik").trim());
//                        list.add(path.replaceAll(" ", "+"));
                        request.getSession().setAttribute("path", profil.getString("path").replaceAll(" ", "+"));
                    }
                    request.getSession().removeAttribute("profil");
                    request.getSession().removeAttribute("menuku");
                    request.getSession().removeAttribute("logout");
                    request.getSession().setAttribute("profil", list);
                    request.getSession().setAttribute("menuku", listMenu);
                    request.getSession().setAttribute("logout", username);
                    request.getSession().setAttribute("expired", expire);
                    //Create Token
                    Token t = new Token();
                    t.CreateToken(username,list);
                    request.getSession().setAttribute("buki_token", t.buki_token);
//                t.readNote("BK20131001");
                    //Log Global Info
                    id.setLlmode("INFO");
                    id.setLlerrmsg("Sukses");
                    ret = validasiNode.LogGlobal(id);

                    if (ret.contains("sukses")) {
                        System.out.println("sukses log " + ret);
                    } else {
                        System.out.println("err log ieu " + ret);
                    }

                    System.out.println("rengse maca na mah");
                    //End of Log
                    return new ModelAndView("redirect:dashboard");
                } catch (Exception e) {
                    id.setLltrmid(macadd);
                    if (sb.toString().contains("Password is expired.")) {
                        expired = sb.toString();
                        expire = sb.toString();
                        request.getSession().setAttribute("expired", expire);
                        id.setLlmode("ERROR");
                        id.setLlerrmsg(sb.toString() + "||");
                        ret = validasiNode.LogGlobal(id);

                        if (ret.contains("sukses")) {
                            System.out.println("sukses log " + ret + " :  e :" + e.getMessage());
                        } else {
                            System.out.println("err log " + ret + " :  e :" + e.getMessage());
                        }
                        return new ModelAndView("redirect:change");
                    } else {
                        request.getSession().setAttribute("expired", expire);
                        id.setLlmode("ERROR");
                        id.setLlerrmsg(sb.toString() + "||");
                        ret = validasiNode.LogGlobal(id);

                        if (ret.contains("sukses")) {
                            System.out.println("sukses log " + ret + " :  e :" + e.getMessage());
                        } else {
                            System.out.println("err log " + ret + " :  e :" + e.getMessage());
                        }
                        map.put("Error", sb.toString() + "<br>");
                        return new ModelAndView("login", map);
                    }
                }
            } catch (IOException e) {
                System.err.println("Error Login : " + e.getMessage());
                map.put("Error", e.getMessage() + "<br>");
                request.getSession().setAttribute("expired", expire);
//            map.put("Error", "alert(\"" + e.getMessage() + "\");");
                return new ModelAndView("login", map);

            }

        }
        map.put("Error", textResult + "<br>");
        return new ModelAndView("login", map);
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    private ModelAndView getHome(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        System.out.println("lebet ka method home");

//        String macadd = request.getSession().getAttribute("macadd").toString();
//        if (!macList.isEmpty() || macList != null) {
//            List macTemp = macList;
//            for (int i = 0; i < macTemp.size(); i++) {
//                if (macTemp.get(i) == macadd) {
//                    macList.remove(i);
//                }
//            }
//        }
        System.out.println("ieu di dashboard");
//        return "redirect:https://www.google.co.id";
        return new ModelAndView("redirect:dashboard");

    }

    @RequestMapping(value = "/404", method = RequestMethod.GET)
    private ModelAndView get404(HttpServletRequest request) throws IOException {
        return new ModelAndView("404");

    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    private ModelAndView getBeranda(HttpServletRequest request) throws IOException {

        Map map = new HashMap();

        ArrayList list = (ArrayList) request.getSession().getAttribute("profil");
        System.out.println("lebet ka Dashboard");

        if (list != null) {
            map.put("url", "home.jsp");

            return new ModelAndView("dashboard", map);
        } else {
            map.put("Error", "YOU MUST LOGIN FIRST <br>");
            return new ModelAndView("redirect:login");
        }

    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    private ModelAndView getChangePass(HttpServletRequest request) throws IOException {
        Map map = new HashMap();
        String ret = "";
        try {
            //Log Global
            LlfglobalId id = new LlfglobalId();
            org.json.simple.JSONObject param = new org.json.simple.JSONObject();
            ValidasiNode validasiNode = new ValidasiNode();
            Enkripsi ek = new Enkripsi();
//        String UsernameEnk = ek.encrypt(Username);
            String pass = request.getSession().getAttribute("password").toString();
            
//            String PasswordEnk = ek.encrypt(pass);
            String username = request.getSession().getAttribute("username").toString();
            System.out.println("change username ="+username);
            param.put("user", username);
            param.put("pass", pass);
            id.setLlappfunc("change methode GET");
            id.setLlappnm("bukiweb");
            id.setLldata1(param.toString());
            id.setLlerrmsg("");
            id.setLlmode("DEBUG");
            id.setLlmodul("bukiweb single logon");
            id.setLltrmid(" ");
            id.setLluser(username);
            ret = validasiNode.LogGlobal(id);

            if (ret.contains("sukses")) {
                System.out.println("sukses log " + ret);
            } else {
                System.out.println("err log " + ret);
            }
            //End of Log

//        logout(request);
            map.put("user", username);
//            map.put("pass", Password);
            //Log Global Info
            id.setLlmode("INFO");
            id.setLlerrmsg("Sukses");
            ret = validasiNode.LogGlobal(id);

            if (ret.contains("sukses")) {
                System.out.println("sukses log " + ret);
            } else {
                System.out.println("err log " + ret);
            }

            String expire = request.getSession().getAttribute("expired").toString();
            if (expire.contains("Password is expired.")) {
                System.out.println(expire);
                map.put("pesan", "<script>\n"
                        + "            alert(\"Password is expired. Please change Your Password\");\n"
                        + "        </script>");
            }
            //End of Log
            return new ModelAndView("changepass", map);
        } catch (Exception e) {
            System.out.println("err : " + e.getMessage());
            map.put("pesan", e.getMessage());
            return new ModelAndView("changepass", map);
        }

    }

    @RequestMapping(value = "/change", method = RequestMethod.POST)
    private ModelAndView changePass(HttpServletRequest request) throws IOException, Exception {

        Enkripsi enkripsi = new Enkripsi();

        getPropUrl();
        Map map = new HashMap();

        String kaptchaExpected = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        String kaptchaReceived = request.getParameter("kaptcha");
        String textResult = "";
        String userid = request.getParameter("username");
//        System.out.println("ipadd " + request.getSession().getAttribute("ipadd"));
        if (kaptchaReceived == null || !kaptchaReceived.contains(kaptchaExpected)) {
            textResult = "You're entered invalid code";
            map.put("pesan", textResult + "<br>");
            map.put("user", userid);

//            map.put("pesan", "alert(\""+ pesan +"\");");
            return new ModelAndView("changepass", map);
        } else {
            textResult = "Congratulation you're entered correct code";
            String ret = "";
            String Oldpass = request.getParameter("password").toUpperCase();
            String newPassword = request.getParameter("newpassword");
            String confirmPassword = request.getParameter("confirmnewpassword");
            StringBuilder sb = new StringBuilder();

            String OldpassEnk = enkripsi.encrypt(Oldpass);
            String newPasswordEnk = enkripsi.encrypt(newPassword);
            String confirmPasswordEnk = enkripsi.encrypt(confirmPassword);

            String passold = request.getSession().getAttribute("password").toString();
            String sesuname = request.getSession().getAttribute("username").toString();

            String pesan = null;
            System.out.println("ieu tah change password");
            org.json.simple.JSONObject pass = new org.json.simple.JSONObject();

            Enkripsi ek = new Enkripsi();
            pass.put("userid", userid);
            pass.put("Oldpassword", OldpassEnk);
            pass.put("password", newPasswordEnk);
            pass.put("confirmnewpassword", confirmPasswordEnk);
            map.put("user", sesuname);
            map.put("pass", passold);
            if (OldpassEnk.equals(passold)) {
                if (!newPassword.equals(Oldpass)) {

                    if (newPassword.equals(confirmPassword)) {
                        try {

                            //Log Global
                            LlfglobalId id = new LlfglobalId();
                            org.json.simple.JSONObject param = new org.json.simple.JSONObject();
                            ValidasiNode validasiNode = new ValidasiNode();
                            param.put("password", ek.encrypt(Oldpass));
                            param.put("newpassword", ek.encrypt(newPassword.toUpperCase()));
                            param.put("confirmnewpassword", ek.encrypt(confirmPassword.toUpperCase()));
                            id.setLlappfunc("change methode GET");
                            id.setLlappnm("bukiweb");
                            id.setLldata1(param.toString());
                            id.setLlerrmsg("");
                            id.setLlmode("DEBUG");
                            id.setLlmodul("bukiweb single logon");
                            id.setLltrmid(" ");
                            id.setLluser(sesuname);
                            ret = validasiNode.LogGlobal(id);

                            if (ret.contains("sukses")) {
                                System.out.println("sukses log " + ret);
                            } else {
                                System.out.println("err log " + ret);
                            }
                            //End of Log

                            URL url = new URL(defUrl + "chgdefpass");
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setDoOutput(true);
                            conn.setDoInput(true);
                            conn.setRequestProperty("Content-Type", "application/json");
                            conn.setRequestProperty("Accept", "application/json");
                            conn.setRequestMethod("POST");
                            OutputStream os = conn.getOutputStream();
                            os.write(gson.toJson(pass).getBytes());
                            os.flush();
                            os.close();
                            int HttpResult = conn.getResponseCode();
                            if (HttpResult == HttpURLConnection.HTTP_OK) {
                                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                                String line = null;
                                while ((line = br.readLine()) != null) {
                                    sb.append(line);
                                }

//                br.close();
//                    System.out.println("pesan dari web service : " + sb.toString());
                            } else {
                                System.out.println("error : " + conn.getResponseMessage());
                            }

                            if (sb.toString().contains("Sukses")) {
                                //Log Global Info
                                sukses = sb.toString();
                                request.getSession().setAttribute("sukses", sukses);
                                id.setLlmode("INFO");
                                id.setLlerrmsg("Sukses");
                                ret = validasiNode.LogGlobal(id);

                                if (ret.contains("sukses")) {
                                    System.out.println("sukses log " + ret);
                                } else {
                                    System.out.println("err log " + ret);
                                }
                                logout(request);
                                //End of Log
                                return new ModelAndView("redirect:login");
                            } else {
                                if (sb.toString().contains("too short")) {
                                    map.put("pesan", sb.toString() + ", minimal 6 digit<br>");
                                } else {
                                    map.put("pesan", sb.toString() + "<br>");
                                }
                                System.out.println("error client");
                                return new ModelAndView("changepass", map);
                            }
                        } catch (MalformedURLException e) {
                            map.put("pesan", e.getMessage() + "<br>");
                            System.out.println(e.getMessage() + ": error client");
                            return new ModelAndView("changepass", map);
                        }
                    } else {
                        pesan = "Your Password doesn't match";
                        map.put("pesan", pesan + "<br>");

//            map.put("pesan", "alert(\""+ pesan +"\");");
                        return new ModelAndView("changepass", map);
                    }

                } else {
                    pesan = "Your new Password already used";
                    map.put("pesan", pesan + "<br>");

//            map.put("pesan", "alert(\""+ pesan +"\");");
                    return new ModelAndView("changepass", map);
                }
            } else {
                pesan = "Your Current Password invalid";
                map.put("pesan", pesan + "<br>");

//            map.put("pesan", "alert(\""+ pesan +"\");");
                return new ModelAndView("changepass", map);
            }
        }
    }

    @RequestMapping(value = "/CUPBSP", method = RequestMethod.GET)
    private ModelAndView getChangeDefPass(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map map = new HashMap();
//        logout(request);

        ServletContext sc = request.getServletContext().getContext(request.getContextPath() + "/CUPBSP");
        String lvlfac = sc.getAttribute("buki_levfac").toString();
        defPass = true;
        String hasil = "";
        StringBuilder sb = new StringBuilder();
        org.json.simple.JSONObject json = new org.json.simple.JSONObject();
        json.put("userlogin", hasil);
        json.put("lvlfac", lvlfac);
        try {
            SessionController scontroller = new SessionController();
            int cekSecure = scontroller.secureURL(request, response);
            if (cekSecure != 0) {
                return new ModelAndView("500");
            }
            URL url = new URL(defUrl + "getAllUser");
            System.out.println(url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(json).getBytes());
            os.flush();
            os.close();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                hasil = sb.toString();
//                br.close();
//                    System.out.println("pesan dari web service : " + sb.toString());
            } else {
                System.out.println("error : " + conn.getResponseMessage());
            }

        } catch (IOException | JSONException ex) {

            System.err.println(ex);
        }
        JSONObject object = new JSONObject(hasil);
        String alluser = object.getString("alluser");
        map.put("release", alluser);
        request.getSession().setAttribute("alluser", alluser);
//        map.put("func", hasil);
//        map.put("combuk", "<!--");
//        map.put("comtup", "-->");
//        map.put("function", "loadbody('chgdefpass')");
//        map.put("title", "ATAU KOSONGKAN UNTUK DEFAULT PASSWORD");
//        map.put("", "");
        return new ModelAndView("body/ReleaseUser/changedefpass", map);

    }

    @RequestMapping(value = "/chgdefpass", method = RequestMethod.GET)
    private ModelAndView getChDefPass(HttpServletRequest request) throws IOException, Exception {

        getPropUrl();
        Map map = new HashMap();
        String newPassword = request.getParameter("newpassword");
        String userlogin = request.getParameter("userlogin");
        String[] userlgn = userlogin.split("/");
        StringBuilder sb = new StringBuilder();
        Enkripsi enkripsi = new Enkripsi();
        String pesan = null;
        org.json.simple.JSONObject pass = new org.json.simple.JSONObject();
        if (newPassword.isEmpty()) {
            newPassword = userlgn[0];
        }
        pass.put("userid", userlgn[0]);
        pass.put("password", enkripsi.encrypt(newPassword));
        try {
            URL url = new URL(defUrl + "chgdefpass");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(pass).getBytes());
            os.flush();
            os.close();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

//                br.close();
                System.out.println("tah ieu pesan web service : " + sb.toString());
                if (sb.toString().contains("Sukses")) {
                    pesan = "<div class=\"alert alert-success text-center\">CHANGE PASSWORD SUCCESS</div>";
                }else{
                    pesan = "<div class=\"alert alert-danger text-center\">" + sb.toString() + "</div>";
                }
            } else {
                System.out.println("error : " + conn.getResponseMessage());
            }
        } catch (MalformedURLException e) {
//                    map.put("pesan", e.getMessage() + "<br>");
            System.out.println(e.getMessage() + ": error client");
            pesan = "<div class=\"alert alert-success text-center\">CHANGE PASSWORD FAILED</div>";
        }
        map.put("pesan", pesan + "<br>");
        map.put("release", request.getSession().getAttribute("alluser"));
//        map.put("url", "changedefpass");
//        map.put("menuku", request.getSession().getAttribute("menu"));
        return new ModelAndView("body/ReleaseUser/changedefpass", map);

    }

    @RequestMapping(value = "/loginSession", method = RequestMethod.GET)
    @ResponseBody
    private String loginSession(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
        newtab1 = 0;

        Map map = new HashMap();
        String ret = "";
        getPropUrl();

        System.out.println("lebet ka method login post");

        String password = request.getParameter("passwordnew");
        String username = request.getParameter("usernamenew");
        System.out.println("username session : " + username);
        request.getSession().setAttribute("username", username.toUpperCase());
        
        String ipadd = request.getParameter("ipadd");
        System.out.println("a tendi ganteng " + password + " Tes :" + username);
        String macadd = "";
        macadd = getMacAdressByUseArp(ipadd);
        //END OF GET MAC ADDRESS

        System.out.println("request.getSession() : " + ipadd);
        System.out.println("request.getSession() : " + macadd);
        username = username.toUpperCase();
        password = password;
        org.json.simple.JSONObject userpass = new org.json.simple.JSONObject();
        Enkripsi ek = new Enkripsi();
        String UsernameEnk = ek.encrypt(username);
        String PasswordEnk = ek.encrypt(password);
        request.getSession().setAttribute("password", PasswordEnk);
        userpass.put("username", UsernameEnk);
        userpass.put("password", PasswordEnk);
        userpass.put("mac", macadd);
        userpass.put("ip", ipadd);
        try {
            //Log Global
            LlfglobalId id = new LlfglobalId();
            org.json.simple.JSONObject param = new org.json.simple.JSONObject();
            ValidasiNode validasiNode = new ValidasiNode();

            param.put("username", username);
//            param.put("password", PasswordEnk);
            id.setLlappfunc("login");
            id.setLlappnm("bukiweb single logon");
            id.setLldata1(param.toString());
            id.setLlerrmsg("");
            id.setLlmode("DEBUG");
            id.setLlmodul("BUKIWEB");
            id.setLltrmid(macadd);
            id.setLluser(username);
            System.out.println("loginSession param di login :" + param);
            ret = validasiNode.LogGlobal(id);
            //End of Log
            if (ret.contains("sukses")) {
                System.out.println("sukses log " + ret);
            } else {
                System.out.println("err log " + ret);
            }
            URL url = new URL(defUrl + "loginPost");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(userpass).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }

            conn.disconnect();
            ArrayList list = new ArrayList();
            try {
                JSONObject listDashboard = new JSONObject(sb.toString());
                JSONArray listProfil = listDashboard.getJSONArray("profil");
                String listMenu = listDashboard.getString("menu");
                System.out.println(listProfil);
                for (int i = 0; i < listProfil.length(); i++) {
                    JSONObject profil = listProfil.getJSONObject(i);
                    list.add(profil.getString("username").trim());
                    list.add(profil.getString("userlogin").trim());
                    list.add(profil.getString("groupname").trim());
                    System.out.println("profil.getString(\"cabang\").trim() ="+profil.getString("cabang").trim());
                    list.add(profil.getString("cabang").trim());
                    list.add(profil.getString("lokasi").trim());
                    list.add(profil.getString("logindate").trim());
                    list.add(profil.getString("loginname").trim());
                    list.add(profil.getString("groupid").trim());
                    list.add(profil.getString("ubatid").trim());
                    list.add(PasswordEnk);
                    list.add(profil.getString("cabangCode").trim());
                    list.add(profil.getString("lokasiCode").trim());
                    list.add(profil.getString("dppcode").trim());
                    list.add(profil.getString("bu").trim());
                    list.add(profil.getString("ubatid").trim());
                    list.add(profil.getString("ulimo").trim());
                    list.add(profil.getString("ulimi").trim());
                    list.add(profil.getString("ulorak").trim());
                    list.add(profil.getString("ulirak").trim());
                    list.add(profil.getString("ukas").trim());
                    list.add(profil.getString("fe").trim());
                    list.add(profil.getString("nonfe").trim());
                    list.add(profil.getString("clcd").trim());
                    list.add(profil.getString("path").replaceAll(" ", "+"));
                    list.add(profil.getString("nik").trim());

                }
                request.getSession().removeAttribute("profil");
                request.getSession().removeAttribute("menuku");
                request.getSession().removeAttribute("logout");
                request.getSession().setAttribute("profil", list);
                request.getSession().setAttribute("menuku", listMenu);
                request.getSession().setAttribute("logout", username);

                //Create Token
                Token t = new Token();
                t.CreateToken(username,list);
                request.getSession().setAttribute("buki_token", t.buki_token);
//                t.readNote("BK20131001");
                //Log Global Info
                id.setLlmode("INFO");
                id.setLlerrmsg("Sukses");
                ret = validasiNode.LogGlobal(id);

                if (ret.contains("sukses")) {
                    System.out.println("loginSession sukses log " + ret);
                } else {
                    System.out.println("loginSession err log ieu " + ret);
                }

                System.out.println("loginSession rengse maca");
                //End of Log
               
                return "0";
//             
            } catch (Exception e) {
                id.setLltrmid(macadd);
                if (sb.toString().contains("Password is expired.")) {
                    expired = sb.toString();
                    id.setLlmode("ERROR");
                    id.setLlerrmsg(sb.toString() + "||");
                    ret = validasiNode.LogGlobal(id);

                    if (ret.contains("sukses")) {
                        System.out.println("loginSession sukses log " + ret + " :  e :" + e.getMessage());
                    } else {
                        System.out.println("loginSession err log " + ret + " :  e :" + e.getMessage());
                    }
                    return "11||Password is expired";
//             
                } else {
                    id.setLlmode("ERROR");
                    id.setLlerrmsg(sb.toString() + "||");
                    ret = validasiNode.LogGlobal(id);

                    if (ret.contains("sukses")) {
                        System.out.println("loginSession sukses log " + ret + " :  e :" + e.getMessage());
                    } else {
                        System.out.println("loginSession err log " + ret + " :  e :" + e.getMessage());
                    }
                    map.put("Error", sb.toString() + "<br>");
                    return "22|| Message : " + sb.toString();
//              
                }
            }
        } catch (IOException e) {
            System.err.println("Error Login : " + e.getMessage());
            map.put("Error", e.getMessage() + "<br>");
//            map.put("Error", "alert(\"" + e.getMessage() + "\");");
            return "99||Error : " + e.getMessage();
//          
        }

    }
    
    @RequestMapping(value = "/loginSess", method = RequestMethod.GET)
    private ModelAndView getLoginSession(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
        System.out.println("masuk loginSess");
        return new ModelAndView("redirect:dashboard");
        
    }
    
    public String getIpLocal() {
        InetAddress ip;
        String ipadd = "";
        try {

            ip = InetAddress.getLocalHost();
            ipadd = ip.getHostAddress();
            System.out.println("loginSession Current IP address : " + ipadd);

        } catch (UnknownHostException e) {

            ipadd = e.getMessage();
        }

        return ipadd;
    }

    public String getMacLocal() {
        InetAddress ip;
        String macadd = "";
        try {

            ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);

            byte[] mac = network.getHardwareAddress();

            System.out.print("loginSession Current MAC address : ");

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            System.out.println(sb.toString());

            macadd = sb.toString();

        } catch (UnknownHostException | SocketException e) {
            macadd = e.getMessage();
        }

        return macadd;
    }

    public String getMacAdressByUseArp(String ip) throws IOException {

        String str = "";
        if (getIpLocal().equals(ip)) {
            str = getMacLocal();
        } else {
            String cmd = "arp -a " + ip;
            Scanner s = new Scanner(Runtime.getRuntime().exec(cmd).getInputStream());
            Pattern pattern = Pattern.compile("(([0-9A-Fa-f]{2}[-:]){5}[0-9A-Fa-f]{2})|(([0-9A-Fa-f]{4}\\.){2}[0-9A-Fa-f]{4})");
            try {
                while (s.hasNext()) {
                    str = s.next();
                    Matcher matcher = pattern.matcher(str);
                    if (matcher.matches()) {
                        break;
                    } else {
                        str = "";
                    }
                }
            } finally {
                s.close();
            }
        }
        System.out.println("str : " + str);
        return str;
    }

    public static void main(String[] args) throws IOException {

        LoginController lc = new LoginController();
        System.out.println(lc.getMacAdressByUseArp("10.2.62.48"));

    }

}
