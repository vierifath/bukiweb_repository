/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.model.Sslnode;
import bukiweb.model.Ssltree2;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Gustiana
 */
@Controller
public class TreeController {

    private final Gson gson = new Gson();
    String grupid = "";
    String treeBefore = "";
    String nodeparent = "";
    String nodeid = "";
    int id = 0;
    String nodepr = "";
    String url = "dashboard";
    String urlforsave = "";
    String func = "loadModal";
    String updel;
    String alamat;
    String title;

    String defUrl;

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();

        defUrl = pc.prop();

        System.out.println("URL : " + defUrl);
    }

    private ModelAndView opening(HttpServletRequest request) {
        getPropUrl();

        Map map = new HashMap();

        List listGrupId = new ArrayList<>();
        try {
            URL url = new URL(defUrl + "getGrupId");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();

            Scanner out = new Scanner(stream).useDelimiter("\\A");
            String hasil = "";
            if (out.hasNext()) {
                hasil = out.next();
            }
            JSONObject json = new JSONObject(hasil);
            JSONArray arrayGrup = json.getJSONArray("GrupId");
            for (int i = 0; i < arrayGrup.length(); i++) {
                listGrupId.add(arrayGrup.get(i));
            }
            map.put("node", listGrupId);
            request.getSession().setAttribute("grup", listGrupId);
        } catch (IOException | JSONException ex) {
            System.err.println(ex);
//            Logger.getLogger(getWebService.class.getName()).log(Level.SEVERE, null, ex);
        }

        title = request.getSession().getAttribute("title").toString();
        map.put("title", title);
        return new ModelAndView("body/Tree/displayTree", map);
    }

    @RequestMapping(value = "/TRADD", method = RequestMethod.GET)
    private ModelAndView getOpening(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
         SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        
        request.getSession().setAttribute("func", "loadModal");
        request.getSession().setAttribute("title", "INSERT NEW TREE");
        func = "loadModal";
        title = "INSERT NEW TREE";
//        getPropUrl();
//
//        Map map = new HashMap();
//
//        List listGrupId = new ArrayList<>();
//        try {
//            URL url = new URL(defUrl + "TRADD");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            InputStream stream = conn.getInputStream();
//
//            Scanner out = new Scanner(stream).useDelimiter("\\A");
//            String hasil = "";
//            if (out.hasNext()) {
//                hasil = out.next();
//            }
//            JSONObject json = new JSONObject(hasil);
//            JSONArray arrayGrup = json.getJSONArray("GrupId");
//            for (int i = 0; i < arrayGrup.length(); i++) {
//                listGrupId.add(arrayGrup.get(i));
//            }
//            map.put("node", listGrupId);
//            request.getSession().setAttribute("node", listGrupId);
//        } catch (IOException | JSONException ex) {
//            System.err.println(ex);
////            Logger.getLogger(getWebService.class.getName()).log(Level.SEVERE, null, ex);
//        }

        return opening(request);

    }

    @RequestMapping(value = "/DISTREE", method = RequestMethod.GET)
    private ModelAndView getGrupById(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        func = request.getSession().getAttribute("func").toString();
        Map map = new HashMap();
        grupid = request.getParameter("node");
        request.getSession().setAttribute("grupid", grupid);
        org.json.simple.JSONObject jsonogrupid = new org.json.simple.JSONObject();
        System.out.println("grupid : " + grupid);
        jsonogrupid.put("idgrup", grupid.trim());
        jsonogrupid.put("function", func);
        try {
            URL url = new URL(defUrl + "GetByGrup");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonogrupid).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }
            JSONObject json = new JSONObject(sb.toString());
            String treeMenu = json.getString("GrupId");

            map.put("treeMenu", treeMenu);
        } catch (IOException | JSONException ex) {
            System.err.println(ex);
//            Logger.getLogger(getWebService.class.getName()).log(Level.SEVERE, null, ex);
        }

//        func = "loadModal";
        return new ModelAndView("body/Tree/loadDisplayTree", map);

    }

    @RequestMapping(value = "/TRADD1", method = RequestMethod.GET)
    private ModelAndView getNodeId(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        grupid = request.getSession().getAttribute("grupid").toString();
        Map map = new HashMap();
        List list = new ArrayList<>();
        org.json.simple.JSONObject objectGrupid = new org.json.simple.JSONObject();
        objectGrupid.put("grupid", grupid);
        try {
            URL url = new URL(defUrl + "getNode");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(objectGrupid).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }
            JSONObject json = new JSONObject(sb.toString());
            JSONArray array = json.getJSONArray("grup");

            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
            }
//            System.out.println("hasil : " + list);
//            request.getSession().setAttribute("node", list);

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            list.add(null);
            list.add(ex.getMessage());
        }
        map.put("node", list);

        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/DetNode", method = RequestMethod.GET)
    private ModelAndView getDetailNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        id = Integer.parseInt(request.getSession().getAttribute("id").toString());
        nodeid = request.getSession().getAttribute("nodeid").toString();
        nodeparent = request.getSession().getAttribute("nodeparent").toString();
        grupid = request.getSession().getAttribute("grupid").toString();
        urlforsave = request.getSession().getAttribute("urlforsave").toString();
        String pesan = "";
        Map map = new HashMap();
        System.out.println("Detail Node");
        String node = request.getParameter("node");
        String parent = "";
        Sslnode sslnode = new Sslnode();
        List listparent = new ArrayList<>();
        try {
            org.json.simple.JSONObject jsonode = new org.json.simple.JSONObject();
            jsonode.put("node", node);
            jsonode.put("parent", nodeparent);
            jsonode.put("nodeid", nodeid);
            jsonode.put("urlforsave", urlforsave);
            URL url = new URL(defUrl + "getDetNode");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();

//                    System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }

            JSONObject listUpdate = new JSONObject(sb.toString());
            parent = listUpdate.getString("parentnode");
            JSONObject allnode = listUpdate.getJSONObject("allnode");
            JSONArray array = listUpdate.getJSONArray("listparent");
            for (int i = 0; i < array.length(); i++) {
                listparent.add(array.get(i));
            }
            request.getSession().setAttribute("parent", listparent);

            sslnode.setSSNODEID(allnode.getString("SSNODEID").trim());
            sslnode.setSSNODENM(allnode.getString("SSNODENM").trim());
            sslnode.setSSNODEST(allnode.getString("SSNODEST").trim());
            sslnode.setSSNODEPRNT(allnode.getString("SSNODEPRNT").trim());
            sslnode.setSSNDIPFUNC(allnode.getString("SSNDIPFUNC").trim());
            sslnode.setSSNODEVER(allnode.getString("SSNODEVER").trim());
            sslnode.setSSNODEURL(allnode.getString("SSNODEURL").trim());
            sslnode.setSSSECCODE(allnode.getString("SSSECCODE").trim());
            sslnode.setSSDESCNODE(allnode.getString("SSDESCNODE").trim());
            sslnode.setSSFIELD01(allnode.getString("SSFIELD01").trim());
            sslnode.setSSFIELD02(allnode.getString("SSFIELD02").trim());
            sslnode.setSSFIELD03(allnode.getString("SSFIELD03").trim());
            sslnode.setSSFIELD04(allnode.getString("SSFIELD04").trim());
            sslnode.setSSFIELD05(allnode.getString("SSFIELD05").trim());
            sslnode.setSSFIELD06(allnode.getString("SSFIELD06").trim());

            pesan = listUpdate.getString("pesan");

        } catch (IOException | JSONException e) {
            sslnode.setSSNODEST("1");
        }
        String rdb1 = "";
        String rdb2 = "";
        String cbb = "";
        map.put("nameParentNode", parent);
//        map.put("id", node);
        map.put("allnode", sslnode);
        map.put("parent", listparent);
        if (sslnode.getSSNODEST().equals("0")) {
            rdb1 = "checked=\"\"";
        } else {
            rdb2 = "checked=\"\"";
        }

        if (pesan.equals("PARENT NODE ISN'T MATCH")) {
//            map.put("btn", "");
            cbb = "disabled=\"\"";
            map.put("pesan", "<div class=\"alert alert-danger text-center\" >CAN'T INSERT NEW TREE IN THIS PARENT, CAUSE " + pesan + "</div>");
        }
        map.put("cbb", cbb);
        map.put("radio1", rdb1);
        map.put("radio2", rdb2);
        map.put("urlforsave", urlforsave);
        return new ModelAndView("body/Tree/formInsertTree", map);
    }

    @RequestMapping(value = "/getBefore", method = RequestMethod.GET)
    private ModelAndView getBefore(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        grupid = request.getSession().getAttribute("grupid").toString();
        Map map = new HashMap();
        nodeid = request.getParameter("nodeid");
        request.getSession().setAttribute("nodeid", nodeid);
//        System.out.println("Grupid : " + grupid);
//        System.out.println("Nodeid : " + nodeid);
        org.json.simple.JSONObject grupNode = new org.json.simple.JSONObject();
        grupNode.put("grupid", grupid);
        grupNode.put("nodeid", nodeid);
        List list = new ArrayList<>();
//        System.out.println("ini node id coy " + nodeid);

        try {
            URL url = new URL(defUrl + "getNode");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(grupNode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }

            JSONObject json = new JSONObject(sb.toString());
            JSONObject beforeParent = json.getJSONObject("mesaages");
            treeBefore = beforeParent.getString("before");
            nodeparent = beforeParent.getString("parent");
            id = Integer.parseInt(json.getString("id"));

            request.getSession().setAttribute("treeBefore", beforeParent.getString("before"));
            request.getSession().setAttribute("nodeparent", beforeParent.getString("parent"));
            request.getSession().setAttribute("id", json.getString("id"));
            JSONArray array = json.getJSONArray("GrupId");
//            System.out.println("after ;id terakhir : " + id);
//            list.addAll(id, list);
            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
//                System.out.println(array.get(i));
//                System.out.println(list.get(i));
            }
//            System.out.println("Before " + nodeid + " is " + sb.toString());
        } catch (IOException | JSONException e) {
        }

        request.getSession().setAttribute("node", list);
        map.put("node", list);
        urlforsave = "InsertBefore";
        request.getSession().setAttribute("urlforsave", "InsertBefore");
        map.put("urlforsave", "InsertBefore");
        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/InsertBefore", method = RequestMethod.GET)
    private ModelAndView insertBefore(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        Map map = new HashMap();
        Ssltree2 ssltree = new Ssltree2();
        String pesan;

        id = Integer.parseInt(request.getSession().getAttribute("id").toString());
        nodeid = request.getSession().getAttribute("nodeid").toString();
        nodeparent = request.getSession().getAttribute("nodeparent").toString();
        grupid = request.getSession().getAttribute("grupid").toString();
        urlforsave = request.getSession().getAttribute("urlforsave").toString();
        treeBefore = request.getSession().getAttribute("treeBefore").toString();
//        System.out.println(request.getParameter("nodeid") + ":" + nodeparent + ":" + nodeid + ":" + grupid + ":" + request.getParameter("faclev"));
        ssltree.setSSMAPID(String.valueOf(id + 1));
        ssltree.setSSNODEID(request.getParameter("nodeid").trim());
        ssltree.setSSPARENT(nodeparent.trim());
        ssltree.setSSNODEPR(treeBefore);
        ssltree.setSSGRPID(grupid);
        ssltree.setSSLVLFAC(request.getParameter("faclev"));

        try {
            URL url = new URL(defUrl + "insertBefore");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(ssltree).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                pesan = sb.toString();
//                pesan = pesan.toString();
//                    System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
                pesan = conn.getResponseMessage();
            }
            System.out.println(sb.toString());

        } catch (IOException | JSONException e) {
            pesan = e.getMessage();
        }
//        System.out.println(pesan);
        if (pesan.contains("sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\" >INSERT NEW TREE SUCCSESSED</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\" >INSERT NEW TREE FAILED, CAUSE " + pesan + "</div>");
        }
        
        map.put("node", request.getSession().getAttribute("grup"));
        return new ModelAndView("body/Tree/displayTree", map);
//        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/getCurrent", method = RequestMethod.GET)
    private ModelAndView getCurrentTree(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();
        
        grupid = request.getSession().getAttribute("grupid").toString();
        Map map = new HashMap();
        nodeid = request.getParameter("nodeid");
        request.getSession().setAttribute("nodeid", nodeid);
        System.out.println("Grupid : " + grupid);
        System.out.println("Nodeid : " + nodeid);
        org.json.simple.JSONObject grupNode = new org.json.simple.JSONObject();
        grupNode.put("grupid", grupid);
        grupNode.put("nodeid", nodeid);
        List list = new ArrayList<>();

        try {
            URL url = new URL(defUrl + "getNode");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(grupNode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }

            JSONObject json = new JSONObject(sb.toString());
            JSONObject beforeParent = json.getJSONObject("mesaages");
//            treeBefore = beforeParent.getString("before");
            request.getSession().setAttribute("nodeparent", beforeParent.getString("parent"));
            request.getSession().setAttribute("id", json.getString("id"));
            nodeparent = beforeParent.getString("parent");
            id = Integer.parseInt(json.getString("id"));

            JSONArray array = json.getJSONArray("GrupId");
            System.out.println("after ;id terakhir : " + id);
            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
            }
//            System.out.println("Before " + nodeid + " is " + sb.toString());
        } catch (IOException | JSONException e) {
        }

        request.getSession().setAttribute("node", list);

        
        map.put("node", list);
        urlforsave = "InsertAfter";
        request.getSession().setAttribute("urlforsave", urlforsave);
        map.put("urlforsave", "InsertAfter");
        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/InsertAfter", method = RequestMethod.GET)
    private ModelAndView insertAfter(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        Map map = new HashMap();
        Ssltree2 ssltree = new Ssltree2();

        id = Integer.parseInt(request.getSession().getAttribute("id").toString());
        nodeid = request.getSession().getAttribute("nodeid").toString();
        nodeparent = request.getSession().getAttribute("nodeparent").toString();
        grupid = request.getSession().getAttribute("grupid").toString();
//        System.out.println("insert after");
//        System.out.println(request.getParameter("nodeid") + ":" + nodeparent + ":" + nodeid + ":" + grupid + ":" + request.getParameter("faclev"));
        ssltree.setSSMAPID(String.valueOf(id + 1));
        ssltree.setSSNODEID(request.getParameter("nodeid").trim());
        ssltree.setSSPARENT(nodeparent.trim());
        ssltree.setSSNODEPR(nodeid);
        ssltree.setSSGRPID(grupid);
        ssltree.setSSLVLFAC(request.getParameter("faclev"));

        List list = new ArrayList<>();
        String pesan;

        try {
            URL url = new URL(defUrl + "insertAfter");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(ssltree).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                pesan = sb.toString();
//                    System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
                pesan = conn.getResponseMessage();
            }
            System.out.println(sb.toString());

        } catch (IOException | JSONException e) {
            pesan = e.getMessage();
        }
        if (pesan.contains("ukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\" >INSERT NEW TREE SUCCSESSED</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\" >INSERT NEW TREE FAILED, CAUSE " + pesan + "</div>");
        }
        
        
        map.put("node", request.getSession().getAttribute("grup"));
        return new ModelAndView("body/Tree/displayTree", map);
//        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/getSubtree", method = RequestMethod.GET)
    private ModelAndView getSubtree(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

//        id = Integer.parseInt(request.getSession().getAttribute("id").toString());
//        nodeparent = request.getSession().getAttribute("nodeparent").toString();
        grupid = request.getSession().getAttribute("grupid").toString();
        Map map = new HashMap();
        nodeid = request.getParameter("nodeid");
        System.out.println("Grupid : " + grupid);
        System.out.println("Nodeid : " + nodeid);
        if (nodeid.contains(grupid)) {
            nodeid = "null";
        }
        request.getSession().setAttribute("nodeid", nodeid);
        request.getSession().setAttribute("nodeparent", nodeid);
        org.json.simple.JSONObject grupNode = new org.json.simple.JSONObject();
        grupNode.put("grupid", grupid);
        grupNode.put("nodeid", nodeid);
        List list = new ArrayList<>();

        try {
            URL url = new URL(defUrl + "GetSubTree");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(grupNode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }

            JSONObject json = new JSONObject(sb.toString());
            JSONArray array = json.getJSONArray("GrupId");
            JSONObject idnodepr = json.getJSONObject("idnodepr");

            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
            }
            nodepr = idnodepr.getString("nodepr");
            
            request.getSession().setAttribute("nodepr", idnodepr.getString("nodepr"));
            request.getSession().setAttribute("id", idnodepr.getInt("id"));
            id = idnodepr.getInt("id");

            System.out.println("id terakhir : " + id + " dan node previous : " + nodepr);

//            System.out.println("Before " + nodeid + " is " + sb.toString());
        } catch (IOException | JSONException e) {
            System.err.println("error : " + e);
        }
        request.getSession().setAttribute("node", list);
        map.put("node", list);
        urlforsave = "insertSubtree";
        request.getSession().setAttribute("urlforsave", urlforsave);
        map.put("urlforsave", "insertSubtree");
        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/insertSubtree", method = RequestMethod.GET)
    private ModelAndView InsertSubtree(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        id = Integer.parseInt(request.getSession().getAttribute("id").toString());
        nodeid = request.getSession().getAttribute("nodeid").toString();
        nodeparent = request.getSession().getAttribute("nodeparent").toString();
        grupid = request.getSession().getAttribute("grupid").toString();
        urlforsave = request.getSession().getAttribute("urlforsave").toString();
        nodepr = request.getSession().getAttribute("nodepr").toString();
        
        String pesan;
        Map map = new HashMap();
        Ssltree2 ssltree = new Ssltree2();
        ssltree.setSSMAPID(String.valueOf(id + 1));
        ssltree.setSSNODEID(request.getParameter("nodeid").trim());
        ssltree.setSSGRPID(grupid);
        ssltree.setSSPARENT(nodeid);
        ssltree.setSSNODEPR(nodepr);
        System.out.println(nodepr + ":" + nodeid);
        ssltree.setSSLVLFAC(request.getParameter("faclev"));

        try {
            URL url = new URL(defUrl + "InsertSubTree");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(ssltree).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                pesan = sb.toString();
//                System.out.println(sb.toString());
            } else {
                pesan = conn.getResponseMessage();
                System.out.println(conn.getResponseMessage());
            }

            System.out.println(sb.toString());
        } catch (IOException | JSONException e) {
            System.err.println("error  : " + e);
            pesan = e.getMessage();
        }

        System.out.println("pesan : " + pesan);
        if (pesan.contains("sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\" >INSERT NEW TREE SUCCSESSED</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\" >INSERT NEW TREE FAILED, CAUSE " + pesan + "</div>");
        }
        
        map.put("node", request.getSession().getAttribute("grup"));
        return new ModelAndView("body/Tree/displayTree", map);
        
//        return new ModelAndView("body/Tree/formInsert", map);

    }

    @RequestMapping(value = "/TRDEL", method = RequestMethod.GET)
    private ModelAndView delTree(HttpServletRequest request, HttpServletResponse response) throws IOException {

        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        request.getSession().setAttribute("title", "DELETE TREE");
        request.getSession().setAttribute("func", "loadUp");
        request.getSession().setAttribute("updel", "formDelete");
        request.getSession().setAttribute("alamat", "GetCurNode");
        title = "DELETE TREE";
        func = "loadUp";
        updel = "formDelete";
        alamat = "GetCurNode";
        return opening(request);

    }

    @RequestMapping(value = "/TRDEL1", method = RequestMethod.GET)
    private ModelAndView delTree1(HttpServletRequest request, HttpServletResponse response) {

        grupid = request.getSession().getAttribute("grupid").toString();
        getPropUrl();
        func = "loadUp";
        request.getSession().setAttribute("func", "loadUp");
        System.out.println("Method kanggo ngahapus tree");
        Map map = new HashMap();
        nodeid = request.getParameter("nodeid");
        System.out.println("Grupid : " + grupid);
        System.out.println("Nodeid : " + nodeid);
        org.json.simple.JSONObject grupNode = new org.json.simple.JSONObject();
        grupNode.put("grupid", grupid);
        grupNode.put("nodeid", nodeid.trim());
        List list = new ArrayList<>();
        String pesan;

        try {
            URL url = new URL(defUrl + "deleteNode");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(grupNode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
//                System.out.println(sb.toString());
                pesan = "Sukses";
            } else {
                System.out.println(conn.getResponseMessage());
                pesan = conn.getResponseMessage();
            }

            System.out.println(sb.toString());
//            System.out.println("Before " + nodeid + " is " + sb.toString());
        } catch (IOException | JSONException e) {
            pesan = e.getMessage();
        }
        System.out.println(pesan);
        if (pesan.equals("Sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\" >DELETE TREE SUCCSESSED</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\" >DELETE TREE FAILED, CAUSE " + pesan + "</div>");
        }
        request.getSession().setAttribute("updel", "formDelete");
        updel = "formDelete";
        map.put("node", request.getSession().getAttribute("grup"));
        return new ModelAndView("body/Tree/displayTree", map);

    }

    @RequestMapping(value = "/TRCHG", method = RequestMethod.GET)
    private ModelAndView upTree(HttpServletRequest request, HttpServletResponse response) throws IOException {

        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        request.getSession().setAttribute("title", "UPDATE TREE");
        request.getSession().setAttribute("func", "loadUp");
        request.getSession().setAttribute("updel", "formUpdate");
        request.getSession().setAttribute("alamat", "updateTree");
        func = "loadUp";
        updel = "formUpdate";
        alamat = "updateTree";
        title = "UPDATE TREE";
        return opening(request);

    }

    @RequestMapping(value = "/getforUpdate", method = RequestMethod.GET)
    private ModelAndView getforUpdate(HttpServletRequest request) {

        getPropUrl();

        grupid = request.getSession().getAttribute("grupid").toString();
        Map map = new HashMap();
        nodeid = request.getParameter("nodeid");
        System.out.println("Grupid : " + grupid);
        System.out.println("Nodeid : " + nodeid);
        org.json.simple.JSONObject grupNode = new org.json.simple.JSONObject();
        grupNode.put("grupid", grupid);
        grupNode.put("nodeid", nodeid);
        Ssltree2 ssltree = new Ssltree2();
        Sslnode sslnode = new Sslnode();
        List listparent = new ArrayList<>();
        String parent = "";
        String nodename = "";
        String nodeprev = "";
        String grupname = "";
        try {
            URL url = new URL(defUrl + "TRCHG");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(grupNode).getBytes());
            os.flush();
            os.close(); 
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();

//                    System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }
            JSONObject listUpdate = new JSONObject(sb.toString());

            parent = listUpdate.getString("parentname");
            nodename = listUpdate.getString("nodename");
            nodeprev = listUpdate.getString("prevname");
            grupname = listUpdate.getString("grupname");

            JSONObject allnode = listUpdate.getJSONObject("TreeNode");
            System.out.println(allnode);
            ssltree.setSSMAPID(allnode.getString("SSMAPID"));
            ssltree.setSSGRPID(allnode.getString("SSGRPID"));
            ssltree.setSSNODEID(allnode.getString("SSNODEID"));
            ssltree.setSSPARENT(allnode.getString("SSPARENT"));
            ssltree.setSSNODEPR(allnode.getString("SSNODEPR"));
            ssltree.setSSLVLFAC(String.valueOf(allnode.getInt("SSLVLFAC")));

        } catch (IOException | JSONException e) {

        }

        map.put("parentname", parent.trim());
        map.put("nodename", nodename.trim());
        map.put("prevname", nodeprev.trim());
        map.put("grupname", grupname.trim());
        map.put("allnode", ssltree);
        updel = request.getSession().getAttribute("updel").toString();
        return new ModelAndView("body/Tree/" + updel, map);
    }

    @RequestMapping(value = "/UpdateTree", method = RequestMethod.GET)
    private ModelAndView UpdateTree(HttpServletRequest request) {

        grupid = request.getSession().getAttribute("grupid").toString();
        getPropUrl();
        Map map = new HashMap();
        nodeid = request.getParameter("nodeid");
        int faclev = Integer.parseInt(request.getParameter("faclev"));
        System.out.println("Grupid : " + grupid);
        System.out.println("Nodeid : " + nodeid);
        org.json.simple.JSONObject grupNode = new org.json.simple.JSONObject();
        grupNode.put("grupid", grupid);
        grupNode.put("nodeid", nodeid);
        grupNode.put("faclev", faclev);
        String pesan;
        try {
            URL url = new URL(defUrl + "updateTree");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(grupNode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                pesan = "Sukses";
//                    System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
                pesan = conn.getResponseMessage();
            }
            System.out.println(sb.toString());

        } catch (IOException | JSONException e) {
            pesan = e.getMessage();
        }
        if (pesan.equals("Sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\" >UPDATE TREE SUCCSESSED</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\" >UPDATE TREE FAILED, CAUSE " + pesan + "</div>");
        }
        updel = "formUpdate";
        request.getSession().setAttribute("updel", "formUpdate");
        map.put("node", request.getSession().getAttribute("grup"));
        return new ModelAndView("body/Tree/displayTree", map);
    }

    @RequestMapping(value = "/TRDIS", method = RequestMethod.GET)
    private ModelAndView displayTree(HttpServletRequest request, HttpServletResponse response) throws IOException {

        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        request.getSession().setAttribute("title", "DISPLAY TREE");
        request.getSession().setAttribute("func", "loadUp");
        request.getSession().setAttribute("updel", "detTree");
        func = "loadUp";
        updel = "detTree";
        title = "DISPLAY TREE";
        return opening(request);
    }


}
