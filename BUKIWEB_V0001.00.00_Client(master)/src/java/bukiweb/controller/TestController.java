/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rustanian
 */
@Controller
public class TestController {
    @RequestMapping(value = "/getTest", method = RequestMethod.GET)
    private ModelAndView getTest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("asup test");
        return new ModelAndView("redirect:http://10.2.62.229:8080");
    }
}
