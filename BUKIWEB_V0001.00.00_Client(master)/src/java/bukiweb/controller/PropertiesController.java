/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Gustiana
 */
public class PropertiesController {

    public String prop() {
        String defUrl;
        Properties prop = new Properties();

        String propFileName = "/bukiweb/conf/url.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }

        // get the property value and print it out
        defUrl = prop.getProperty("url");

        System.out.println("URL : " + defUrl);
        return defUrl;
    }
    public String appURL() {
        Properties prop = new Properties();

        String propFileName = "/bukiweb/conf/url.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            System.err.println(" Error Prop : " + ex.getMessage());
        }
        return prop.getProperty("appURL");
    }

}
