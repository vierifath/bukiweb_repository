/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.model.Ssfndsc;
import bukiweb.model.Sslnode;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dpti
 */
@Controller
public class DisplayNodeController {

    Gson gson = new Gson();

    //SSFNODE//
    //test
    String SSNODEID = "";
    String SSNODENM = "";
    String SSNODEST = "";
    String SSNODEURL = "";
    String SSDESCNODE = "";
    String SSSECCOD = "";
    String SSNODEPRNT = "";
    String SSNODEVER = "";
    String SSNDIPFUNC = "";
    String SSFIELD1 = "";
    String SSFIELD2 = "";
    String SSFIELD3 = "";
    String SSFIELD4 = "";
    String SSFIELD5 = "0";
    String SSFIELD6 = "0";
//coba
    //SSFNDSC//
    String SSFLD01 = "null";
    String SSFLD02 = "null";
    String SSFLD03 = "null";
    String SSFLD04 = "0";
    String SSFLD05 = "0";

    String defUrl;

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();

        defUrl = pc.prop();

        System.out.println("ieu teh URL : " + defUrl);
    }

    @RequestMapping(value = "/NDDIS", method = RequestMethod.GET)
    private ModelAndView getNode(HttpServletRequest request, HttpServletResponse response) throws IOException {

        //cek URL KETIK
        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        //end
        return new ModelAndView("body/Node/viewNode");
//        return new ModelAndView("dashboard", map);
    }

    @RequestMapping(value = "/NDDIS2", method = RequestMethod.GET)
    private ModelAndView getNode2(HttpServletRequest request, HttpServletResponse response) {
        Map map = new HashMap();
        List list = new ArrayList<>();

        getPropUrl();

        try {
            URL url = new URL(defUrl + "displayNode");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            String hasil = "";
            if (out.hasNext()) {
                hasil = out.next();
            }
            JSONObject listUpdate = new JSONObject(hasil);
            JSONArray allnode = listUpdate.getJSONArray("Node");
            for (int i = 0; i < allnode.length(); i++) {
                JSONObject objectNode = allnode.getJSONObject(i);
                Sslnode sslnode = new Sslnode();
                sslnode.setSSNODEID(objectNode.getString("SSNODEID").trim());
                sslnode.setSSNODENM(objectNode.getString("SSNODENM").trim());
                sslnode.setSSNODEST(objectNode.getString("SSNODEST").trim());
                sslnode.setSSNODEPRNT(objectNode.getString("SSNODEPRNT").trim());
                sslnode.setSSNDIPFUNC(objectNode.getString("SSNDIPFUNC").trim());
                sslnode.setSSNODEVER(objectNode.getString("SSNODEVER").trim());
                sslnode.setSSNODEURL(objectNode.getString("SSNODEURL").trim());
                sslnode.setSSSECCODE(objectNode.getString("SSSECCODE").trim());
                sslnode.setSSDESCNODE(objectNode.getString("SSDESCNODE").trim());
                sslnode.setSSFIELD01(objectNode.getString("SSFIELD01").trim());
                sslnode.setSSFIELD02(objectNode.getString("SSFIELD02").trim());
                sslnode.setSSFIELD03(objectNode.getString("SSFIELD03").trim());
                sslnode.setSSFIELD04(objectNode.getString("SSFIELD04").trim());
                sslnode.setSSFIELD05(objectNode.getString("SSFIELD05").trim());
                sslnode.setSSFIELD06(objectNode.getString("SSFIELD06").trim());
                list.add(sslnode);
            }
            request.getSession().setAttribute("node", list);
            map.put("node", list);
        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            list.add(null);
            list.add(ex.getMessage());
        }

        return new ModelAndView("body/Node/getAllnode", map);
    }

    @RequestMapping(value = "/NDCHG", method = RequestMethod.GET)
    private ModelAndView getupdateNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        Map map = new HashMap();
        Sslnode sslnode = new Sslnode();
        String node = request.getParameter("node");
        System.out.println("tah ieu node na kasep : " + node);
        String parent = "";
        List listparent = new ArrayList<>();

        if (node == null) {
            List list = new ArrayList<>();
            try {
                //cek URL KETIK
                SessionController scontroller = new SessionController();
                int cekSecure = scontroller.secureURL(request, response);
                if (cekSecure != 0) {
                    return new ModelAndView("500");
                }
                //end
                URL url = new URL(defUrl + "nodeid");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                InputStream stream = conn.getInputStream();
                Scanner out = new Scanner(stream).useDelimiter("\\A");
                String hasil = "";
                if (out.hasNext()) {
                    hasil = out.next();
                }
                JSONObject json = new JSONObject(hasil);
                JSONArray array = json.getJSONArray("nodeid");

                for (int i = 0; i < array.length(); i++) {
                    list.add(array.get(i));
                }
                System.out.println("ieu hasil ti web service panginten : " + list);
                request.getSession().setAttribute("node", list);
                map.put("node", list);

            } catch (IOException | JSONException ex) {
                System.err.println(ex);
                list.add(null);
                list.add(ex.getMessage());
            }

            return new ModelAndView("body/Node/formUpdateNode", map);
        } else {
            String pesan = "";
            try {
                org.json.simple.JSONObject jsonode = new org.json.simple.JSONObject();
                jsonode.put("node", node);
                URL url = new URL(defUrl + "NDCHG");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                os.write(gson.toJson(jsonode).getBytes());
                os.flush();
                os.close();
                StringBuilder sb = new StringBuilder();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }

                    br.close();

//                    System.out.println(sb.toString());
                } else {
                    System.out.println(conn.getResponseMessage());
                }
                JSONObject listUpdate = new JSONObject(sb.toString());
                pesan = listUpdate.getString("pesan");
                parent = listUpdate.getString("parentnode");
                JSONObject allnode = listUpdate.getJSONObject("allnode");
                JSONArray array = listUpdate.getJSONArray("listparent");
                for (int i = 0; i < array.length(); i++) {
                    listparent.add(array.get(i));
                }
//                request.getSession().setAttribute("parent", listparent);

                SSNODEST = allnode.getString("SSNODEST");

                System.out.println("ieu status : " + SSNODEST);

                sslnode.setSSNODEID(allnode.getString("SSNODEID").trim());
                sslnode.setSSNODENM(allnode.getString("SSNODENM").trim());
                sslnode.setSSNODEST(allnode.getString("SSNODEST").trim());
                sslnode.setSSNODEPRNT(allnode.getString("SSNODEPRNT").trim());
                sslnode.setSSNDIPFUNC(allnode.getString("SSNDIPFUNC").trim());
                sslnode.setSSNODEVER(allnode.getString("SSNODEVER").trim());
                sslnode.setSSNODEURL(allnode.getString("SSNODEURL").trim());
                sslnode.setSSSECCODE(allnode.getString("SSSECCODE").trim());
                sslnode.setSSDESCNODE(allnode.getString("SSDESCNODE").trim());
                sslnode.setSSFIELD01(allnode.getString("SSFIELD01").trim());
                sslnode.setSSFIELD02(allnode.getString("SSFIELD02").trim());
                sslnode.setSSFIELD03(allnode.getString("SSFIELD03").trim());
                sslnode.setSSFIELD04(allnode.getString("SSFIELD04").trim());
                sslnode.setSSFIELD05(allnode.getString("SSFIELD05").trim());
                sslnode.setSSFIELD06(allnode.getString("SSFIELD06").trim());

            } catch (IOException | JSONException e) {
                sslnode.setSSNODEST("1");
            }
            String rdb1 = "";
            String rdb2 = "";
            String cbb = "";
            map.put("nameParentNode", parent);
            System.out.println("ieu nodeid : " + node);
            map.put("nodeid", node);
            map.put("allnode", sslnode);
            map.put("parent", listparent);
            if (sslnode.getSSNODEST().equals("0")) {
                rdb1 = "checked=\"\"";
            } else {
                rdb2 = "checked=\"\"";
            }

            if (pesan.equals("gagal")) {
                cbb = "disabled=\"\"";
                pesan = "onchange=\"pilihParent()\"";
//                map.put("pesan", "<div class=\"alert alert-error text-center\">YOU MUST DELETE " + sslnode.getSSNODEID() + " / " + sslnode.getSSNODENM() + " FROM TREE</div>");
            }

            map.put("btn", pesan);
            map.put("cbb", cbb);
            map.put("radio1", rdb1);
            map.put("radio2", rdb2);
            return new ModelAndView("body/Node/formGetNode", map);
        }
//        return new ModelAndView("dashboard", map);
    }

    @RequestMapping(value = "/NDCHG2", method = RequestMethod.GET)
    private ModelAndView updateNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        String pesan;

        System.out.println("ieu method update post panginten");
        Map map = new HashMap();
        Sslnode sslnode = new Sslnode();
        SSNODEID = request.getParameter("nodeid");
        SSNODENM = request.getParameter("nodename");
        try {
            String SSNODESTtemp = request.getParameter("nodestatus");
            System.out.println("ieu status anyar : " + SSNODESTtemp);
            if (!SSNODESTtemp.contains("null") || SSNODESTtemp != null) {
                SSNODEST = SSNODESTtemp;
            }
        } catch (Exception e) {

            System.out.println(" aya error : " + e.getMessage());
        }

        System.out.println("iseu status : " + SSNODEST);
        SSSECCOD = request.getParameter("securenode");
        SSNODEPRNT = request.getParameter("parentnode");
        if (SSNODEPRNT.equals("null")) {
            SSNODEPRNT = "";
        }
        SSNDIPFUNC = request.getParameter("ipserver");
        SSNODEVER = request.getParameter("version");
        SSNODEURL = request.getParameter("nodeurl");
        SSDESCNODE = request.getParameter("descnode");

        sslnode.setSSNODEID(SSNODEID);
        sslnode.setSSNODENM(SSNODENM);
        sslnode.setSSNODEPRNT(SSNODEPRNT);
        sslnode.setSSNODEST(SSNODEST);
        sslnode.setSSNDIPFUNC(SSNDIPFUNC);
        sslnode.setSSNODEURL(SSNODEURL);
        sslnode.setSSNODEVER(SSNODEVER);
        sslnode.setSSSECCODE(SSSECCOD);
        sslnode.setSSDESCNODE(SSDESCNODE);
        sslnode.setSSFIELD01(SSFIELD1);
        sslnode.setSSFIELD02(SSFIELD2);
        sslnode.setSSFIELD03(SSFIELD3);
        sslnode.setSSFIELD04(SSFIELD4);
        sslnode.setSSFIELD05(SSFIELD5);
        sslnode.setSSFIELD06(SSFIELD6);

        System.out.println("lebet kadieu ka NDCHG2");
//        List list = (List) request.getSession().getAttribute("parent");
//        map.put("parent", list);
        try {
            URL url = new URL(defUrl + "NDCHG2");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(sslnode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                JSONObject objectPesan = new JSONObject(sb.toString());
                System.out.println(objectPesan.getString("kode"));
                pesan = objectPesan.getString("kode");
            } else {
                pesan = conn.getResponseMessage();
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            pesan = e.getMessage();
            System.out.println("aduh gagal euy : " + e.getMessage());
        }
//        map.put("node", list);

        if (pesan.equals("sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\">UPDATE NODE SUCCSESS</div>");
        } else if (pesan.contains("ConstraintViolationException")) {
            map.put("pesan", "<div class=\"alert alert-danger text-center\">UPDATE NODE INVALID</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\">UPDATE NODE FAILED, CAUSE " + pesan + "</div>");
        }

        return new ModelAndView("body/Node/formUpdateNode", map);
    }

    @RequestMapping(value = "/NDADD", method = RequestMethod.GET)
    private ModelAndView insertNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        System.out.println("lebet insert node");
        Map map = new HashMap();
        List list = new ArrayList<>();
        try {
            //cek URL KETIK
            SessionController scontroller = new SessionController();
            int cekSecure = scontroller.secureURL(request, response);
            if (cekSecure != 0) {
                return new ModelAndView("500");
            }
            //end
            URL url = new URL(defUrl + "NDADD");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            String hasil = "";
            if (out.hasNext()) {
                hasil = out.next();
            }
            JSONObject json = new JSONObject(hasil);
            JSONArray array = json.getJSONArray("parent");

            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
            }
            System.out.println("ieu mah hasil ti web service : " + list);
            request.getSession().setAttribute("parent", list);
            map.put("parent", list);

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            list.add(null);
            list.add(ex.getMessage());
            map.put("parent", list);
        }
        return new ModelAndView("body/Node/formInputNode", map);
    }

    @RequestMapping(value = "/NDADD1", method = RequestMethod.GET)
    private ModelAndView getinsertNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        System.out.println("test out println weh");
        Map map = new HashMap();
        Sslnode sslnode = new Sslnode();
        Ssfndsc ssfndsc = new Ssfndsc();

//        System.out.println("Menuku : " + request.getParameter("nodeid"));
        //SSFNODE//
        SSNODEID = request.getParameter("nodeid");
        SSNODENM = request.getParameter("nodename");
        SSNODEST = request.getParameter("nodestatus");
        SSSECCOD = request.getParameter("securenode");
        SSNODEPRNT = request.getParameter("parentnode");

        if (SSNODEPRNT.equals("null")) {
            SSNODEPRNT = "";
        }

        SSNDIPFUNC = request.getParameter("ipserver");
        SSNODEVER = request.getParameter("version");
        SSNODEURL = request.getParameter("nodeurl");
        SSDESCNODE = request.getParameter("descnode");

        sslnode.setSSNODEID(SSNODEID);
        sslnode.setSSNODENM(SSNODENM);
        sslnode.setSSNODEPRNT(SSNODEPRNT);
        sslnode.setSSNODEST(SSNODEST);
        sslnode.setSSNDIPFUNC(SSNDIPFUNC);
        sslnode.setSSNODEURL(SSNODEURL);
        sslnode.setSSNODEVER(SSNODEVER);
        sslnode.setSSSECCODE(SSSECCOD);
        sslnode.setSSDESCNODE(SSDESCNODE);
        sslnode.setSSFIELD01(SSFIELD1);
        sslnode.setSSFIELD02(SSFIELD2);
        sslnode.setSSFIELD03(SSFIELD3);
        sslnode.setSSFIELD04(SSFIELD4);
        sslnode.setSSFIELD05(SSFIELD5);
        sslnode.setSSFIELD06(SSFIELD6);

        System.out.println("lebet kadieu ka NDADD1");

        String pesan;

        try {
            URL url = new URL(defUrl + "NDADD1");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(sslnode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                JSONObject objectPesan = new JSONObject(sb.toString());
                pesan = objectPesan.getString("kode");
                System.out.println(sb.toString());
            } else {
                pesan = conn.getResponseMessage();
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            pesan = e.getMessage();
            System.out.println("aduh gagal euy : " + e.getMessage());
        }

        if (pesan.equals("sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\">INSERT NODE SUCCSESS</div>");
        } else if (pesan.contains("ConstraintViolationException")) {
            map.put("pesan", "<div class=\"alert alert-danger text-center\">INSERT NODE FAILED, CAUSE NODEID ALREADY EXIST</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\">INSERT NODE FAILED, CAUSE " + pesan + "</div>");
        }

        List list = new ArrayList<>();
        try {
            //cek URL KETIK

            //end
            URL url = new URL(defUrl + "NDADD");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            String hasil = "";
            if (out.hasNext()) {
                hasil = out.next();
            }
            JSONObject json = new JSONObject(hasil);
            JSONArray array = json.getJSONArray("parent");

            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
            }
            System.out.println("alhamdulillah aya hasil : " + list);
            request.getSession().setAttribute("parent", list);
            map.put("parent", list);

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            list.add(null);
            list.add(ex.getMessage());
            map.put("parent", list);
        }

        System.out.println("method insert");
        return new ModelAndView("body/Node/formInputNode", map);
    }

    @RequestMapping(value = "/NDDEL", method = RequestMethod.GET)
    private ModelAndView deleteNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        Map map = new HashMap();
        Sslnode sslnode = new Sslnode();
        
        String node = request.getParameter("node");
        String parent = "";
        List listparent = new ArrayList<>();
        if (node == null) {
            List list = new ArrayList<>();
            try {
                //cek URL KETIK
                SessionController scontroller = new SessionController();
                int cekSecure = scontroller.secureURL(request, response);
                if (cekSecure != 0) {
                    return new ModelAndView("500");
                }
                //end
                URL url = new URL(defUrl + "nodeid");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                InputStream stream = conn.getInputStream();
                Scanner out = new Scanner(stream).useDelimiter("\\A");
                String hasil = "";
                if (out.hasNext()) {
                    hasil = out.next();
                }
                JSONObject json = new JSONObject(hasil);
                JSONArray array = json.getJSONArray("nodeid");

                for (int i = 0; i < array.length(); i++) {
                    list.add(array.get(i));
                }
                System.out.println("ieu hasil na : " + list);
                request.getSession().setAttribute("node", list);
                map.put("node", list);

            } catch (IOException | JSONException ex) {
                System.err.println(ex);
                list.add(null);
                list.add(ex.getMessage());
            }
            return new ModelAndView("body/Node/fromDeleteNode", map);
        } else {
            String pesan = "";
            try {
                org.json.simple.JSONObject jsonode = new org.json.simple.JSONObject();
                jsonode.put("node", node);
                URL url = new URL(defUrl + "NDCHG");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                os.write(gson.toJson(jsonode).getBytes());
                os.flush();
                os.close();
                StringBuilder sb = new StringBuilder();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }

                    br.close();

                } else {
                    System.out.println(conn.getResponseMessage());
                }
                JSONObject listUpdate = new JSONObject(sb.toString());
                pesan = listUpdate.getString("pesan");
                parent = listUpdate.getString("parentnode");
                JSONObject allnode = listUpdate.getJSONObject("allnode");
                JSONArray array = listUpdate.getJSONArray("listparent");
                for (int i = 0; i < array.length(); i++) {
                    listparent.add(array.get(i));
                }
                request.getSession().setAttribute("parent", listparent);

                sslnode.setSSNODEID(allnode.getString("SSNODEID").trim());
                sslnode.setSSNODENM(allnode.getString("SSNODENM").trim());
                sslnode.setSSNODEST(allnode.getString("SSNODEST").trim());
                sslnode.setSSNODEPRNT(allnode.getString("SSNODEPRNT").trim());
                sslnode.setSSNDIPFUNC(allnode.getString("SSNDIPFUNC").trim());
                sslnode.setSSNODEVER(allnode.getString("SSNODEVER").trim());
                sslnode.setSSNODEURL(allnode.getString("SSNODEURL").trim());
                sslnode.setSSSECCODE(allnode.getString("SSSECCODE").trim());
                sslnode.setSSDESCNODE(allnode.getString("SSDESCNODE").trim());
                sslnode.setSSFIELD01(allnode.getString("SSFIELD01").trim());
                sslnode.setSSFIELD02(allnode.getString("SSFIELD02").trim());
                sslnode.setSSFIELD03(allnode.getString("SSFIELD03").trim());
                sslnode.setSSFIELD04(allnode.getString("SSFIELD04").trim());
                sslnode.setSSFIELD05(allnode.getString("SSFIELD05").trim());
                sslnode.setSSFIELD06(allnode.getString("SSFIELD06").trim());

            } catch (IOException | JSONException e) {
                sslnode.setSSNODEST("1");
            }
            String rdb1 = "";
            String rdb2 = "";
            String cbb = "";
            map.put("nameParentNode", parent);
            map.put("nodeid", node);
            map.put("allnode", sslnode);
            map.put("parent", listparent);
            if (sslnode.getSSNODEST().equals("0")) {
                rdb1 = "checked=\"\"";
                cbb = "disabled=\"\"";
            } else {
                rdb2 = "checked=\"\"";
            }

            if (pesan.equals("gagal")) {
                pesan = "disabled=\"\"";
                map.put("pesan", "<div class=\"alert alert-danger text-center\">TO DELETE '" + sslnode.getSSNODENM() + "', YOU MUST DELETE '" + sslnode.getSSNODEID() + " / " + sslnode.getSSNODENM() + "' FROM TREE</div>");
            }

            List list = new ArrayList<>();
            try {
                //cek URL KETIK

                //end
                URL url = new URL(defUrl + "nodeid");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                InputStream stream = conn.getInputStream();
                Scanner out = new Scanner(stream).useDelimiter("\\A");
                String hasil = "";
                if (out.hasNext()) {
                    hasil = out.next();
                }
                JSONObject json = new JSONObject(hasil);
                JSONArray array = json.getJSONArray("nodeid");

                for (int i = 0; i < array.length(); i++) {
                    list.add(array.get(i));
                }
                System.out.println("ieu hasil na : " + list);
//            request.getSession().setAttribute("node", list);

            } catch (IOException | JSONException ex) {
                System.err.println(ex);
                list.add(null);
                list.add(ex.getMessage());
            }
                map.put("node", list);

            map.put("btn", pesan);
            map.put("cbb", cbb);
            map.put("radio1", rdb1);
            map.put("radio2", rdb2);
            return new ModelAndView("body/Node/formGetNodeDelete", map);
        }

    }

    @RequestMapping(value = "/NDDEL1", method = RequestMethod.GET)
    private ModelAndView deletegetNode(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        System.out.println("ieu method update post");
        Map map = new HashMap();
        Sslnode sslnode = new Sslnode();
        SSNODEID = request.getParameter("nodeid");
        System.out.println("lebet kadieu ka NDDEL1");
//        List list = (List) request.getSession().getAttribute("parent");
//        map.put("parent", list);
        String pesan;
        org.json.simple.JSONObject objectNode = new org.json.simple.JSONObject();
        objectNode.put("SSNODEID", SSNODEID);
        try {
            URL url = new URL(defUrl + "NDDEL1");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(objectNode).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();

                System.out.println(sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }
            JSONObject objectPesan = new JSONObject(sb.toString());
            pesan = objectPesan.getString("kode");
        } catch (IOException | JSONException e) {
            System.out.println("aduh gagal euy : " + e.getMessage());
            pesan = e.getMessage();
        }

        if (pesan.equals("sukses")) {
            map.put("pesan", "<div class=\"alert alert-success text-center\">DELETE NODE SUCCSESS</div>");
        } else {
            map.put("pesan", "<div class=\"alert alert-danger text-center\">DELETE NODE FAILED, CAUSE " + pesan + "</div>");

        }
        List list = new ArrayList<>();
        try {
            URL url = new URL(defUrl + "nodeid");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            String hasil = "";
            if (out.hasNext()) {
                hasil = out.next();
            }
            JSONObject json = new JSONObject(hasil);
            JSONArray array = json.getJSONArray("nodeid");

            for (int i = 0; i < array.length(); i++) {
                list.add(array.get(i));
            }
            System.out.println("tah ieu hasil na : " + list);
//            request.getSession().setAttribute("node", list);
            map.put("node", list);

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            list.add(null);
            list.add(ex.getMessage());
        }
//        System.out.println("method delete");
        return new ModelAndView("body/Node/fromDeleteNode", map);
    }
}
