/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import antlr.StringUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rustanian
 */
public class Token {

    private String namaFile = "";
    public String buki_token = "";

    public String getPropLog() {

        Properties prop = new Properties();
        String propFileName = "/bukiweb/conf/url.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(Token.class.getName()).log(Level.SEVERE, null, ex);
        }

        // get the property value and print it out
        namaFile = prop.getProperty("namaFile");
        return null;

    }

    public String readNote(String userid) {
       
        getPropLog();
        String ret = "ret";
        try (BufferedReader br = new BufferedReader(new FileReader(namaFile + userid + ".txt"))) {
            String sCurrentLine;
            String abc[];
            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                abc = sCurrentLine.split("=");
                sCurrentLine = ((String) abc[0]).trim();
//                ret += sCurrentLine ;
                if (sCurrentLine.equals("TOKEN")) {
                    ret +="#"+((String) abc[1]).trim();
                } else if (sCurrentLine.equals("DATE")) {
                    ret +="#"+((String) abc[1]).trim();
                } else if (sCurrentLine.equals("TIME")) {
                    ret +="#"+((String) abc[1]).trim();
                } else if (sCurrentLine.equals("USERID")) {
                    ret +="#"+((String) abc[1]).trim();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            ret="err :"+e.getMessage();
        }

        return ret;
    }

    public static void main(String[] args) {
        try {
            Token t = new Token();
            ArrayList list=new ArrayList();
//            t.readNote("BK20131001");
            t.CreateToken("BK20911038",list);
        } catch (Exception e) {
            System.out.println("err " + e.getMessage());
        }

    }
     
    public String CreateToken(String userid,ArrayList list) {
         
        try {
            getPropLog();
            System.out.println("nama file :" + namaFile);
            Random rand = new Random();

            Date date = new Date();
            DateFormat Formatjam = new SimpleDateFormat("mmss");
            DateFormat Formatjam2 = new SimpleDateFormat("HHmmss");
            DateFormat Formathari = new SimpleDateFormat("yyyyMMdd");
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new FileWriter(namaFile + userid + ".txt", false)));
//                    new FileWriter("D:/" + userid + ".txt", false)));
            
            
            String jam = Formatjam.format(date);
            String hari = Formathari.format(date);
            int a = Integer.parseInt(jam);
            String token = "";
            String profil = "";
            System.out.println("jam : " + a);
            int n = rand.nextInt(a);
            for (int i = 0; i < 8; i++) {
                jam = Formatjam.format(date);
                a = Integer.parseInt(jam);
                n = rand.nextInt(a);
                token += org.apache.commons.lang.StringUtils.rightPad(String.valueOf(n), 4, "0");
                System.out.println("token :" + token);
            }
            
           
        
//            profil= String.format("%1$"+30+"s",(list.get(0).toString()))+ String.format("%1$"+10+"s",(list.get(1).toString()))+String.format("%1$"+30+"s",(list.get(2).toString()))+String.format("%1$"+30+"s",(list.get(3).toString()));
            profil= (list.get(0).toString()+String.format("%1$"+30+"s",(""))).substring(0,30)+ (list.get(1).toString()+String.format("%1$"+10+"s",(""))).substring(0,10)+(list.get(2).toString()+String.format("%1$"+30+"s",(""))).substring(0,30)+(list.get(3).toString()+String.format("%1$"+30+"s",(""))).substring(0,30);
            profil+= (list.get(4).toString()+String.format("%1$"+30+"s",(""))).substring(0,30)+(list.get(5).toString()+String.format("%1$"+10+"s",(""))).substring(0,10)+(list.get(6).toString()+String.format("%1$"+8+"s",(""))).substring(0,8)+(list.get(7).toString()+String.format("%1$"+10+"s",(""))).substring(0,10);
            profil+= (list.get(8).toString()+String.format("%1$"+10+"s",(""))).substring(0,10)+(list.get(9).toString()+String.format("%1$"+30+"s",(""))).substring(0,30)+(list.get(10).toString()+String.format("%1$"+4+"s",(""))).substring(0,4)+(list.get(11).toString()+String.format("%1$"+4+"s",(""))).substring(0,4);
            profil+= (list.get(12).toString()+String.format("%1$"+3+"s",(""))).substring(0,3)+(list.get(13).toString()+String.format("%1$"+2+"s",(""))).substring(0,2)+(list.get(14).toString()+String.format("%1$"+10+"s",(""))).substring(0,10)+(list.get(15).toString()+String.format("%1$"+16+"s",(""))).substring(0,16);
            profil+= (list.get(15).toString()+String.format("%1$"+16+"s",(""))).substring(0,16)+(list.get(16).toString()+String.format("%1$"+16+"s",(""))).substring(0,16)+(list.get(17).toString()+String.format("%1$"+16+"s",(""))).substring(0,16)+(list.get(18).toString()+String.format("%1$"+4+"s",(""))).substring(0,4);
            profil+= (list.get(19).toString()+String.format("%1$"+1+"s",(""))).substring(0,1)+(list.get(20).toString()+String.format("%1$"+1+"s",(""))).substring(0,1)+(list.get(21).toString()+String.format("%1$"+1+"s",(""))).substring(0,1)+(list.get(22).toString()+String.format("%1$"+30+"s",(""))).substring(0,30);
            profil+=(list.get(24).toString()+String.format("%1$"+10+"s",(""))).substring(0,10);
           
            out.println("DATE = " + hari);
            out.println("TIME = " + Formatjam2.format(date));
            out.println("USERID = " + userid);
            out.println("TOKEN = " + token);
            out.println("PROFILE =" + profil);
//            out.println("PROFILE = " + list.get(0)+ list.get(1)+list.get(2)+list.get(3)+ list.get(4)+ list.get(5)+list.get(6)+list.get(7)+ list.get(8)+ list.get(9)+list.get(10)+list.get(11));
            buki_token=token;
            out.close();
        } catch (Exception e) {
            System.out.println("err : " + e.getMessage());
            Logger.getLogger(Token.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

}
