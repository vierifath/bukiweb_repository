package bukiweb.controller;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReleaseUser {

    private final Gson gson = new Gson();
    String defUrl;
    String userlogin;
    String func = "UAAUB1";
    String title = "RELEASE USER BUKIWEB";
    String alamat = "getUserid";

    @RequestMapping(value = "/getNamaUser", method = RequestMethod.GET)
    @ResponseBody
    private String getNamaUser(HttpServletRequest request, HttpServletResponse response) {
//        System.out.println("test disini re");
        return null;
    }

    public String nextp() {
        String nextp = "";

        Properties prop = new Properties();

        String propFileName = "/bukiweb/conf/release.properties";
        InputStream inputStream = getClass().getResourceAsStream(propFileName);
        try {
            prop.load(inputStream);
        } catch (Exception e) {
            System.err.println("Error Properties : " + e.getMessage());
        }

        nextp = prop.getProperty("nextp");
        return nextp;
    }

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();

        defUrl = pc.prop();

        System.out.println("URL : " + defUrl);
    }

    private ModelAndView getUser(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        func = request.getSession().getAttribute("func").toString();
        title = request.getSession().getAttribute("title").toString();
        userlogin = request.getSession().getAttribute("userlogin").toString();
        Map map = new HashMap();
        org.json.simple.JSONObject jsonogrupid = new org.json.simple.JSONObject();
        ArrayList GetGrupID = (ArrayList) request.getSession().getAttribute("profil");
        jsonogrupid.put("grupid", GetGrupID.get(7));
        jsonogrupid.put("nodeid", "UAAUB");
        jsonogrupid.put("ubatid", GetGrupID.get(8));
        jsonogrupid.put("nextp", nextp());
        jsonogrupid.put("func", func);
        jsonogrupid.put("userlogin", userlogin);
        System.out.println("func1 : " + func);
//        System.out.println(GetGrupID.get(7));
        try {
            URL url = new URL(defUrl + "UAAUB");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonogrupid).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            } else {
                System.out.println(conn.getResponseMessage());
            }

            JSONObject json = new JSONObject(sb.toString());
            String release = json.getString("ret_release");
            request.getSession().setAttribute("release", release);
            System.out.println("func2 : " + func);
            map.put("release", release);
            map.put("func", func);
            map.put("title", title);

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
        }
        return new ModelAndView("body/ReleaseUser/displayReleaseUser", map);
    }

    @RequestMapping(value = "/UAAUB", method = RequestMethod.GET)
    private ModelAndView getReleaseUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //cek URL KETIK
        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        //end
        ServletContext sc = request.getServletContext().getContext(request.getContextPath() + "/UAAUB");
        request.getSession().setAttribute("userlogin", sc.getAttribute("buki_userlogin"));
        request.getSession().setAttribute("func", "UAAUB1");
        request.getSession().setAttribute("title", "RELEASE USER BUKIWEB");
        func = "UAAUB1";
        title = "RELEASE USER BUKIWEB";
        return getUser(request, response);
    }

    @RequestMapping(value = "/UAAUB1", method = RequestMethod.GET)
    private ModelAndView updateReleaseUser(HttpServletRequest request, HttpServletResponse response) {

        getPropUrl();

        String pesan;
        Map map = new HashMap();
        org.json.simple.JSONObject jsonouserid = new org.json.simple.JSONObject();
        String userId = request.getParameter("userlogin"); //Meminta Parameter dari depan berdasarkan nama
        System.out.println("test :" + userId);
        String[] arr = userId.split("/");
        jsonouserid.put("userId", arr[0]);
        System.out.println("asup oge kadieu : " + arr[0]);
        try {
            URL url = new URL(defUrl + "getUserid");
            alamat = "getUserid";
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonouserid).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                JSONObject jsonoPesan = new JSONObject(sb.toString());
                pesan = jsonoPesan.getString("pesan");
                map.put("release", jsonoPesan.getString("release"));
            } else {
                pesan = conn.getResponseMessage();
//                System.out.println(conn.getResponseMessage());
            }

        } catch (IOException | JSONException ex) {
//            System.err.println(ex);
            pesan = ex.getMessage();
        }

        if (pesan.equals("sukses")) {
            map.put("msg", "<div class=\"alert alert-success text-center\"> RELEASE USER " + userId + " SUKSES</div>");
        } else {
            map.put("msg", "<div class=\"alert alert-danger text-center\"> RELEASE USER " + userId + " FAILED, CAUSE " + pesan.toUpperCase() + "</div>");
        }

        userlogin = request.getSession().getAttribute("userlogin").toString();
        org.json.simple.JSONObject jsonogrupid = new org.json.simple.JSONObject();
        jsonogrupid.put("userlogin", userlogin);
        String hasil = "";
        try {
            URL url = new URL(defUrl + "user");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            if (out.hasNext()) {
                hasil = out.next();

            }

            System.out.println("hasil : " + hasil);
//            JSONObject json = new JSONObject(hasil);
//            String release = json.getString("ret_release");
        } catch (Exception e) {
        }

        func = request.getSession().getAttribute("func").toString();
        map.put("release", hasil);
        map.put("func", func);
        map.put("title", "RELEASE USER BUKIWEB");
        return new ModelAndView("body/ReleaseUser/displayReleaseUser", map);
    }

    @RequestMapping(value = "/UAAUR", method = RequestMethod.GET)
    private ModelAndView getReleaseUser400(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //cek URL KETIK
        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        //end
        ServletContext sc = request.getServletContext().getContext(request.getContextPath() + "/UAAUB");
        request.getSession().setAttribute("userlogin", sc.getAttribute("buki_userlogin"));
        request.getSession().setAttribute("func", "UAAUR1");
        request.getSession().setAttribute("title", "RELEASE USER AS400");
        func = "UAAUR1";
        title = "RELEASE USER AS400";
        return getUser(request, response);
    }

    @RequestMapping(value = "/UAAUR1", method = RequestMethod.GET)
    private ModelAndView updateReleaseUser400(HttpServletRequest request, HttpServletResponse response) {
        func = request.getSession().getAttribute("func").toString();
        alamat = "releaseUserAs";
        getPropUrl();
        String pesan;

        Map map = new HashMap();
        org.json.simple.JSONObject jsonouserid = new org.json.simple.JSONObject();
        String userId = request.getParameter("userlogin"); //Meminta Parameter dari depan berdasarkan nama
        System.out.println("test :" + userId);
        String[] arr = userId.split("/");
        jsonouserid.put("userId", arr[0]);
        try {
            URL url = new URL(defUrl + "releaseUserAs");
            alamat = "getUserid";
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonouserid).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                JSONObject objectPesan = new JSONObject(sb.toString());
                pesan = objectPesan.getString("pesan");
            } else {
                pesan = conn.getResponseMessage();
                System.out.println(conn.getResponseMessage());
            }

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            pesan = ex.toString();
        }

        if (pesan.contains("sukses")) {
            map.put("msg", "<div class=\"alert alert-success text-center\"> RELEASE USER " + userId + " SUKSES</div>");

        } else {
            map.put("msg", "<div class=\"alert alert-danger text-center\"> RELEASE USER " + userId + " FAILED, CAUSE " + pesan.toUpperCase() + "</div>");

        }

        String hasil = "";
        try {
            URL url = new URL(defUrl + "userAs");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream stream = conn.getInputStream();
            Scanner out = new Scanner(stream).useDelimiter("\\A");
            if (out.hasNext()) {
                hasil = out.next();

            }

            System.out.println("hasil : " + hasil);
//            JSONObject json = new JSONObject(hasil);
//            String release = json.getString("ret_release");
        } catch (Exception e) {
        }
        
        func = request.getSession().getAttribute("func").toString();
        map.put("release", hasil);
        map.put("func", func);
        func = "UAAUR1";
        map.put("title", "RELEASE USER AS400");
        return new ModelAndView("body/ReleaseUser/displayReleaseUser", map);
//        return new ModelAndView("body/ReleaseUser/displayReleaseUser400");
    }

    String aksi;

    @RequestMapping(value = "/CUPBSE", method = RequestMethod.GET)
    private ModelAndView getUserEn(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //cek URL KETIK
        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        //end
        ServletContext sc = request.getServletContext().getContext(request.getContextPath() + "/UAAUB");
        request.getSession().setAttribute("userlogin", sc.getAttribute("buki_userlogin"));
        request.getSession().setAttribute("func", "enabledUser");
        request.getSession().setAttribute("title", "ENABLE USER PROFILE");
        request.getSession().setAttribute("aksi", "1");
        func = "enabledUser";
        aksi = "1";
        title = "ENABLE USER PROFILE";
        return getUser(request, response);
    }

    @RequestMapping(value = "/CUPBSD", method = RequestMethod.GET)
    private ModelAndView getUserDis(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //cek URL KETIK
        SessionController scontroller = new SessionController();
        int cekSecure = scontroller.secureURL(request, response);
        if (cekSecure != 0) {
            return new ModelAndView("500");
        }
        //end
        ServletContext sc = request.getServletContext().getContext(request.getContextPath() + "/UAAUB");
        request.getSession().setAttribute("userlogin", sc.getAttribute("buki_userlogin"));
        request.getSession().setAttribute("func", "enabledUser");
        request.getSession().setAttribute("title", "DISABLE USER PROFILE");
        request.getSession().setAttribute("aksi", "2");
        func = "enabledUser";
        aksi = "2";
        title = "DISABLE USER PROFILE";
        return getUser(request, response);
    }

    @RequestMapping(value = "/enabledUser", method = RequestMethod.GET)
    private ModelAndView enabledUser(HttpServletRequest request) {

        Map map = new HashMap();

        aksi = request.getSession().getAttribute("aksi").toString();
        title = request.getSession().getAttribute("title").toString();
        String userId = request.getParameter("userlogin");
        String[] user = userId.split("/");
        org.json.simple.JSONObject objectParam = new org.json.simple.JSONObject();
        objectParam.put("userid", user[0]);
        objectParam.put("aksi", aksi);
        String pesan;

        System.out.println(userId);

        List listGrupId = new ArrayList<>();
        try {
            URL url = new URL(defUrl + "enableUser");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(objectParam).getBytes());

            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                pesan = sb.toString();
                br.close();
//                System.out.println(sb.toString());
            } else {
                pesan = conn.getResponseMessage();
                System.out.println(conn.getResponseMessage());
            }

        } catch (IOException | JSONException ex) {
            System.err.println(ex);
            pesan = ex.getMessage();
//            Logger.getLogger(getWebService.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (pesan.contains("Sukses")) {
            map.put("msg", "<div class=\"alert alert-success text-center\"> " + title + " USER " + userId + " SUKSES</div>");
        } else {
            map.put("msg", "<div class=\"alert alert-danger text-center\"> " + title + " USER " + userId + " " + pesan.toUpperCase() + "</div>");
        }
        map.put("func", "enabledUser");
        map.put("release", request.getSession().getAttribute("release"));
        map.put("title", title);

        return new ModelAndView("body/ReleaseUser/displayReleaseUser", map);

    }
}
