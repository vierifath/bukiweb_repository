/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import bukiweb.model.LlfglobalId;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rustanian
 */
@Controller
public class ValidasiNode {

    Gson gson = new Gson();
    String defUrl;
    String appURL;

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();

        defUrl = pc.prop();
        appURL = pc.appURL();
        System.out.println("URL : " + defUrl);
    }

    @RequestMapping(value = "/cekSession", method = RequestMethod.GET)
    public @ResponseBody
    String cekSession(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
        String ret = "";
        String note = "";
        String logou = "";
        String paramuser = request.getParameter("userid");
        System.out.println("param " + paramuser);
        LoginController loginController = new LoginController();
        if (request.getSession().getAttribute("profil") == null) {
            ret = "1#Session is denied please login again";
            //remove session
            //jalankan fungsi logout
            System.out.println("logut " + loginController.logout(request));
        } else {
            getPropUrl();
            ArrayList list = new ArrayList();
            list = (ArrayList) request.getSession().getAttribute("profil");
            System.out.println(list.get(1));
            System.out.println(list.get(9));
            org.json.simple.JSONObject userpass = new org.json.simple.JSONObject();

            userpass.put("username", list.get(1));
            userpass.put("password", list.get(9));
            
            Enkripsi ek = new Enkripsi();
            
            StringBuilder sb = new StringBuilder();
            try {
                URL url = new URL(defUrl + "cekUserEnable");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                os.write(gson.toJson(userpass).getBytes());
                os.flush();
                os.close();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }

                    br.close();
                    System.out.println(sb.toString());
                    JSONObject listDashboard = new JSONObject(sb.toString());
                    String kode = listDashboard.getString("errcode");
                    String msg = listDashboard.getString("errmsg");
                    if (kode.equals("0")) {
                        ret = "0#" + msg;
                        //read note || check tanggallogin
                        Token token = new Token();
                        Date date = new Date();
                        String abc[];
                        DateFormat formathari = new SimpleDateFormat("yyyyMMdd");
                        String hari = formathari.format(date);
                        String tanggal = "";

                        note = token.readNote(paramuser);
                        abc = note.split("#");
                        tanggal = ((String) abc[1]).trim();
                        if (hari.equals(tanggal)) {
                            ret = "0#" + msg;
                        } else {
                            ret = "4#" + " Expired date, please login again !!!";
                            logou = loginController.logout(request);
                        }
                    } else {
                        ret = "1#" + msg;
                        logou = loginController.logout(request);
                    }
                } else {
                    System.out.println(conn.getResponseMessage());
                    ret = "2#" + conn.getResponseMessage();
                }
            } catch (Exception e) {
                ret = "3#" + e.getMessage()+"#Profile not valid";
            }
        }
        return ret;
//        return "1#Session is denied please login again";
    }

    public static void main(String[] args) {
        try {
            String note, paramuser = "BK20131001";
            Token token = new Token();
            Date date = new Date();
            String abc[];
            DateFormat formathari = new SimpleDateFormat("yyyyMMdd");
            String hari = formathari.format(date);
            String tanggal = "";

            note = token.readNote(paramuser);
            System.out.println("note " + note);
            abc = note.split("#");
            tanggal = ((String) abc[1]).trim();
            if (hari.equals(tanggal)) {
                System.out.println("sukses");
            } else {
                System.out.println(" Expired date, please login again !!!");

            }
        } catch (Exception e) {
            System.out.println("err " + e.getMessage());
        }
        //read note || check tanggallogin

    }

    public String LogGlobal(LlfglobalId llfglobalId) throws IOException {
        getPropUrl();
        String ret = "";
        Date date = new Date();
        DateFormat Formathari = new SimpleDateFormat("yyyyMMdd");
        DateFormat Formatjam = new SimpleDateFormat("HHmmss");
        String hari = Formathari.format(date);
        String jam = Formatjam.format(date);

        org.json.simple.JSONObject userpass = new org.json.simple.JSONObject();
        System.out.println("param di fungsi : " + llfglobalId.getLldata1());
        System.out.println("param di fungsi2 : " + gson.toJson(llfglobalId.getLldata1()));
        userpass.put("user", llfglobalId.getLluser());
        userpass.put("terminal", llfglobalId.getLltrmid());
        userpass.put("module", llfglobalId.getLlmodul());
        userpass.put("application_name", llfglobalId.getLlappnm());
        userpass.put("application_function", llfglobalId.getLlappfunc());
        userpass.put("mode", llfglobalId.getLlmode());
        userpass.put("param", llfglobalId.getLldata1());
        userpass.put("date", hari);
        userpass.put("time", jam);
        userpass.put("msg", llfglobalId.getLlerrmsg());

        URL url = new URL(defUrl + "logGlobal");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestMethod("POST");
        OutputStream os = conn.getOutputStream();
        os.write(gson.toJson(userpass).getBytes());
        os.flush();
        os.close();
        StringBuilder sb = new StringBuilder();
        int HttpResult = conn.getResponseCode();
        if (HttpResult == HttpURLConnection.HTTP_OK) {
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }

            br.close();
            System.out.println("ret " + sb.toString());
            if (sb.toString().contains("sukses")) {
                ret = "sukses";
            } else {
                ret = "Gagal";
            }

        } else {
            System.out.println(conn.getResponseMessage());
            ret = conn.getResponseMessage();
        }

        conn.disconnect();
        return ret;
    }

    @RequestMapping(value = "/validationPage", method = RequestMethod.GET)
    private ModelAndView getNode(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
        String alamat_url = null, ses_dita = null, appName = null, userid, ret, servRus = null;
        String alamat_servlet = null;
//        String paramNilai[] = null;
        String paramNilai = null;
        String nodeid = null;
        String namaClass = null;
        String[] tempAlamatUrl;
        List list = null;
         
        paramNilai=(String) request.getParameter("app").trim();
        paramNilai+= "="+(String) request.getParameter("modul").trim();
        paramNilai+= "="+(String) request.getParameter("userid").trim();
        paramNilai+= "="+(String) request.getParameter("seccode").trim();
        paramNilai+= "="+(String) request.getParameter("verson").trim();
        paramNilai+= "="+(String) request.getParameter("ipfunc").trim();
        paramNilai+= "="+(String) request.getParameter("desknode").trim();
        paramNilai+= "="+(String) request.getParameter("nodetea").trim();
        paramNilai+= "="+(String) request.getParameter("levfac").trim();
        
        String macaddr = (String) request.getSession().getAttribute("macaddr");
        //add security URL GET
        request.getSession().setAttribute("buki_secure", "rustanian");
        LlfglobalId id = new LlfglobalId();
        try {
            System.out.println("sess : " + request.getSession().getAttribute("profil"));
            if (request.getSession().getAttribute("profil") == null) {
                System.out.println("cek 1");
                request.getSession().setAttribute("Errorlogin", "YOU MUST LOGIN FIRST <br>");
                return new ModelAndView("redirect:login");
            } else {
                
                request.getRequestURL();

                list = (List) request.getSession().getAttribute("profil");
                tempAlamatUrl = request.getParameter("tujuan").split("/");

                alamat_url = request.getParameter("tujuan");
                servRus = request.getParameter("servRus");
//                servRus=servRus.replace("/WebClient/Bukiweb/dashboard#", "");
//                servRus=servRus.replace("/WebClient/Bukiweb/dashboard", "");
                servRus = servRus.replace("/web/dashboard#", "");
                servRus = servRus.replace("/web/dashboard", "");
//                System.out.println("alam " + alamat_url);

                System.out.println("url : " + servRus + alamat_url);
//                URL url = new URL(servRus + alamat_url);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setRequestMethod("GET");
//                conn.connect();
//                int HttpResult = conn.getResponseCode();
                int HttpResult = 200;
                System.out.println("httpresult : " + HttpResult);
                
                if (HttpResult == 404) {
//                    alamat_url += "/" + nodeid;
                    nodeid = tempAlamatUrl[tempAlamatUrl.length - 1];
                    System.out.println("nodeid : " + nodeid);
                    namaClass = tempAlamatUrl[tempAlamatUrl.length - 2];
                    System.out.println("namaClass : " + namaClass);
                    alamat_url = alamat_url.replaceAll("/" + nodeid, "");
                }

                ses_dita = request.getParameter("modul");
//                System.out.println("alam 2 " + ses_dita);
                appName = request.getParameter("app");

//                System.out.println("alam 3 " + appName);
                appName = appName.replace("%20", " ");
                org.json.simple.JSONObject param = new org.json.simple.JSONObject();

                param.put("buki_userlogin", (list.get(1)).toString().trim());
                param.put("buki_username", (list.get(0)).toString().trim());
                param.put("buki_group", (list.get(2)).toString().trim());
                param.put("buki_cabang", (list.get(3)).toString().trim());
                param.put("buki_lokasi", (list.get(4)).toString().trim());
                System.out.println("alamat url  :" + alamat_url);
                id.setLlappfunc(alamat_url);
                id.setLlappnm(ses_dita.trim());
                id.setLldata1(param.toString());

                id.setLlerrmsg("");
                id.setLlmode("DEBUG");

                id.setLlmodul("BUKIWEB");
                id.setLltrmid(macaddr);
                id.setLluser((String) list.get(1));

                ret = LogGlobal(id);
                if (ret.contains("sukses")) {
                    System.out.println("sukses log " + ret);
                } else {
                    System.out.println("err log " + ret);
                }
                if (alamat_url.startsWith("http://") == true) {

                    String url_temp = alamat_url;
                    System.out.println("masuk sini " + alamat_url);


                    System.out.println("disini aja");
                    alamat_servlet = alamat_url.replace("http://", "");
                    String[] temp;
                    System.out.println("alamar-servlet : " + alamat_servlet);
                    temp = alamat_servlet.split("/");
                    alamat_servlet = temp[1];

                    ServletContext sc = request.getServletContext().getContext("/");

                   
                    sc.setAttribute("buki_userlogin", list.get(1));
                    sc.setAttribute("buki_username", list.get(0));
                    sc.setAttribute("buki_group", list.get(2));
                    sc.setAttribute("buki_cabang", list.get(3));
                    sc.setAttribute("buki_lokasi", list.get(4));
                    //tambah security URL GET
                    sc.setAttribute("buki_secure", request.getParameter("seccode"));
                    sc.setAttribute("buki_ipfunc", request.getParameter("ipfunc"));
                    //tambah buat form
                   
                    sc.setAttribute("form_name", request.getParameter("modul"));
                    sc.setAttribute("verson", request.getParameter("verson").replace("-", "."));
                    sc.setAttribute("verson", request.getParameter("verson").replace("-", "."));
                    sc.setAttribute("buki_cabangCode", list.get(10));
                    sc.setAttribute("buki_lokasiCode", list.get(11));
                    sc.setAttribute("buki_grupid", list.get(7));
                    sc.setAttribute("buki_password", list.get(9));

                    sc.setAttribute("buki_desknode", request.getParameter("desknode"));
                    sc.setAttribute("buki_token", request.getSession().getAttribute("buki_token"));
                    sc.setAttribute("buki_node", request.getParameter("nodetea"));
                    sc.setAttribute("buki_levfac", request.getParameter("levfac"));
                    sc.setAttribute("buki_ipmain", servRus);
                    sc.setAttribute("buki_url", alamat_url);

                    sc.setAttribute("buki_dppcode", list.get(12));
                    sc.setAttribute("buki_bu", list.get(13));
                    sc.setAttribute("buki_ubatid", list.get(14));
                    sc.setAttribute("buki_ulimo", list.get(15));
                    sc.setAttribute("buki_ulimi", list.get(16));
                    sc.setAttribute("buki_ulorak", list.get(17));
                    sc.setAttribute("buki_ulirak", list.get(18));
                    sc.setAttribute("buki_ukas", list.get(19));
                    sc.setAttribute("buki_fe", list.get(20));
                    sc.setAttribute("buki_nonfe", list.get(21));
                    sc.setAttribute("buki_clcrdt", list.get(22));
                    System.out.println("akhir : " + list.get(22));

                    id.setLlmode("INFO");
                    id.setLlerrmsg("Sukses");
                    ret = LogGlobal(id);
                    if (ret.contains("sukses")) {
                        System.out.println("sukses log " + ret);
                    } else {
                        System.out.println("err log " + ret);
                    }
                    return new ModelAndView("redirect:http://localhost:8084/web/getTest");

                } else {
                    ServletContext sc = request.getServletContext().getContext(alamat_url);
//                    ServletContext sc = request.getServletContext().getContext(request.getContextPath() + nodeId);

                   
//                    sc = request.getServletContext().getContext( alamat_url);
                    sc.setAttribute("buki_userlogin", list.get(1));
                    sc.setAttribute("buki_username", list.get(0));
                    sc.setAttribute("buki_group", list.get(2));
                    sc.setAttribute("buki_cabang", list.get(3));
                    sc.setAttribute("buki_lokasi", list.get(4));
                    //tambah security URL GET
                    sc.setAttribute("buki_secure", request.getParameter("seccode"));
                    sc.setAttribute("buki_ipfunc", request.getParameter("ipfunc"));
                    System.out.println("request.getParameter(\"ipfunc\") ="+request.getParameter("ipfunc"));
                    //tambah buat form
                    System.out.println(request.getParameter("modul"));
                    sc.setAttribute("form_name", request.getParameter("modul"));
                    sc.setAttribute("verson", request.getParameter("verson").replace("-", "."));
                    sc.setAttribute("verson", request.getParameter("verson").replace("-", "."));
                    sc.setAttribute("buki_cabangCode", list.get(10));
                    sc.setAttribute("buki_lokasiCode", list.get(11));
                    sc.setAttribute("buki_grupid", list.get(7));
                    sc.setAttribute("buki_password", list.get(9));
                    
                    sc.setAttribute("buki_desknode", request.getParameter("desknode"));
                    sc.setAttribute("buki_token", request.getSession().getAttribute("buki_token"));
                    sc.setAttribute("buki_node", request.getParameter("nodetea"));
                    sc.setAttribute("buki_levfac", request.getParameter("levfac"));
                    sc.setAttribute("buki_ipmain", servRus);
                    sc.setAttribute("buki_url", alamat_url);
                  
                    
                    sc.setAttribute("buki_dppcode", list.get(12));
                    sc.setAttribute("buki_bu", list.get(13));
                    sc.setAttribute("buki_ubatid", list.get(14));
                    sc.setAttribute("buki_ulimo", list.get(15));
                    sc.setAttribute("buki_ulimi", list.get(16));
                    sc.setAttribute("buki_ulorak", list.get(17));
                    sc.setAttribute("buki_ulirak", list.get(18));
                    sc.setAttribute("buki_ukas", list.get(19));
                    sc.setAttribute("buki_fe", list.get(20));
                    sc.setAttribute("buki_nonfe", list.get(21));
                    sc.setAttribute("buki_clcrdt", list.get(22));

                    sc.setAttribute("nodeid", nodeid);
                    sc.setAttribute("class", namaClass);

                    id.setLlmode("INFO");
                    id.setLlerrmsg("Sukses");
                    ret = LogGlobal(id);
                    if (ret.contains("sukses")) {
                        System.out.println("sukses log " + ret);
                    } else {
                        System.out.println("err log " + ret);
                    }
                    Map map = new HashMap();
                    map.put("sc", sc);
                    System.out.println("redirect:" + servRus + alamat_url+"?paramNilai="+paramNilai);
                    return new ModelAndView("redirect:" + servRus + alamat_url+"?paramNilai="+paramNilai);
//               
                }
            }
        } catch (Exception e) {
            System.out.println("catch " + e.getMessage());
            org.json.simple.JSONObject param = new org.json.simple.JSONObject();

            param.put("gagal", e.getMessage());
            id.setLldata1(param.toString());
            id.setLlmode("Catch");
            id.setLlerrmsg("err : " + e.getMessage());

            id.setLlappfunc(alamat_url);
            id.setLlappnm(ses_dita.trim());
            id.setLlmodul(appName.trim());
            id.setLltrmid(macaddr);
            id.setLluser((String) list.get(1));

            ret = LogGlobal(id);
            if (ret.contains("sukses")) {
                System.out.println("sukses log " + ret);
            } else {
                System.out.println("err log " + ret);
            }
            System.out.println("cek 2 " + e.getMessage());
            return new ModelAndView("500");
        }
    }
}
