/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.controller;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author HP
 */
@Controller
public class ChangePictureController {

    Gson gson = new Gson();

    String defUrl;

    public void getPropUrl() {

        PropertiesController pc = new PropertiesController();

        defUrl = pc.prop();

        System.out.println("ieu teh URL : " + defUrl);
    }

    @RequestMapping(value = "/changePicture", method = RequestMethod.GET)
    private ModelAndView changePicture(HttpServletRequest request, HttpServletResponse response) throws IOException {

        getPropUrl();
        System.out.println("asup ka change Picture");
        Map map = new HashMap();
        //cek URL KETIK
        String pathDef = "../img/userWhite.jpg";
        String Username = request.getSession().getAttribute("username").toString();
        String path = request.getSession().getAttribute("path").toString();
        List listPath = new ArrayList();
//        listPath.add(path);
        org.json.simple.JSONObject jsonoParam = new org.json.simple.JSONObject();
        jsonoParam.put("username", Username);
        String pesanerr = "";
        try {
            URL url = new URL(defUrl + "getPicture");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonoParam).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();

                System.out.println(sb.toString());
                JSONObject jsonoPath = new JSONObject(sb.toString());
                JSONArray jsonaPath = jsonoPath.getJSONArray("listPath");
                for (int i = 0; i < jsonaPath.length(); i++) {
                    listPath.add(jsonaPath.get(i));
                }
                System.out.println("lamun ieu listPath : " + listPath);
                map.put("listPath", listPath);
            } else {
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            System.err.println("e : " + e);
            pesanerr = "<div class=\"alert alert-danger text-center\">" + e.getMessage() + "</div>";
        }
//        map.put("path", path);
        map.put("pesanerr", pesanerr);
//        map.put("count", count);
//        map.put("listPath", listPath);
        //end
        return new ModelAndView("changePicture", map);
//        return new ModelAndView("dashboard", map);
    }

    @RequestMapping(value = "/savePicture", method = RequestMethod.POST)
    private @ResponseBody
    String savePicture(HttpServletRequest request, HttpServletResponse response) throws IOException {

        System.out.println("alhamdulillah lebet ka save Picture");
        Map map = new HashMap();

        String idx = request.getParameter("idx");
        String path = request.getParameter("path").replaceAll(" ", "+");
        String Username = request.getSession().getAttribute("username").toString();
        org.json.simple.JSONObject jsonoParam = new org.json.simple.JSONObject();
        jsonoParam.put("username", Username);
        jsonoParam.put("path", path);
        jsonoParam.put("idx", idx);
        String pesanerr = "";
        String ret = "";
        try {
            URL url = new URL(defUrl + "savePicture");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(gson.toJson(jsonoParam).getBytes());
            os.flush();
            os.close();
            StringBuilder sb = new StringBuilder();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();

                pesanerr = sb.toString();
            } else {
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            System.err.println("e : " + e);
            pesanerr = e.getMessage();
        }

        if (pesanerr.contains("sukses")) {
            ret = "0";
        } else {
            ret = "1";
        }

        return ret;
//        return new ModelAndView("dashboard", map);
    }

    public static void main(String[] args) throws IOException {
        String test = "01232434";
        if (!test.contains("43")) {
            System.out.println("kanggo test hungkul : " + test);
        }
    }
}
