/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.model;

import java.io.Serializable;

/**
 *
 * @author Rustanian
 */
public class Ssltree2 implements Serializable {

    String SSMAPID;
    String SSGRPID;
    String SSNODEID;
    String SSPARENT;
    String SSNODEPR;
    String SSLVLFAC;

    public String getSSMAPID() {
        return SSMAPID;
    }

    public void setSSMAPID(String SSMAPID) {
        this.SSMAPID = SSMAPID;
    }

    public String getSSGRPID() {
        return SSGRPID;
    }

    public void setSSGRPID(String SSGRPID) {
        this.SSGRPID = SSGRPID;
    }

    public String getSSNODEID() {
        return SSNODEID;
    }

    public void setSSNODEID(String SSNODEID) {
        this.SSNODEID = SSNODEID;
    }

    public String getSSPARENT() {
        return SSPARENT;
    }

    public void setSSPARENT(String SSPARENT) {
        this.SSPARENT = SSPARENT;
    }

    public String getSSNODEPR() {
        return SSNODEPR;
    }

    public void setSSNODEPR(String SSNODEPR) {
        this.SSNODEPR = SSNODEPR;
    }

    public String getSSLVLFAC() {
        return SSLVLFAC;
    }

    public void setSSLVLFAC(String SSLVLFAC) {
        this.SSLVLFAC = SSLVLFAC;
    }

}
