/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.model;

import java.io.Serializable;

/**
 *
 * @author Rustanian
 */
public class Ssfuser implements Serializable {

    String SSUIDSGN;
    String SSGRPID;
    String SSSTSFNGR;
    String SSSTSLGN;
    String SSSTSFE;
    String SSSTSNONFE;

    public String getSSUIDSGN() {
        return SSUIDSGN;
    }

    public void setSSUIDSGN(String SSUIDSGN) {
        this.SSUIDSGN = SSUIDSGN;
    }

    public String getSSGRPID() {
        return SSGRPID;
    }

    public void setSSGRPID(String SSGRPID) {
        this.SSGRPID = SSGRPID;
    }

    public String getSSSTSFNGR() {
        return SSSTSFNGR;
    }

    public void setSSSTSFNGR(String SSSTSFNGR) {
        this.SSSTSFNGR = SSSTSFNGR;
    }

    public String getSSSTSLGN() {
        return SSSTSLGN;
    }

    public void setSSSTSLGN(String SSSTSLGN) {
        this.SSSTSLGN = SSSTSLGN;
    }

    public String getSSSTSFE() {
        return SSSTSFE;
    }

    public void setSSSTSFE(String SSSTSFE) {
        this.SSSTSFE = SSSTSFE;
    }

    public String getSSSTSNONFE() {
        return SSSTSNONFE;
    }

    public void setSSSTSNONFE(String SSSTSNONFE) {
        this.SSSTSNONFE = SSSTSNONFE;
    }
    
    

}
