/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.model;

import java.io.Serializable;

/**
 *
 * @author Rustanian
 */
public class Sslmodup implements Serializable {

    String SSNODEID;
    String SSMODUPSEQ;
    String SSMODVER;
    String SSMODVERDT;
    String SSLOCUPD;
    String SSUPDNM;

    public String getSSNODEID() {
        return SSNODEID;
    }

    public void setSSNODEID(String SSNODEID) {
        this.SSNODEID = SSNODEID;
    }

    public String getSSMODUPSEQ() {
        return SSMODUPSEQ;
    }

    public void setSSMODUPSEQ(String SSMODUPSEQ) {
        this.SSMODUPSEQ = SSMODUPSEQ;
    }

    public String getSSMODVER() {
        return SSMODVER;
    }

    public void setSSMODVER(String SSMODVER) {
        this.SSMODVER = SSMODVER;
    }

    public String getSSMODVERDT() {
        return SSMODVERDT;
    }

    public void setSSMODVERDT(String SSMODVERDT) {
        this.SSMODVERDT = SSMODVERDT;
    }

    public String getSSLOCUPD() {
        return SSLOCUPD;
    }

    public void setSSLOCUPD(String SSLOCUPD) {
        this.SSLOCUPD = SSLOCUPD;
    }

    public String getSSUPDNM() {
        return SSUPDNM;
    }

    public void setSSUPDNM(String SSUPDNM) {
        this.SSUPDNM = SSUPDNM;
    }
    
    

}
