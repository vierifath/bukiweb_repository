/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.model;

import java.io.Serializable;

/**
 *
 * @author Rustanian
 */
public class Sslnode implements Serializable {

    private String SSNODEID;
    private String SSNODENM;
    private String SSNODEST;
    private String SSNODEPRNT;
    private String SSNDIPFUNC;
    private String SSNODEVER;
    private String SSNODEURL;
    private String SSSECCODE;
    private String SSDESCNODE;
    private String SSFIELD01;
    private String SSFIELD02;
    private String SSFIELD03;
    private String SSFIELD04;
    private String SSFIELD05;
    private String SSFIELD06;

    public String getSSNODEURL() {
        return SSNODEURL;
    }

    public void setSSNODEURL(String SSNODEURL) {
        this.SSNODEURL = SSNODEURL;
    }

    public String getSSSECCODE() {
        return SSSECCODE;
    }

    public void setSSSECCODE(String SSSECCODE) {
        this.SSSECCODE = SSSECCODE;
    }

    public String getSSDESCNODE() {
        return SSDESCNODE;
    }

    public void setSSDESCNODE(String SSDESCNODE) {
        this.SSDESCNODE = SSDESCNODE;
    }

    public String getSSFIELD05() {
        return SSFIELD05;
    }

    public void setSSFIELD05(String SSFIELD05) {
        this.SSFIELD05 = SSFIELD05;
    }

    public String getSSFIELD06() {
        return SSFIELD06;
    }

    public void setSSFIELD06(String SSFIELD06) {
        this.SSFIELD06 = SSFIELD06;
    }

    public String getSSNODEID() {
        return SSNODEID;
    }

    public void setSSNODEID(String SSNODEID) {
        this.SSNODEID = SSNODEID;
    }

    public String getSSNODENM() {
        return SSNODENM;
    }

    public void setSSNODENM(String SSNODENM) {
        this.SSNODENM = SSNODENM;
    }

    public String getSSNODEST() {
        return SSNODEST;
    }

    public void setSSNODEST(String SSNODEST) {
        this.SSNODEST = SSNODEST;
    }

    public String getSSNODEPRNT() {
        return SSNODEPRNT;
    }

    public void setSSNODEPRNT(String SSNODEPRNT) {
        this.SSNODEPRNT = SSNODEPRNT;
    }

    public String getSSNODEVER() {
        return SSNODEVER;
    }

    public void setSSNODEVER(String SSNODEVER) {
        this.SSNODEVER = SSNODEVER;
    }

    public String getSSNDIPFUNC() {
        return SSNDIPFUNC;
    }

    public void setSSNDIPFUNC(String SSNDIPFUNC) {
        this.SSNDIPFUNC = SSNDIPFUNC;
    }

    public String getSSFIELD01() {
        return SSFIELD01;
    }

    public void setSSFIELD01(String SSFIELD01) {
        this.SSFIELD01 = SSFIELD01;
    }

    public String getSSFIELD02() {
        return SSFIELD02;
    }

    public void setSSFIELD02(String SSFIELD02) {
        this.SSFIELD02 = SSFIELD02;
    }

    public String getSSFIELD03() {
        return SSFIELD03;
    }

    public void setSSFIELD03(String SSFIELD03) {
        this.SSFIELD03 = SSFIELD03;
    }

    public String getSSFIELD04() {
        return SSFIELD04;
    }

    public void setSSFIELD04(String SSFIELD04) {
        this.SSFIELD04 = SSFIELD04;
    }

}
