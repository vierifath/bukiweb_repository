/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.model;

import java.io.Serializable;

/**
 *
 * @author Rustanian
 */
public class Sslgroup implements Serializable {

    String SSGRPID;
    String SSGRPNM;
    String SSLVLADM;
    String SSULIMO;
    String SSULIMI;

    public String getSSGRPID() {
        return SSGRPID;
    }

    public void setSSGRPID(String SSGRPID) {
        this.SSGRPID = SSGRPID;
    }

    public String getSSGRPNM() {
        return SSGRPNM;
    }

    public void setSSGRPNM(String SSGRPNM) {
        this.SSGRPNM = SSGRPNM;
    }

    public String getSSLVLADM() {
        return SSLVLADM;
    }

    public void setSSLVLADM(String SSLVLADM) {
        this.SSLVLADM = SSLVLADM;
    }

    public String getSSULIMO() {
        return SSULIMO;
    }

    public void setSSULIMO(String SSULIMO) {
        this.SSULIMO = SSULIMO;
    }

    public String getSSULIMI() {
        return SSULIMI;
    }

    public void setSSULIMI(String SSULIMI) {
        this.SSULIMI = SSULIMI;
    }

    public String getSSULIRAK() {
        return SSULIRAK;
    }

    public void setSSULIRAK(String SSULIRAK) {
        this.SSULIRAK = SSULIRAK;
    }

    public String getSSULORAK() {
        return SSULORAK;
    }

    public void setSSULORAK(String SSULORAK) {
        this.SSULORAK = SSULORAK;
    }

    public String getNEXTP() {
        return NEXTP;
    }

    public void setNEXTP(String NEXTP) {
        this.NEXTP = NEXTP;
    }
    String SSULIRAK;
    String SSULORAK;
    String NEXTP;

}
