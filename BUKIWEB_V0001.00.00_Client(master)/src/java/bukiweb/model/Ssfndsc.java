/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.model;

import java.io.Serializable;

/**
 * private String* private String* @author dpti private String
 */
public class Ssfndsc implements Serializable{

    private String SSNDID;
    private String SSSECCOD;
    private String SSFLD01;
    private String SSFLD02;
    private String SSFLD03;
    private String SSFLD04;
    private String SSFLD05;

    public String getSSNDID() {
        return SSNDID;
    }

    public void setSSNDID(String SSNDID) {
        this.SSNDID = SSNDID;
    }

    public String getSSSECCOD() {
        return SSSECCOD;
    }

    public void setSSSECCOD(String SSSECCOD) {
        this.SSSECCOD = SSSECCOD;
    }

    public String getSSFLD01() {
        return SSFLD01;
    }

    public void setSSFLD01(String SSFLD01) {
        this.SSFLD01 = SSFLD01;
    }

    public String getSSFLD02() {
        return SSFLD02;
    }

    public void setSSFLD02(String SSFLD02) {
        this.SSFLD02 = SSFLD02;
    }

    public String getSSFLD03() {
        return SSFLD03;
    }

    public void setSSFLD03(String SSFLD03) {
        this.SSFLD03 = SSFLD03;
    }

    public String getSSFLD04() {
        return SSFLD04;
    }

    public void setSSFLD04(String SSFLD04) {
        this.SSFLD04 = SSFLD04;
    }

    public String getSSFLD05() {
        return SSFLD05;
    }

    public void setSSFLD05(String SSFLD05) {
        this.SSFLD05 = SSFLD05;
    }
    
    
}
