/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.entity.data2000;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Gustiana
 */
@Entity
@Table(name = "DATA2000.TBFILE")
public class TBFILE implements Serializable{

    @Id
    private String TBFRTYPE;
    private int TBFCODE;
    private String TBFSHORT;
    private String TBFNAME;
    private int TBFSTAT;
    private String TBFMAINT;
    private String FILLER;

    public int getTBFCODE() {
        return TBFCODE;
    }

    public void setTBFCODE(int TBFCODE) {
        this.TBFCODE = TBFCODE;
    }

    public String getTBFSHORT() {
        return TBFSHORT;
    }

    public void setTBFSHORT(String TBFSHORT) {
        this.TBFSHORT = TBFSHORT;
    }

    public String getTBFNAME() {
        return TBFNAME;
    }

    public void setTBFNAME(String TBFNAME) {
        this.TBFNAME = TBFNAME;
    }

    public int getTBFSTAT() {
        return TBFSTAT;
    }

    public void setTBFSTAT(int TBFSTAT) {
        this.TBFSTAT = TBFSTAT;
    }

    public String getTBFMAINT() {
        return TBFMAINT;
    }

    public void setTBFMAINT(String TBFMAINT) {
        this.TBFMAINT = TBFMAINT;
    }

    public String getFILLER() {
        return FILLER;
    }

    public void setFILLER(String FILLER) {
        this.FILLER = FILLER;
    }

    public String getTBFRTYPE() {
        return TBFRTYPE;
    }

    public void setTBFRTYPE(String TBFRTYPE) {
        this.TBFRTYPE = TBFRTYPE;
    }

}
