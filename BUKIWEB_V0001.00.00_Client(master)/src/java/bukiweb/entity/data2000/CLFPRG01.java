/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.entity.data2000;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Gustiana
 */

@Entity
@Table(name = "DATA2000.CLFPRG01")
public class CLFPRG01 implements Serializable{
    
    @Id
    @NotEmpty
    @Length(max = 5)
    private int CLLVL;
    
    @NotEmpty
    @Length(max = 10)
    @Column(name = "CLSEQ#")
    private int CLSEQ;
    
    @NotEmpty
    @Length(max = 10)
    @Column(name = "CLREQSEQ#")
    private int CLREQSEQ;
    
    @Length(max = 10)
    @NotEmpty
    private int CLGRP;
    
    @NotEmpty
    @Length(max = 10)
    private int CLREQGRP;
    
    @NotEmpty
    @Length(max = 10)
    private String CLPRGL;
    
    @NotEmpty
    @Length(max = 10)
    private String CLPRGN;
    
    @NotEmpty
    @Length(max = 1)
    private String CLEODF;
    
    @NotEmpty
    @Length(max = 1)
    private String CLEOMF;
    
    @NotEmpty
    @Length(max = 1)
    private String CLEOYF;
    
    @NotEmpty
    @Length(max = 1)
    private String CLPRDT;
    
    @NotEmpty
    @Length(max = 1)
    private String CLPRMS;
    
    @NotEmpty
    @Length(max = 8)
    private int CLLSDT;
    
    @NotEmpty
    @Length(max = 8)
    private int CLCRDT;
    
    @NotEmpty
    @Length(max = 1)
    private String CLCRDS;

    public int getCLLVL() {
        return CLLVL;
    }

    public void setCLLVL(int CLLVL) {
        this.CLLVL = CLLVL;
    }

    public int getCLSEQ() {
        return CLSEQ;
    }

    public void setCLSEQ(int CLSEQ) {
        this.CLSEQ = CLSEQ;
    }

    public int getCLREQSEQ() {
        return CLREQSEQ;
    }

    public void setCLREQSEQ(int CLREQSEQ) {
        this.CLREQSEQ = CLREQSEQ;
    }

    public int getCLGRP() {
        return CLGRP;
    }

    public void setCLGRP(int CLGRP) {
        this.CLGRP = CLGRP;
    }

    public int getCLREQGRP() {
        return CLREQGRP;
    }

    public void setCLREQGRP(int CLREQGRP) {
        this.CLREQGRP = CLREQGRP;
    }

    public String getCLPRGL() {
        return CLPRGL;
    }

    public void setCLPRGL(String CLPRGL) {
        this.CLPRGL = CLPRGL;
    }

    public String getCLPRGN() {
        return CLPRGN;
    }

    public void setCLPRGN(String CLPRGN) {
        this.CLPRGN = CLPRGN;
    }

    public String getCLEODF() {
        return CLEODF;
    }

    public void setCLEODF(String CLEODF) {
        this.CLEODF = CLEODF;
    }

    public String getCLEOMF() {
        return CLEOMF;
    }

    public void setCLEOMF(String CLEOMF) {
        this.CLEOMF = CLEOMF;
    }

    public String getCLEOYF() {
        return CLEOYF;
    }

    public void setCLEOYF(String CLEOYF) {
        this.CLEOYF = CLEOYF;
    }

    public String getCLPRDT() {
        return CLPRDT;
    }

    public void setCLPRDT(String CLPRDT) {
        this.CLPRDT = CLPRDT;
    }

    public String getCLPRMS() {
        return CLPRMS;
    }

    public void setCLPRMS(String CLPRMS) {
        this.CLPRMS = CLPRMS;
    }

    public int getCLLSDT() {
        return CLLSDT;
    }

    public void setCLLSDT(int CLLSDT) {
        this.CLLSDT = CLLSDT;
    }

    public int getCLCRDT() {
        return CLCRDT;
    }

    public void setCLCRDT(int CLCRDT) {
        this.CLCRDT = CLCRDT;
    }

    public String getCLCRDS() {
        return CLCRDS;
    }

    public void setCLCRDS(String CLCRDS) {
        this.CLCRDS = CLCRDS;
    }
    
    
    
}
