/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.entity.datauser;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Gustiana
 */
@Entity
@Table(name = "DATAUSER.SSFUSER")
public class SSFUSER implements Serializable{

    @Id
    @Column(name = "SSUIDSGN")
    private String SSUIDSGN;
    @Column(name = "SSGRPID")
    private String SSGRPID;
    @Column(name = "SSSTSFNGR")
    private int SSSTSFNGR;
    @Column(name = "SSSTSLGN")
    private int SSSTSLGN;
    @Column(name = "SSSTSFE")
    private int SSSTSFE;
    @Column(name = "SSSTSNONFE")
    private int SSSTSNONFE;

    public String getSSUIDSGN() {
        return SSUIDSGN;
    }

    public void setSSUIDSGN(String SSUIDSGN) {
        this.SSUIDSGN = SSUIDSGN;
    }

    public String getSSGRPID() {
        return SSGRPID;
    }

    public void setSSGRPID(String SSGRPID) {
        this.SSGRPID = SSGRPID;
    }

    public int getSSSTSFNGR() {
        return SSSTSFNGR;
    }

    public void setSSSTSFNGR(int SSSTSFNGR) {
        this.SSSTSFNGR = SSSTSFNGR;
    }

    public int getSSSTSLGN() {
        return SSSTSLGN;
    }

    public void setSSSTSLGN(int SSSTSLGN) {
        this.SSSTSLGN = SSSTSLGN;
    }

    public int getSSSTSFE() {
        return SSSTSFE;
    }

    public void setSSSTSFE(int SSSTSFE) {
        this.SSSTSFE = SSSTSFE;
    }

    public int getSSSTSNONFE() {
        return SSSTSNONFE;
    }

    public void setSSSTSNONFE(int SSSTSNONFE) {
        this.SSSTSNONFE = SSSTSNONFE;
    }

    

}
