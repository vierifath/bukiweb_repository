/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.entity.datauser;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Gustiana
 */
@Entity
public class SSFGROUP implements Serializable {

    @Id
    private String SSGRPID;
    private String SSGRPNM;
    private String SSLVLADM;
    private int SSULIMO;
    private int SSULIMI;
    private int SSULIRAK;
    private int SSULORAK;
    private String NEXTP;

    public String getSSGRPID() {
        return SSGRPID;
    }

    public void setSSGRPID(String SSGRPID) {
        this.SSGRPID = SSGRPID;
    }

    public String getSSGRPNM() {
        return SSGRPNM;
    }

    public void setSSGRPNM(String SSGRPNM) {
        this.SSGRPNM = SSGRPNM;
    }

    public String getSSLVLADM() {
        return SSLVLADM;
    }

    public void setSSLVLADM(String SSLVLADM) {
        this.SSLVLADM = SSLVLADM;
    }

    public int getSSULIMO() {
        return SSULIMO;
    }

    public void setSSULIMO(int SSULIMO) {
        this.SSULIMO = SSULIMO;
    }

    public int getSSULIMI() {
        return SSULIMI;
    }

    public void setSSULIMI(int SSULIMI) {
        this.SSULIMI = SSULIMI;
    }

    public int getSSULIRAK() {
        return SSULIRAK;
    }

    public void setSSULIRAK(int SSULIRAK) {
        this.SSULIRAK = SSULIRAK;
    }

    public int getSSULORAK() {
        return SSULORAK;
    }

    public void setSSULORAK(int SSULORAK) {
        this.SSULORAK = SSULORAK;
    }

    public String getNEXTP() {
        return NEXTP;
    }

    public void setNEXTP(String NEXTP) {
        this.NEXTP = NEXTP;
    }

}
