/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.entity.datauser;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Gustiana
 */
@Entity
@Table(name = "DATAUSER.SSFNODE")
public class SSFNODE implements Serializable {

    @Id
    @Column(name = "SSNODEID", nullable = false)
    private String SSNODEID;

    @Column(name = "SSNODENM", nullable = false)
    private String SSNODENM;

//    @Column(name = "SSNODEST", nullable = false)
//    private int SSNODEST;
  
    @Column(name = "SSNODEST", nullable = false)
    private String SSNODEST;

//    @Column(name = "SSNODEXL", nullable = false)
//    private String SSNODEXL;

    @Column(name = "SSNODEFL", nullable = false)
    private String SSNODEFL;

//    @Column(name = "SSNODEXN", nullable = false)
//    private String SSNODEXN;

    @Column(name = "SSNODEPRNT", nullable = false)
    private String SSNODEPRNT;

    @Column(name = "SSNODEVER", nullable = false)
    private String SSNODEVER;

    @Column(name = "SSNDIPFUNC", nullable = false)
    private String SSNDIPFUNC;

    @Column(name = "SSNDUPDDIR", nullable = false)
    private String SSNDUPDDIR;

    @Column(name = "SSNDUPDNM", nullable = false)
    private String SSNDUPDNM;

//    @Column(name = "SSNUMFILE", nullable = false)
//    private int SSNUMFILE;
//
//    @Column(name = "SSNODEVER1", nullable = false)
//    private String SSNODEVER1;
//
//    @Column(name = "SSFIELD1", nullable = false)
//    private String SSFIELD1;
//
//    @Column(name = "SSFIELD2", nullable = false)
//    private String SSFIELD2;
//
//    @Column(name = "SSFIELD3", nullable = false)
//    private int SSFIELD3;
//
//    @Column(name = "SSFIELD4", nullable = false)
//    private int SSFIELD4;

    public String getSSNODEID() {
        return SSNODEID;
    }

    public void setSSNODEID(String SSNODEID) {
        this.SSNODEID = SSNODEID;
    }

    public String getSSNODENM() {
        return SSNODENM;
    }

    public void setSSNODENM(String SSNODENM) {
        this.SSNODENM = SSNODENM;
    }

    public String getSSNODEST() {
        return SSNODEST;
    }

    public void setSSNODEST(String SSNODEST) {
        this.SSNODEST = SSNODEST;
    }

    public String getSSNODEFL() {
        return SSNODEFL;
    }

    public void setSSNODEFL(String SSNODEFL) {
        this.SSNODEFL = SSNODEFL;
    }

    public String getSSNODEPRNT() {
        return SSNODEPRNT;
    }

    public void setSSNODEPRNT(String SSNODEPRNT) {
        this.SSNODEPRNT = SSNODEPRNT;
    }

    public String getSSNODEVER() {
        return SSNODEVER;
    }

    public void setSSNODEVER(String SSNODEVER) {
        this.SSNODEVER = SSNODEVER;
    }

    public String getSSNDIPFUNC() {
        return SSNDIPFUNC;
    }

    public void setSSNDIPFUNC(String SSNDIPFUNC) {
        this.SSNDIPFUNC = SSNDIPFUNC;
    }

    public String getSSNDUPDDIR() {
        return SSNDUPDDIR;
    }

    public void setSSNDUPDDIR(String SSNDUPDDIR) {
        this.SSNDUPDDIR = SSNDUPDDIR;
    }

    public String getSSNDUPDNM() {
        return SSNDUPDNM;
    }

    public void setSSNDUPDNM(String SSNDUPDNM) {
        this.SSNDUPDNM = SSNDUPDNM;
    }

   
}
