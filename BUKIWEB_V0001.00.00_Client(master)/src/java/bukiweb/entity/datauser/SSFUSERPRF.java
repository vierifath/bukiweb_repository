/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bukiweb.entity.datauser;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

/**
 **
 * @author Gustiana
 *
 */
@Entity
@Table(name = "DATAUSER.SSLUSERPRF")
public class SSFUSERPRF implements Serializable {

    @Id
    @NotEmpty
    private String UIDSGN;
    @NotEmpty
    private int DPCODU;
    @NotEmpty
    private int UBATID;
    @NotEmpty
    private String UNAME;
    @NotEmpty
    private int ULOC;
    @NotEmpty
    private int UBU;
    @NotEmpty
    private int USG;
    @NotEmpty
    private int UFE;
    @NotEmpty
    private int ULVL;
    @NotEmpty
    private int UFUN;
    @NotEmpty
    private int ULIMO;
    @NotEmpty
    private int ULIMI;
    @NotEmpty
    private String UAUT;
    @NotEmpty
    private String GSECUR;
    @NotEmpty
    private String UTSGN;
    @NotEmpty
    private String NEXTP;
    @NotEmpty
    private String SYSID;
    @NotEmpty
    private int SMBRCH;
    @NotEmpty
    private int UKAS;
    @NotEmpty
    private int ULIRAK;
    @NotEmpty
    private int ULORAK;
    @NotEmpty
    private String FILER1;
    @NotEmpty
    private String FILER2;
    @NotEmpty
    private String FILER3;
    @NotEmpty
    private String FILER4;
    @NotEmpty
    private String FILER5;
    @NotEmpty
    private String FILER6;
    @NotEmpty
    private String FILER7;
    @NotEmpty
    private String FILER8;
    @NotEmpty
    private String FILER9;
    @NotEmpty
    private String FILERA;

    public String getUIDSGN() {
        return UIDSGN;
    }

    public void setUIDSGN(String UIDSGN) {
        this.UIDSGN = UIDSGN;
    }

    public int getDPCODU() {
        return DPCODU;
    }

    public void setDPCODU(int DPCODU) {
        this.DPCODU = DPCODU;
    }

    public int getUBATID() {
        return UBATID;
    }

    public void setUBATID(int UBATID) {
        this.UBATID = UBATID;
    }

    public String getUNAME() {
        return UNAME;
    }

    public void setUNAME(String UNAME) {
        this.UNAME = UNAME;
    }

    public int getULOC() {
        return ULOC;
    }

    public void setULOC(int ULOC) {
        this.ULOC = ULOC;
    }

    public int getUBU() {
        return UBU;
    }

    public void setUBU(int UBU) {
        this.UBU = UBU;
    }

    public int getUSG() {
        return USG;
    }

    public void setUSG(int USG) {
        this.USG = USG;
    }

    public int getUFE() {
        return UFE;
    }

    public void setUFE(int UFE) {
        this.UFE = UFE;
    }

    public int getULVL() {
        return ULVL;
    }

    public void setULVL(int ULVL) {
        this.ULVL = ULVL;
    }

    public int getUFUN() {
        return UFUN;
    }

    public void setUFUN(int UFUN) {
        this.UFUN = UFUN;
    }

    public int getULIMO() {
        return ULIMO;
    }

    public void setULIMO(int ULIMO) {
        this.ULIMO = ULIMO;
    }

    public int getULIMI() {
        return ULIMI;
    }

    public void setULIMI(int ULIMI) {
        this.ULIMI = ULIMI;
    }

    public String getUAUT() {
        return UAUT;
    }

    public void setUAUT(String UAUT) {
        this.UAUT = UAUT;
    }

    public String getGSECUR() {
        return GSECUR;
    }

    public void setGSECUR(String GSECUR) {
        this.GSECUR = GSECUR;
    }

    public String getUTSGN() {
        return UTSGN;
    }

    public void setUTSGN(String UTSGN) {
        this.UTSGN = UTSGN;
    }

    public String getNEXTP() {
        return NEXTP;
    }

    public void setNEXTP(String NEXTP) {
        this.NEXTP = NEXTP;
    }

    public String getSYSID() {
        return SYSID;
    }

    public void setSYSID(String SYSID) {
        this.SYSID = SYSID;
    }

    public int getSMBRCH() {
        return SMBRCH;
    }

    public void setSMBRCH(int SMBRCH) {
        this.SMBRCH = SMBRCH;
    }

    public int getUKAS() {
        return UKAS;
    }

    public void setUKAS(int UKAS) {
        this.UKAS = UKAS;
    }

    public int getULIRAK() {
        return ULIRAK;
    }

    public void setULIRAK(int ULIRAK) {
        this.ULIRAK = ULIRAK;
    }

    public int getULORAK() {
        return ULORAK;
    }

    public void setULORAK(int ULORAK) {
        this.ULORAK = ULORAK;
    }

    public String getFILER1() {
        return FILER1;
    }

    public void setFILER1(String FILER1) {
        this.FILER1 = FILER1;
    }

    public String getFILER2() {
        return FILER2;
    }

    public void setFILER2(String FILER2) {
        this.FILER2 = FILER2;
    }

    public String getFILER3() {
        return FILER3;
    }

    public void setFILER3(String FILER3) {
        this.FILER3 = FILER3;
    }

    public String getFILER4() {
        return FILER4;
    }

    public void setFILER4(String FILER4) {
        this.FILER4 = FILER4;
    }

    public String getFILER5() {
        return FILER5;
    }

    public void setFILER5(String FILER5) {
        this.FILER5 = FILER5;
    }

    public String getFILER6() {
        return FILER6;
    }

    public void setFILER6(String FILER6) {
        this.FILER6 = FILER6;
    }

    public String getFILER7() {
        return FILER7;
    }

    public void setFILER7(String FILER7) {
        this.FILER7 = FILER7;
    }

    public String getFILER8() {
        return FILER8;
    }

    public void setFILER8(String FILER8) {
        this.FILER8 = FILER8;
    }

    public String getFILER9() {
        return FILER9;
    }

    public void setFILER9(String FILER9) {
        this.FILER9 = FILER9;
    }

    public String getFILERA() {
        return FILERA;
    }

    public void setFILERA(String FILERA) {
        this.FILERA = FILERA;
    }

}
