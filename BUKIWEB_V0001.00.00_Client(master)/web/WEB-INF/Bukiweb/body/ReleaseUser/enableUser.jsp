<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--<link href="../css/bootstrap.min.css" rel="stylesheet">-->
<!--<link href="../font-awesome/css/font-awesome.css" rel="stylesheet">-->
<!--<link href="../css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="../css/animate.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">

<link href="../css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">-->

<script>
    function enUser(URL) {
        $("#test1").load(URL + "?" + $("form").serialize());
    }
    function loadReleaseA(URL) {
//        alert(URL);
        URL = URL.split("/");
//        alert(URL[0]);
//        document.getElementById("release").innerHTML = "<input readonly=\"\" value=\"" + URL[1] + "\" name=\"username\" type=\"text\" class=\"form-control\" id=\"username\">";
        document.getElementById("username").value = URL[1];
    }
</script>
<link href="../css/plugins/select2/select2.min.css" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">                        
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    ${form_name} - ${verson}
                </div>
                <div class="panel-body">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>RELEASE USER MAINTENANCE</h5>                                   
                        </div>
                        <div class="ibox-content">
                            <form id="form" class="form-horizontal" method="get">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">USER ID</label>
                                    <div class="col-sm-10 input-group-sm m-b-n-sm">

                                        ${release}

                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label class="col-sm-2 control-label">USERNAME</label>
                                    <div class="col-sm-10 input-group-sm m-b-n-sm" id="release">
                                        <input readonly="" value="" name="username" type="text" class="form-control" id="username">
                                    </div>
                                </div>
                                <script>
                                    function enableBukiweb() {
//                                        alert(document.getElementById("username"));
                                    }
                                </script>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">STATUS</label>
                                    <div class="col-sm-10">
                                        <div class="i-checks">
                                            <label class=""> 
                                                <div style="position: relative;" class="iradio_square-green">
                                                    <input style="position: absolute; opacity: 0;" value="1" name="userIdRelease" type="radio"  >
                                                    <ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins>
                                                </div> <i></i> ENABLE 
                                            </label>
                                        </div>
                                        <div class="i-checks">
                                            <label class=""> 
                                                <div style="position: relative;" class="iradio_square-green">
                                                    <input style="position: absolute; opacity: 0;" value="2" name="a" type="radio" >
                                                    <ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins>
                                                </div> <i></i> DISABLE 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button id="mybut" onclick="enUser('UAAUB1')" class="btn btn-w-m btn-outline btn-primary" type="button"><i class="fa fa-spinner"></i> PROSES</button>                                        
                                        <button  class="btn btn-w-m btn-outline btn-primary" type="reset"><i class="fa fa-times"></i> RESET</button>
                                    </div>
                                </div>
                            </form>
                            <div id="nengkuh"></div>                        
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
<%--<c:import url="../../footer/footer.jsp"/>--%>
<!-- Mainly scripts -->
<!--<script src="../js/jquery-2.1.1.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

 Custom and plugin javascript 
<script src="../js/inspinia.js"></script>
<script src="../js/plugins/pace/pace.min.js"></script>

<script src="../js/plugins/iCheck/icheck.min.js"></script>-->
<script>
                                                    $(document).ready(function () {
                                                        $('.i-checks').iCheck({
                                                            checkboxClass: 'icheckbox_square-green',
                                                            radioClass: 'iradio_square-green',
                                                        });
                                                    });
</script>


<script src="../js/plugins/select2/select2.full.min.js"></script>

<script>
$(".select2_demo_3").select2({
                                                allowClear: true
                                            });
</script>