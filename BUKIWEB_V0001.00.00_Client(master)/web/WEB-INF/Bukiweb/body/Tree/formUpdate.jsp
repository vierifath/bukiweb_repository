<%-- 
    Document   : formInsert
    Created on : Dec 11, 2015, 9:29:07 AM
    Author     : Gustiana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--<script type="text/javascript" src="../js/validateUsername.js"></script>-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#faclevel').keyup(function () {
            var regex = new RegExp(/[^0-9]/g);
            var containsNonNumeric = this.value.match(regex);
            if (containsNonNumeric)
                this.value = this.value.replace(regex, '');
        });
    });
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <!--<div class="col-sm-12">-->

    <!--</div>-->
    <div class="panel panel-info">
        <div class="panel-heading">
            ${form_name} - ${verson}
        </div>
        <div class="panel-body">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>MAINTENANCE TREE</h5>                                   
                </div>
                ${pesan}
                <div class="ibox-content">
                    <form id="form" class="form-horizontal" method="get">
                        <div class="ibox float-e-margins">
                            <!--<div class="ibox-content">-->

                            <div class="form-group">
                                <label class="col-sm-2 control-label">GROUP</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSGRPID}" name="nodename" type="text" class="form-control">
                                </div>
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${grupname}" name="nodename" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">NODE ID</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSNODEID}" name="nodeid" type="text" class="form-control">
                                </div>                                   
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${nodename}" name="nodeid" type="text" class="form-control">
                                </div>                                   
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PARENT</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSPARENT}" name="nodename" type="text" class="form-control">
                                </div>
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${parentname}" name="nodename" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">PREVIOUS</label>
                                <div class="col-sm-3 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${allnode.SSNODEPR}" name="nodename" type="text" class="form-control">
                                </div>
                                <div class="col-sm-7 input-group-sm m-b-n-sm">
                                    <input readonly="" value="${prevname}" name="nodename" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">FACILITY LEVEL</label>
                                <div class="col-sm-2">
                                    <input id="faclevel" value="${allnode.SSLVLFAC}" maxlength="1" name="faclev" type="text" class="form-control">
                                </div>
                            </div>
                                <script>
                                    function updateTree(URL){
                                        $("#test1").load(URL + '?' + $("form").serialize());
                                    }
                                </script>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button onclick="updateTree('UpdateTree')" class="btn btn-w-m btn-outline btn-primary" type="button"><i class="fa fa-edit"></i> EDIT</button>                                        
                                </div>
                            </div>
                            <!--</div>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>         