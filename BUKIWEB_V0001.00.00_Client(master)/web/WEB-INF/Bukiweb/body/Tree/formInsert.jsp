<%-- 
    Document   : formInsert
    Created on : Dec 11, 2015, 9:29:07 AM
    Author     : Gustiana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="../css/plugins/select2/select2.min.css" rel="stylesheet">
<div class="wrapper wrapper-content animated fadeInRight">
    <!--<div class="col-sm-12">-->

    <!--</div>-->
    <div class="panel panel-info">
        <div class="panel-heading">
            ${form_name} - ${verson}
        </div>
            
        <div class="panel-body">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>MAINTENANCE TREE</h5>                                   
                </div>
                ${pesan}
                <div class="ibox-content">
                    <form class="form-horizontal" method="post">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Node ID</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">

                                <select id="nodeid" onchange="loadBb('DetNode?node=' + this.value)" class="select2_demo_3 form-control m-b" name="nodeid">
                                    <option disabled="" selected="">--Pilih Node ID--</option>
                                    <c:forEach var="node" items="${node}" >
                                        <option value="${node.get(0)}">${node.get(0)} ----- ${node.get(1)}</option>
                                    </c:forEach>
                                </select> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="neng">
            <form id="form" class="form-horizontal" method="get">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Node Name</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" value="${allnode.SSNODENM}" name="nodename" type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <label class="col-sm-2 control-label">Node Status</label>
                                <script type="text/javascript">
                                    function cekrdb1() {
                                        document.getElementById("parentnodeid").disabled = true;
                                    }

                                    function cekrdb2() {
                                        document.getElementById("parentnodeid").disabled = false;
                                    }

                                </script>
                                <div class="col-sm-10">
                                    <input disabled="" onchange="cekrdb1()" ${radio1} value="0" id="optionsRadios1" name="nodestatus" type="radio"> Parent Node
                                </div>
                                <div class="col-sm-10">
                                    <input disabled="" onchange="cekrdb2()" ${radio2} value="1" id="optionsRadios2" name="nodestatus" type="radio"> Child of Parent Node
                                </div>
                            </div>                                    
                        </div>
                        <div class="form-group">
                            <div>
                                <label class="col-sm-2 control-label">Parent Node</label>
                                <div class="col-sm-4 input-group-sm m-b-n-sm">
                                    <select disabled="" id="parentnodeid" class="form-control m-b" name="parentnode" ${cbb}>
                                        <option value="${allnode.SSNODEPRNT}" hidden="hidden">${allnode.SSNODEPRNT} <label class="col-sm-2 control-label"> / </label>${nameParentNode}</option>
                                        <c:forEach var="parent" items="${parent}" >
                                            <option value="${parent.get(0)}">${parent.get(0)} <label class="col-sm-2 control-label"> / </label>${parent.get(1)}</option>
                                        </c:forEach>
                                    </select>                                         
                                </div>
                            </div>                                    
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Version</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" value="${allnode.SSNODEVER}" name="version" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">IP Server Function</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" value="${allnode.SSNDIPFUNC}" name="ipserver" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">URL</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" value="${allnode.SSNODEURL}" required="" name="nodeurl" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Secure Node</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" value="${allnode.SSSECCODE}" name="securenode" required="" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Deskripsi Node</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" value="${allnode.SSDESCNODE}" required="" name="descnode" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                            <div class="form-group">
                            <label class="col-sm-2 control-label">Facility Level</label>
                            <div class="col-sm-10 input-group-sm m-b-n-sm">
                                <input disabled="" required="" name="faclev" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button disabled="" onclick="loadBodyPost('NDCHG2')" class="btn btn-w-m btn-outline btn-primary" type="button"><i class="fa fa-save"></i>SAVE</button>                                        
                                <button disabled="" class="btn btn-w-m btn-outline btn-primary" type="reset"><i class="fa fa-times"></i> RESET</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/plugins/select2/select2.full.min.js"></script>

<script>
$(".select2_demo_3").select2({
                                                
                                            });
</script>
        