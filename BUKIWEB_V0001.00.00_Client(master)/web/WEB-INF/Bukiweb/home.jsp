<%-- 
    Document   : home
    Created on : Nov 10, 2015, 3:37:03 PM
    Author     : dpti
--%>
<style>

    #img1:hover {
        opacity: 0.5;
        filter: alpha(opacity=50);  For IE8 and earlier 
    }
</style>
<style>

    #tooltiptext1 {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
        top: 100%;
        left: 50%;
        margin-left: -60px;
    }

    /*    #img:hover .tooltiptext {
            visibility: visible;
        }*/
</style>
<script>
    function mouseOver1() {
    document.getElementById("tooltiptext1").style.visibility = "visible";
    }
    function mouseOut1() {
    document.getElementById("tooltiptext1").style.visibility = "hidden";
    }
</script>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!--<!DOCTYPE html>-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="col-md-12">
        <div class="profile-image">
            <div class="dropdown profile-element text-center">
                <!--../img/userWhite1.png-->
                <!--<img src="${profil[23]}" class="img-circle m-b-md" alt="profile">-->
                <span>
                    <script>
                        function changePicture() {
                        //                                            alert("lebet ka ubah potret");
                        $("#test1").load("changePicture");
                        }
                    </script>
                    <!--onmouseover="mouseOver()" onmouseout="mouseOut()"-->
                    <img id="img1" src="${profil[23]}" onmouseover="mouseOver1()" onmouseout="mouseOut1()" title="Change Image" width="80" height="80" class="img img-circle" onclick="changePicture()" alt="profile">
                    <span id="tooltiptext1">
                        Change Image
                    </span>
                </span>
            </div>
        </div>
        <div class="profile-info">
            <div class="">
                <div>
                    <h2 class="no-margins">
                        ${profil[0]}
                    </h2>
                    <h4>${profil[1]}</h4>
                    <small>
<!--                        <script>
                                    var findIP = new Promise(r => {
                                        var w = window, 
                                        a = new (w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({iceServers:[]}), 
                                        b = () => {}; 
                                        a.createDataChannel(""); 
                                        a.createOffer(c => a.setLocalDescription(c, b, b), b); 
                                        a.onicecandidate = c => {
                                            try{
                                                c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)
                                            } catch (e){
                                                
                                            }
                                        }
                                    });
                                    
                                    /*Usage example*/
                                    findIP.then(
                                            ip => alert(ip)
                                            ).catch(e => console.error(e));
                        </script>-->
                        <table style="font-size: 11px;">
                            <tr>
                                <td>Group Name</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>${profil[7]} - ${profil[2]}</td>
                            </tr>
                            <tr>
                                <td>Branch</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>${profil[3]}</td>
                            </tr>
                            <tr>
                                <td>Location</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>${profil[4]}</td>
                            </tr>
                            <tr>
                                <td>Logged Date</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>${profil[5]}</td>
                            </tr>
                            <tr>
                                <td>Logged Time</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>${profil[6]}</td>
                            </tr>
                            <!--                                <tr>
                                                                <td><input type="text" name="version" /></td>
                                                            </tr>-->
                        </table>

                    </small>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="text-center p-md">
                        <h2><span class="text-navy">HI.. ${profil[0]} SELAMAT DATANG DI APLIKASI BUKIWEB</span></h2>
                        <p>
                            All config options you can change bootstrap skin from the theme box configuration (green icon on the left side of page).
                        </p>
                        <br/>
                        <img src="../img/NewLogoBUKIWEB270.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
