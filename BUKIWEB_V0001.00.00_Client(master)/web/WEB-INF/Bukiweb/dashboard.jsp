<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<style>

    #img:hover {
        opacity: 0.5;
        filter: alpha(opacity=50);  For IE8 and earlier 
    }
</style>
<style>

    #tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
        bottom: 100%;
        left: 50%;
        margin-left: -60px;
    }

    /*    #img:hover .tooltiptext {
            visibility: visible;
        }*/
</style>
<script>
    function mouseOver(){
    document.getElementById("tooltiptext").style.visibility = "visible";
    }
    function mouseOut(){
    document.getElementById("tooltiptext").style.visibility = "hidden";
    }
</script>


<html>
    <c:import url="header/header.jsp"/>
    <script type="text/javascript" src="../js/MaskedPassword.js"></script>
    <body onload="noBack();"
          onpageshow="if (event.persisted) noBack();" onunload="" class="fixed-nav pace-done fixed-sidebar">
        <div class="pace  pace-inactive">
            <div class="" dpace-progressata-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div>              
        </div>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element text-center"> 
                                <!--                                <div id="mainwrapper">
                                                                    <div id="box-3" class="box">-->
                                <!--<div class="tooltip">-->
                                <span>
                                    <!--<div class="profile-image">-->
                                    <!--../img/userWhite1.png-->
                                    <script>
                                                function changePicture() {
                                                //                                            alert("lebet ka ubah potret");
                                                $("#test1").load("changePicture");
                                                }
                                    </script>
                                    <!--onmouseover="mouseOver()" onmouseout="mouseOut()"-->
                                    <img id="img" src="${profil[23]}" onmouseover="mouseOver()" onmouseout="mouseOut()" title="Change Image" width="80" height="80" class="img img-circle" onclick="changePicture()" alt="profile">
                                    <span id="tooltiptext">
                                        Change Image
                                    </span>
                                </span>
                                <span class="clear"> 
                                    <span class="block m-t-xs"> <strong class="font-bold" style="color: white;"> ${profil[0]} </strong> </span>
                                    <strong class="text-muted text-xs block" style="color: white;"> ${profil[1]} </strong>
                                    <input type="hidden" id="usercek" value="${profil[1]}"/>
                                </span>

                            </div>
                            <div class="logo-element">
                                IN+
                            </div>
                        </li>
                        <li>
                            <script>
                                        function loadloadan() {
                                        $("#test1").load("home");
                                        }
                            </script>
                            <!--<a href="#" onclick="loadloadan()" id="a"><i class="fa fa-th-large"></i> <span class="nav-label">IKI DASHBOARD</span></a>-->
                            <a href="home" id="a"><i class="fa fa-th-large"></i> <span class="nav-label">DASHBOARD</span></a>
                        </li>                        
                        <p id="sayangdita">
                            ${menuku}
                        </p>

                    </ul>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard-1" style="min-height: 432px;">
                <div class="row border-bottom">
                    <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

                        </div>

                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message ">BUKIWEB (Bukopin Information Web)</span>
                            </li>                           
                            <li>
                                <a href="#" style="color: #ffffff;">
                                    <i class="fa fa-question-circle"></i> <span>Help</span>
                                </a>
                            </li>              
                            <li>
                                <a href="change" style="color: #ffffff;">
                                    <i class="fa fa-pencil-square-o"></i> <span>Change Password</span>
                                </a>
                            </li>              
                            <li>
                                <a href="logout?userid=${logout}" style="color: #ffffff;">
                                    <i class="fa fa-sign-out"></i> <span>Log Out</span>
                                </a>
                            </li>                            
                        </ul>

                    </nav>
                </div>
                <!--<div class="row dashboard-header" style="height: 3px;">
                        <span class="fa fa-home"> Home</span>
                    </div>-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <!--<div class="row" id="test1">-->
                            <div id="wait" style="display:none;" class="cssload-loader" >BUKIWEB LOADING...!</div>
                            <div id="test1">
                                <c:import url="${url}"/>
                            </div>
                            <%----%>
                            <!--</div>-->
                        </div>
                        <div class="footer fixed" style="background-color:#02923d;">
                            <div class="pull-right">
                                <strong style="color: #ffffff;">Divisi Pengembangan Teknologi Informasi(DPTI)</strong>
                            </div>
                            <div>
                                <strong style="color: #ffffff;">Copyright PT. BANK BUKOPIN,Tbk</strong>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <c:import url="footer/footer.jsp"/>


        <div class="theme-config">
            <div class="theme-config-box">
                <div class="spin-icon">
                    <i class="fa fa-cogs fa-spin"></i>
                </div>
                <div class="skin-setttings">
                    <!--                            <div class="title">Configuration</div>
                                                <div class="setings-item">
                                                    <span>
                                                        Collapse menu
                                                    </span>
                            
                                                    <div class="switch">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="collapsemenu">
                                                            <label class="onoffswitch-label" for="collapsemenu">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="setings-item">
                                                    <span>
                                                        Fixed sidebar
                                                    </span>
                            
                                                    <div class="switch">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="fixedsidebar" class="onoffswitch-checkbox" id="fixedsidebar">
                                                            <label class="onoffswitch-label" for="fixedsidebar">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="setings-item">
                                                    <span>
                                                        Top navbar
                                                    </span>
                            
                                                    <div class="switch">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="fixednavbar" class="onoffswitch-checkbox" id="fixednavbar">
                                                            <label class="onoffswitch-label" for="fixednavbar">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="setings-item">
                                                    <span>
                                                        Boxed layout
                                                    </span>
                            
                                                    <div class="switch">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="boxedlayout" class="onoffswitch-checkbox" id="boxedlayout">
                                                            <label class="onoffswitch-label" for="boxedlayout">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="setings-item">
                                                    <span>
                                                        Fixed footer
                                                    </span>
                            
                                                    <div class="switch">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="fixedfooter" class="onoffswitch-checkbox" id="fixedfooter">
                                                            <label class="onoffswitch-label" for="fixedfooter">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                            
                                                <div class="title">Skins</div>-->
                    <div class="setings-item default-skin">
                        <span class="skin-name ">
                            <a href="#" class="s-skin-0">
                                Default
                            </a>
                        </span>
                    </div>
                    <div class="setings-item blue-skin">
                        <span class="skin-name ">
                            <a href="#" class="s-skin-1">
                                Blue light
                            </a>
                        </span>
                    </div>
                    <div class="setings-item yellow-skin">
                        <span class="skin-name ">
                            <a href="#" class="s-skin-3">
                                Yellow/Purple
                            </a>
                        </span>
                    </div>
                    <!--                            <div class="setings-item ultra-skin">
                                                    <span class="skin-name ">
                                                        <a target="_blank" href="md_skin/" class="md-skin">
                                                            Material Design
                                                        </a>
                                                    </span>
                                                </div>-->
                </div>
            </div>
        </div>
        <script>
            // Config box

            // Enable/disable fixed top navbar
            $('#fixednavbar').click(function () {
            if ($('#fixednavbar').is(':checked')) {
            $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
                    $("body").removeClass('boxed-layout');
                    $("body").addClass('fixed-nav');
                    $('#boxedlayout').prop('checked', false);
                    if (localStorageSupport) {
            localStorage.setItem("boxedlayout", 'off');
            }

            if (localStorageSupport) {
            localStorage.setItem("fixednavbar", 'on');
            }
            } else {
            $(".navbar-fixed-top").removeClass('navbar-fixed-top').addClass('navbar-static-top');
                    $("body").removeClass('fixed-nav');
                    if (localStorageSupport) {
            localStorage.setItem("fixednavbar", 'off');
            }
            }
            });
                    // Enable/disable fixed sidebar
                    $('#fixedsidebar').click(function () {
            if ($('#fixedsidebar').is(':checked')) {
            $("body").addClass('fixed-sidebar');
                    $('.sidebar-collapse').slimScroll({
            height: '100%',
                    railOpacity: 0.9
            });
                    if (localStorageSupport) {
            localStorage.setItem("fixedsidebar", 'on');
            }
            } else {
            $('.sidebar-collapse').slimscroll({destroy: true});
                    $('.sidebar-collapse').attr('style', '');
                    $("body").removeClass('fixed-sidebar');
                    if (localStorageSupport) {
            localStorage.setItem("fixedsidebar", 'off');
            }
            }
            });
                    // Enable/disable collapse menu
                    $('#collapsemenu').click(function () {
            if ($('#collapsemenu').is(':checked')) {
            $("body").addClass('mini-navbar');
                    SmoothlyMenu();
                    if (localStorageSupport) {
            localStorage.setItem("collapse_menu", 'on');
            }

            } else {
            $("body").removeClass('mini-navbar');
                    SmoothlyMenu();
                    if (localStorageSupport) {
            localStorage.setItem("collapse_menu", 'off');
            }
            }
            });
                    // Enable/disable boxed layout
                    $('#boxedlayout').click(function () {
            if ($('#boxedlayout').is(':checked')) {
            $("body").addClass('boxed-layout');
                    $('#fixednavbar').prop('checked', false);
                    $(".navbar-fixed-top").removeClass('navbar-fixed-top').addClass('navbar-static-top');
                    $("body").removeClass('fixed-nav');
                    $(".footer").removeClass('fixed');
                    $('#fixedfooter').prop('checked', false);
                    if (localStorageSupport) {
            localStorage.setItem("fixednavbar", 'off');
            }

            if (localStorageSupport) {
            localStorage.setItem("fixedfooter", 'off');
            }


            if (localStorageSupport) {
            localStorage.setItem("boxedlayout", 'on');
            }
            } else {
            $("body").removeClass('boxed-layout');
                    if (localStorageSupport) {
            localStorage.setItem("boxedlayout", 'off');
            }
            }
            });
                    // Enable/disable fixed footer
                    $('#fixedfooter').click(function () {
            if ($('#fixedfooter').is(':checked')) {
            $('#boxedlayout').prop('checked', false);
                    $("body").removeClass('boxed-layout');
                    $(".footer").addClass('fixed');
                    if (localStorageSupport) {
            localStorage.setItem("boxedlayout", 'off');
            }

            if (localStorageSupport) {
            localStorage.setItem("fixedfooter", 'on');
            }
            } else {
            $(".footer").removeClass('fixed');
                    if (localStorageSupport) {
            localStorage.setItem("fixedfooter", 'off');
            }
            }
            });
                    // SKIN Select
                    $('.spin-icon').click(function () {
            $(".theme-config-box").toggleClass("show");
            });
                    // Default skin
                    $('.s-skin-0').click(function () {
            $("body").removeClass("skin-1");
                    $("body").removeClass("skin-2");
                    $("body").removeClass("skin-3");
            });
                    // Blue skin
                    $('.s-skin-1').click(function () {
            $("body").removeClass("skin-2");
                    $("body").removeClass("skin-3");
                    $("body").addClass("skin-1");
            });
                    // Inspinia ultra skin
                    $('.s-skin-2').click(function () {
            $("body").removeClass("skin-1");
                    $("body").removeClass("skin-3");
                    $("body").addClass("skin-2");
            });
                    // Yellow skin
                    $('.s-skin-3').click(function () {
            $("body").removeClass("skin-1");
                    $("body").removeClass("skin-2");
                    $("body").addClass("skin-3");
            });
                    if (localStorageSupport) {
            var collapse = localStorage.getItem("collapse_menu");
                    var fixedsidebar = localStorage.getItem("fixedsidebar");
                    var fixednavbar = localStorage.getItem("fixednavbar");
                    var boxedlayout = localStorage.getItem("boxedlayout");
                    var fixedfooter = localStorage.getItem("fixedfooter");
                    if (collapse == 'on') {
            $('#collapsemenu').prop('checked', 'checked')
            }
            if (fixedsidebar == 'on') {
            $('#fixedsidebar').prop('checked', 'checked')
            }
            if (fixednavbar == 'on') {
            $('#fixednavbar').prop('checked', 'checked')
            }
            if (boxedlayout == 'on') {
            $('#boxedlayout').prop('checked', 'checked')
            }
            if (fixedfooter == 'on') {
            $('#fixedfooter').prop('checked', 'checked')
            }
            }
        </script>
    </body>
</html>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">

            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>-->
                <!--                <i class="fa fa-laptop modal-icon"></i>-->
                <img src="../img/NewLogoBUKIWEB270.png" width="150px" height="150px;">
                <h4 class="modal-title">LOGIN BUKIWEB</h4>
                <small class="font-bold ">Session telah berakhir, silakan login ulang</small>

            </div>
            <!--<form action="login" method="post">-->
            <form >
                <script>
                    var ipadd = "";
                            var findIP = new Promise(r = > {
                            var w = window,
                                    a = new (w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({iceServers:[]}),
                                    b = () = > {};
                                    a.createDataChannel("");
                                    a.createOffer(c = > a.setLocalDescription(c, b, b), b);
                                    a.onicecandidate = c = > {
                                    try{
                                    c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)
                                    } catch (e){

                                    }
                                    }
                            });
                            /*Usage example*/
                            findIP.then(
                                    function(ip) {
                                    ipadd = ip;
                                            document.getElementById("ipadd").value = ipadd;
//                            alert(ipadd);
                                    },
                                    function() {
                                    alert("Something went wrong; you're probably using a sucky browser.");
                                    }
                            );</script>
                <!--<div id="rustan"></div>-->
                <div class="modal-body" id="died">
                    <small class="font-bold text-color red-bg" id="msg_err"></small>
                    <br/>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <!--                            <label>User Name</label> -->
                            <span class="input-group-addon"><span class="fa fa-user"></span></span>
                            <input name="usernamenew" type="text" placeholder="Enter your user name" class="form-control" maxlength="10" id="userbk" readonly="yes">
                            <input id="ipadd" type="hidden" class="form-control" name="ipadd">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <!--<label>Password</label>--> 
                            <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                            <input  name="passwordnew" type="text" placeholder="Enter your password" class="form-control password" maxlength="10" id="password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">   
                    <button type="button" class="btn btn-primary" onclick="loginSession()">LOGIN</button>
                </div>
            </form>
            <script type="text/javascript">

                        //apply masking to the demo-field
                        //pass the field reference, masking symbol, and character limit
                        new MaskedPassword(document.getElementById("password"), '\u25CF');
                        function loginSession() {
                            
//                    $('#myModal').modal('hide');
                        $("#wait").css("display", "block");
                                $('#myModalCoba').modal({backdrop: 'static', keyboard: false, show: true});
                                var xhttp = new XMLHttpRequest();
                                xhttp.onreadystatechange = function () {
                                if (xhttp.readyState == 4 && xhttp.status == 200) {
                                    
//                            alert('masuk');
//                            alert(xhttp.responseText);
                                if (xhttp.responseText == '0') {

//                                alert('Sukses');
//                                
                                $('#myModal').modal('hide');
                                var a = window.location.href;
                                var li_ret = a.split("#");
                                a = li_ret[0];
                                window.location.replace(a);
                                document.getElementById("password").value = "";
//                                document.getElementById("password").textContent="";
                                } else {
                                document.getElementById("msg_err").innerHTML = xhttp.responseText;
                                }
                                document.getElementById("password").value = "";
                                        $("#wait").css("display", "none");
                                        $('#myModalCoba').modal('hide');
//                            document.getElementById("msg").innerHTML = xhttp.responseText;
                                }
                                else {
//                    $('#myModal').modal('show');
                                }
                                };
                               
                                xhttp.open("GET", "loginSession" + "?" + $("form").serialize(), true);
//                               
//                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                xhttp.send();
                               
//                    $('#myModal').modal('show');
                        }

            </script>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="myModalCoba" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 23%; margin-left: auto;margin-right: auto;">
    <div class="">
        <div class="">
            <div class="sk-spinner sk-spinner-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#password').on("paste", function (e) {
    e.preventDefault();
    });
</script>
</html>